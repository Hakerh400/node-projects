"use strict"

const fs = require("fs")
const path = require("path")
const cp = require("child_process")
const O = require("../js-util")
const assert = require("../assert")

const intval = 1e3

const main = () => {
  func()
}

const func = () => {
  cp.execSync("rfkill block all")
  setTimeout(func, intval)
}

main()