"use strict"

const fs = require("fs")
const path = require("path")
const O = require("../js-util")
const assert = require("../assert")
const refactor = require(".")

const dir = "C:/Projects/pure0/src"

const exts = [
  "hh",
  "cc",
]

const main = () => {
  refactor(dir, exts, (pth, s) => {
    s = s
      .replace(/\bstd::string\b/g, "String")
      .replace(/\bstd::unordered_set\b/g, "Set")
      .replace(/\bstd::unordered_map\b/g, "Map")
    
    return s
  })
}

main()