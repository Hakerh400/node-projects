"use strict"

const fs = require("fs")
const path = require("path")
const O = require("../js-util")
const assert = require("../assert")

const refactor = (dir, exts, fn) => {
  const queue = [dir]
  
  while(queue.length !== 0){
    const dir = queue.shift()
    const names = fs.readdirSync(dir)
    
    for(const name of names){
      const pth = path.join(dir, name)
      const stat = fs.statSync(pth)
      
      if(stat.isDirectory()){
        queue.push(pth)
        continue
      }
      
      if(stat.isFile()){
        let {ext} = path.parse(name)
        if(ext === "") continue
        
        assert(ext[0] === ".")
        ext = ext.slice(1)
        if(!exts.includes(ext)) continue
        
        let s = O.rfs(pth, 1)
        let res = fn(pth, s)
        if(res === null || res === undefined) continue
        
        assert(typeof res === "string")
        O.wfs(pth, res)
        
        continue
      }
    }
  }
}

module.exports = refactor