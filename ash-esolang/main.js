"use strict"

const fs = require("fs")
const path = require("path")
const crypto = require("crypto")
const O = require("../js-util")
const assert = require("../assert")

const cwd = __dirname
const src_file = path.join(cwd, "src.txt")
const inp_file = path.join(cwd, "inp.txt")

const main = () => {
  let src = O.rfs(src_file, 1)
  let inp = O.rfs(inp_file, 1)
  
  let mem = new Memory()
  let prog = new Prog(mem, src, inp)
  
  if(0){
    let fst = 1
    let n = 0
    
    while(1){
      // if(n === 100)
      //   O.exit()
      
      if(fst) fst = 0
      else O.logb()
      
      log(prog.show())
      if(prog.halted) break
      
      prog.tick()
      n++
    }
    
    O.logb()
    
    let out = prog.get_output()
    
    if(out.length !== 0)
      log(out)
  }else{
    prog.run()
    log(prog.show_mem())
    log(prog.get_output())
  }
}

class Prog {
  constructor(mem, src, inp){
    let defs = O.obj()
    
    src = src.replace(/^([^\:]+)\:\=([^\n]*)/gm, (m, name, s) => {
      name = name.trim()
      s = s.trim()
      defs[name] = s
      return ""
    })
    
    let names = O.keys(defs).sort((a, b) => {
      a = a.length
      b = b.length
      
      return a >= b ? -1 : 1
    })
    
    loop: while(1){
      for(let name of names){
        let s = defs[name]
        
        if(src.includes(name)){
          src = src.replace(name, defs[name])
          continue loop
        }
      }
      
      break
    }
    
    src = src.replace(/[^\<\>\~\,\.\[\]]+/g, "")
    inp = [...inp].map(a => `1${a}`).join("")
      .split("").map(a => a | 0)
    
    this.src = src
    
    let [insts, brackets] = parse_insts(src)
    
    this.insts = insts
    this.insts_num = insts.length
    this.brackets = brackets
    this.mem = mem
    this.ip = 0
    this.ptr = 0
    this.inp = inp
    this.out = []
    this.inp_index = 0
    this.inp_len = inp.length
  }
  
  get halted(){
    return this.ip === this.insts_num
  }
  
  run(){
    while(!this.halted)
      this.tick()
    
    return this.get_output()
  }
  
  tick(){
    let {insts, brackets, mem, ip, ptr} = this
    let inst = insts[this.ip++]
    
    if(inst === 0){
      this.ptr = ptr - 1
      return
    }
    
    if(inst === 1){
      this.ptr = ptr + 1
      return
    }
    
    if(inst === 2){
      mem.randomize(ptr)
      return
    }
    
    if(inst === 4){
      mem.set(ptr, this.read())
      return
    }
    
    if(inst === 5){
      this.write(mem.get(ptr))
      return
    }
    
    if(inst === 6){
      if(!mem.get(ptr))
        this.ip = brackets.get(ip) + 1
        
      return
    }
    
    if(inst === 7){
      if(mem.get(ptr))
        this.ip = brackets.get(ip) + 1
      
      return
    }
  }
  
  read(){
    let {inp, inp_index, inp_len} = this
    if(inp_index === inp_len) return 0
    
    let b = inp[inp_index]
    this.inp_index = inp_index + 1
    
    return b
  }
  
  write(b){
    this.out.push(b)
  }
  
  get_output(){
    return this.out.join("")
  }
  
  show_mem(){
    let {mem, ptr} = this
    
    let offset = 20
    let s_mem = mem.show(ptr - offset, ptr + offset)
    
    return `${s_mem}\n${" ".repeat(offset)}^`
  }
  
  show_insts(){
    let {src, ip} = this
    return `${src}\n${" ".repeat(ip)}^`
  }
  
  show(){
    let s1 = this.show_mem()
    let s2 = this.show_insts()
    
    return `${s1}\n\n${s2}`
  }
}

class Memory {
  constructor(){
    this.mp = new Map([[0, 1]])
  }
  
  getm(i){
    let {mp} = this
    if(!mp.has(i)) return null
    return mp.get(i)
  }
  
  get(i){
    let {mp} = this
    
    let b = this.getm(i)
    if(b !== null) return b
    
    b = rand_bit()
    mp.set(i, b)
    
    return b
  }
  
  set(i, n){
    this.mp.set(i, n)
  }
  
  randomize(i){
    this.mp.delete(i)
  }
  
  show(i_min, i_max){
    let s = ""
    let xs = [s]
    
    for(let i = i_min; i < i_max; i++){
      let b = this.getm(i)
      let s = b !== null ? String(b) : "."
      xs.push(s)
    }
    
    xs.push(s)
    
    return xs.join("")
  }
}

const parse_insts = s => {
  let insts = []
  let stack = []
  let brackets = new Map()
  
  for(let c of s){
    if(c === "<"){
      insts.push(0)
      continue
    }
    
    if(c === ">"){
      insts.push(1)
      continue
    }
    
    if(c === "~"){
      insts.push(2)
      continue
    }
    
    if(c === ","){
      insts.push(4)
      continue
    }
    
    if(c === "."){
      insts.push(5)
      continue
    }
    
    if(c === "["){
      stack.push(insts.length)
      insts.push(6)
      continue
    }
    
    if(c === "]"){
      if(stack.length === 0)
        syntax_err()
      
      let i = stack.pop()
      let j = insts.length
      
      brackets.set(i, j)
      brackets.set(j, i)
      insts.push(7)
      
      continue
    }
  }
  
  if(stack.length !== 0)
    syntax_err()
  
  return [insts, brackets]
}

const rand_bit = () => {
  return crypto.randomBytes(1)[0] & 1
}

const syntax_err = () => {
  O.exit("Syntax error")
}

main()