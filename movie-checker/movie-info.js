"use strict"

const fs = require("fs")
const path = require("path")
const O = require("../js-util")
const assert = require("../assert")

class MovieInfo {
  tags = []
  synopsis = null
  plot = null
  
  constructor(name, year){
    this.name = name
    this.year = year
  }
  
  add_tag(tag){
    this.tags.push(tag)
  }
  
  set_synopsis(synopsis){
    this.synopsis = synopsis
  }
  
  set_plot(plot){
    this.plot = plot
  }
  
  get_title(){
    return `${this.name} (${this.year})`;
  }
  
  toString(){
    return [
      `Title: ${this.get_title()}`,
      [...this.tags.length === 0 ? [] :
        [`Tags: ${this.tags.join(", ")}`]],
      [...this.synopsis === null ? [] :
        [`Synopsis: ${this.synopsis}`]],
      [...this.plot === null ? [] :
        [`Plot: ${this.plot}`]],
    ].join("\n")
  }
}

module.exports = MovieInfo