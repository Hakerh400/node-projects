"use strict"

const fs = require("fs")
const path = require("path")
const urlm = require("url")
const cheerio = require("../../Other/node/node_modules/cheerio")
const O = require("../js-util")
const assert = require("../assert")
const download = require("../download")
const MovieInfo = require("./movie-info")

const cwd = __dirname
const proj = path.parse(cwd).name
const movies_dir = "E:/DISK/Torrents/003/_Movies"
const data_dir = path.join(movies_dir, proj)
const inp_file = path.join(data_dir, "inp.txt")
const out_file = path.join(data_dir, "out.txt")

const main = async () => {
  const inp = O.rfs(inp_file, 1);
  const out = await check_movies(O.sanl(inp))
  
  O.wfs(out_file, out.join("\n\n"))
}

const check_movies = async names => {
  const res = []
  
  for(const name of names)
    res.push(await check_movie(name))
  
  return res
}

const check_movie = async name_raw => {
  let dom = await find_link("https://www.imdb.com/",
    `imdb ${name_raw}`)
  assert(dom !== null)
  
  let e = dom(`span.hero__primary-text`)
  assert(e.length === 1)
  
  const name = e.text()
  
  e = dom(`a[href*="/releaseinfo?ref_=tt_ov_rdat"]`)
  assert(e.length === 1)
  
  const year = Number(e.text())
  const info = new MovieInfo(name, year)
  const title = info.get_title()
  
  e = dom(`div[data-testid="genres"]`)
  if(e.length !== 0){
    const tags = e.find(`.ipc-chip-list__scroller a`)
    
    for(let i = 0; i !== tags.length; i++)
      info.add_tag(tags.eq(i).text())
  }
  
  e = dom(`p[data-testid="plot"] span:last-child`)
  assert(e.length === 1)
  
  info.set_synopsis(e.text())
  
  dom = await find_link("https://en.wikipedia.org/",
    `wikipedia movie film ${title}`)
  
  get_plot: if(dom !== null){
    e = dom(`#Plot`)
    if(e.length === 0) break get_plot
    
    e = e.closest("h2")
    assert(e.length !== 0)
    
    const es = e.nextUntil("h2", "p")
    const plot = []
    
    for(let i = 0; i !== es.length; i++){
      e = es.eq(i).clone()
      e.find(".reference").remove()
      
      let s = e.text().trim()
      if(s.length === 0) continue
      
      plot.push(s)
    }
    
    info.set_plot(plot.join("\n"))
  }
  
  return info
}

const get_dom = async url => {
  url = url.toString()
  const s = await download(url)
  return cheerio.load(s)
}

const find_online = async query => {
  const url = new URL("https://search.brave.com/")
  const ps = url.searchParams
  
  url.pathname = "search"
  
  ps.append("source", "web")
  ps.append("show_local", "0")
  ps.append("q", query)
  
  return get_dom(url)
  
  // const url = new URL("https://duckduckgo.com/")
  // const ps = url.searchParams
  // 
  // url.pathname = ""
  // 
  // ps.append("ia", "web")
  // ps.append("q", query)
  // 
  // return get_dom(url)
}

const find_link = async (site, query) => {
  const dom = await find_online(query)
  const e = dom(`#results a[href^="${site}"]`)
  if(e.length === 0) return null
  
  const url = e.attr("href")
  
  return get_dom(url)
  
  // const dom = await find_online(query)
  // const e = dom(`section[data-testid="mainline"] a[href^="${site}"]`)
  // if(e.length === 0) return null
  // 
  // const url = e.attr("href")
  // 
  // return get_dom(url)
}

main().catch(log)