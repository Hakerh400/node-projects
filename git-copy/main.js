'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('assert');
const cp = require('child_process');
const O = require('../omikron');
const logStatus = require('../log-status');
const format = require('../format');

const proj = 'liza'
const srcDir = format.path('-dw/a/001');

const cwd = __dirname;
const projsDir = path.join(cwd, '../..');
const projDir = path.join(projsDir, proj);
const batchPush = path.join(projsDir, 'batch-projects', 'p.bat');

const main = () => {
  purge(projDir);
  
  const dirs = fs.readdirSync(srcDir).
    filter(a => /^\d{3}$/.test(a)).
    sort((a, b) => Number(a) - Number(b));
  
  const dirsNum = dirs.length;
  
  for(let i = 0; i !== dirsNum; i++){
    logStatus(i + 1, dirsNum, 'revision');
    
    const dirName = dirs[i];
    const dir = path.join(srcDir, dirName);
    
    copy(dir, projDir);
    runBatch(batchPush, projDir);
  }
};

const purge = dir => {
  spawn('rm', ['-rf', f(path.join(dir, '*'))]);
};

const copy = (from, to) => {
  spawn('cp', ['-r', f(path.join(from, '*')), f(to)]);
};

const runBatch = (pth, dir) => {
  spawn('cmd', ['/k', pth], {cwd: dir});
};

const spawn = (...args) => {
  const res = cp.spawnSync(...args);
  const {fd} = process.stdout;
  
  // fs.writeSync(fd, res.stdout);
  // fs.writeSync(fd, res.stderr);
};

const f = a => a.replace(/\\/g, '/');

main();