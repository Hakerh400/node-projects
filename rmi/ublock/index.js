'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('assert');
const O = require('../../js-util');
const rmi = require('..');
const tabs = require('./tabs');
const ublockDir = require('./dir');
const yt_subs = require("./yt-subs")

const methods = {
  ublockDir,
  tabs,
  yt_subs,
};

module.exports = methods;