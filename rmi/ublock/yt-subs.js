"use strict"

const fs = require("fs")
const path = require("path")
const assert = require("assert")
const cp = require("child_process")
const O = require("../../js-util")
const rmi = require("..")
const ublock_dir = require('./dir');

const subs_ext = "vtt"
const yt_dl_exe = "C:/Program Files/YoutubeDL/yt-dlp.cmd"

const yt_subs_dir = path.join(ublock_dir, "yt-subs")

if(!O.exi(yt_subs_dir))
  O.mk_dir(yt_subs_dir)

const ytids_in_progress = new Map()

const methods = {
  async get(ytid){
    const pth = ytid_to_pth(ytid)
    
    if(ytids_in_progress.has(ytid))
      return {status: 0}
    
    if(O.exi(pth))
      return {status: 1, subs: O.rfs(pth, 1)}
    
    const proc = cp.spawn(`"${yt_dl_exe}"`, [
      `--write-auto-subs`,
      `--skip-download`,
      `-o`, pth,
      `https://www.youtube.com/watch?v=${ytid}`,
    ], {
      shell: Boolean(1),
    });
    
    proc.on("error", err => {
      log(`[yt_subs.proc.error] [${ytid}] ${err}`)
    })
    
    proc.stdin.on("error", err => {
      log(`[yt_subs.proc.stdin.error] [${ytid}] ${err}`)
    })
    
    proc.stdout.on("error", err => {
      log(`[yt_subs.proc.stdout.error] [${ytid}] ${err}`)
    })
    
    proc.stderr.on("error", err => {
      log(`[yt_subs.proc.stderr.error] [${ytid}] ${err}`)
    })
    
    const out_bufs = []
    const err_bufs = []
    
    proc.stdout.on("data", buf => {
      out_bufs.push(buf)
    })
    
    proc.stderr.on("data", buf => {
      err_bufs.push(buf)
    })
    
    proc.on("exit", code => {
      const out = Buffer.concat(out_bufs).toString().trim()
      const err = Buffer.concat(err_bufs).toString().trim()
      
      if(code !== 0){
        log(`[yt_subs.exit.nzec] [${ytid}] ${code} ${out} ${err}`)
        ytids_in_progress.set(ytid, null)
        return
      }
      
      const pth1 = `${pth}.en.vtt`
      
      if(!O.exi(pth1)){
        log(`[yt_subs.exit.no_en_vtt] [${ytid}]`)
        ytids_in_progress.set(ytid, null)
        return
      }
      
      fs.renameSync(pth1, pth)
      ytids_in_progress.delete(ytid)
    })
    
    ytids_in_progress.set(ytid, proc)
    
    if(ytids_in_progress.has(ytid))
      return {status: 0}
  },
}

const ytid_to_name = ytid => {
  return `[${ytid}]`
}

const ytid_to_base = ytid => {
  const name = ytid_to_name(ytid)
  return `${name}.${subs_ext}`
}

const ytid_to_pth = ytid => {
  const base = ytid_to_base(ytid)
  return path.join(yt_subs_dir, base)
}

module.exports = methods