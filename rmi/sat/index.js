'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('assert');
const O = require('../../js-util');
const sat = require('../../sat');
const rmi = require('..');

const methods = {
  async solve(clauses){
    return sat.solve(clauses)
  },
}

module.exports = methods