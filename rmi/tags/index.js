"use strict"

const fs = require("fs")
const path = require("path")
const assert = require("assert")
const O = require("../../js-util")
const rmi = require("..")

const tags_dir = path.join(rmi.mainDir, 'tags');
const data_file = path.join(tags_dir, 'data.json');

let data = null
let is_visible = 0

const init = () => {
  O.mk_dir(tags_dir)
  
  if(!O.exi(data_file)){
    data = {}
    save_data()
  }else{
    data = JSON.parse(O.rfs(data_file, 1))
  }
}

const save_data = () => {
  O.wfs(data_file, JSON.stringify(data))
}

const nav_file = (pth, d) => {
  let dir = path.join(pth, "..")
  let names = fs.readdirSync(dir)
    .filter(a => /\S\.\S/.test(a))
  let name = path.parse(pth).base
  let index = names.indexOf(name)
  
  assert(index !== -1)
  
  let index1 = O.mod(index + d, names.length)
  let name1 = names[index1]
  let pth1 = path.join(dir, name1).replace(/\\/g, "/")
  
  return `file:///${escape(pth1)}`
}

const norm_pth = pth => {
  return unescape(pth.replace(/^file\:\/+/, ""))
}

const methods = {
  async has_tags(pth){
    pth = norm_pth(pth)
    return O.has(data, pth)
  },
  
  async get_tags(pth){
    pth = norm_pth(pth)
    if(!O.has(data, pth)) return null
    return data[pth]
  },
  
  async set_tags(pth, tags){
    pth = norm_pth(pth)
    data[pth] = tags
    save_data()
  },
  
  async remove_tags(pth){
    pth = norm_pth(pth)
    delete data[pth]
    save_data()
  },
  
  async prev_file(pth){
    pth = norm_pth(pth)
    return nav_file(pth, -1)
  },
  
  async next_file(pth){
    pth = norm_pth(pth)
    return nav_file(pth, 1)
  },
  
  async is_visible(){
    return is_visible
  },
  
  async set_visible(v){
    is_visible = v ? 1 : 0
  },
  
  // async search_fn(fn_src){
  //   let fn = new Function("pth, tags", fn_src)
  //   let results = []
  // 
  //   for(let pth of O.keys(data)){
  //   }
  // },
  
  async search(query){
    let results = []
  
    for(let pth of O.keys(data)){
      let tags = O.sanl(data[pth])[1] || ""
      
      if(query.every(s => tags.includes(s)))
        results.push([pth, tags])
    }
    
    return results
  },
}

init()

module.exports = methods