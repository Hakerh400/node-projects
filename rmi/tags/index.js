"use strict"

const fs = require("fs")
const path = require("path")
const assert = require("assert")
const O = require("../../js-util")
const rmi = require("..")

const tags_dir = path.join(rmi.mainDir, 'tags');
const data_file = path.join(tags_dir, 'data.json');

let data = null

const init = () => {
  O.mk_dir(tags_dir)
  
  if(!O.exi(data_file)){
    data = {}
    save_data()
  }else{
    data = JSON.parse(O.rfs(data_file, 1))
  }
}

const save_data = () => {
  O.wfs(data_file, JSON.stringify(data))
}

const nav_file = (pth, d) => {
  let dir = path.join(pth, "..")
  let names = fs.readdirSync(dir)
    .filter(a => /\S\.\S/.test(a))
  let name = path.parse(pth).base
  let index = names.indexOf(name)
  
  assert(index !== -1)
  
  let index1 = O.mod(index + d, names.length)
  let name1 = names[index1]
  let pth1 = path.join(dir, name1).replace(/\\/g, "/")
  
  return `file:///${escape(pth1)}`
}

const norm_pth = pth => {
  return unescape(pth.replace(/^file\:\/+/, ""))
}

const methods = {
  async has_tags(pth){
    pth = norm_pth(pth)
    return O.has(data, pth)
  },
  
  async get_tags(pth){
    pth = norm_pth(pth)
    if(!O.has(data, pth)) return null
    return data[pth]
  },
  
  async set_tags(pth, tags){
    pth = norm_pth(pth)
    data[pth] = tags
    save_data()
  },
  
  async remove_tags(pth){
    pth = norm_pth(pth)
    delete data[pth]
    save_data()
  },
  
  async prev_file(pth){
    pth = norm_pth(pth)
    return nav_file(pth, -1)
  },
  
  async next_file(pth){
    pth = norm_pth(pth)
    return nav_file(pth, 1)
  },
}

init()

module.exports = methods