'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('assert');
const O = require('../../js-util');
const rmi = require('..');

return

const tags_dir = path.join(rmi.mainDir, 'tags');
const main_dir_file = path.join(tags_dir, 'main-dir.txt');
const dirs_file = path.join(tags_dir, 'dirs.txt');
const tags_file = path.join(tags_dir, 'tags.json');

const main_dir = O.rfs(main_dir_file, 1);
const dirs = O.sanl(O.rfs(dirs_file, 1));

let files = [];

const nm = pth => {
  return pth.replace(/\\/g, '/');
};

for(const dir of dirs){
  const dir_pth = path.join(main_dir, dir);
  
  for(const name of fs.readdirSync(dir_pth)){
    if(!name.includes('.')) continue;
    
    const pth = nm(path.join(dir, name));
    files.push(pth);
  }
}

const files_num = files.length;

const init_tags = () => {
  return {
    files_tags_mp: {},
    tags_files_mp: {},
    tagged_num: 0,
  };
};

const save_tags_aux = tags => {
  O.wfs(tags_file, JSON.stringify(tags));
  return tags;
};

const save_tags = () => {
  save_tags_aux(tags_obj);
};

const init_tags_file = () => {
  const tags = init_tags();
  return save_tags_aux(tags);
};

const load_tags = () => {
  if(!fs.existsSync(tags_file))
    return init_tags_file();
  
  const s = O.rfs(tags_file, 1).trim();
  if(s === '') return init_tags_file();
  
  return JSON.parse(s);
};

const tags_obj = load_tags();

files = files.filter(file => !O.has(tags_obj.files_tags_mp, file));

const methods = {
  async get_status(){
    return [files_num, tags_obj.tagged_num];
  },
  
  async add_tags(file, tags){
    const {files_tags_mp, tags_files_mp} = tags_obj;
    
    file = nm(file);
    
    if(O.has(files_tags_mp))
      assert.fail();
    
    files_tags_mp[file] = tags;
    
    for(const tag of tags){
      if(!O.has(tags_files_mp, tag))
        tags_files_mp[tag] = [];
      
      tags_files_mp[tag].push(file);
    }
    
    const index = files.indexOf(file);
    assert(index !== -1);
    
    const val = files[index];
    const last = files.pop();
    if(index !== files.length) files[index] = last;
    
    tags_obj.tagged_num++;
    save_tags();
  },
  
  async get_next_file(){
    if(files.length === 0)
      return null;
    
    return O.randElem(files);
  },
};

module.exports = methods;