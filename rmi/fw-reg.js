"use strict"

const fs = require("fs")
const path = require("path")
const assert = require("assert")
const cp = require("child_process")
const O = require("../../js-util")
const rmi = require(".")

const cwd = __dirname
const cinema_dir = path.join(cwd, "../../Other/Cinema")
const reg_file = path.join(cinema_dir, "regex.js")

const fw_reg = require(reg_file)

const reg_to_str = reg => {
  return String(reg).split("/").slice(1, -1).join("/")
}

const fs_reg_str = reg_to_str(fw_reg)

const methods = {
  async get(){
    return fs_reg_str
  },
}

module.exports = methods