'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('assert');
const O = require('../js-util');
const ublock = require('./ublock');
const tags = require('./tags');
const sat = require('./sat');
const fw_reg = require("./fw-reg")

const methods = {
  ublock,
  tags,
  sat,
  
  get_fw_reg: fw_reg.get,

  async ping(){
    return 'ok';
  },

  async echo(val){
    return val;
  },

  fs: {
    async isFile(pth){
      return fs.statSync(pth).isFile();
    },

    async isDir(pth){
      return fs.statSync(pth).isDirectory();
    },
  },
};

module.exports = methods;