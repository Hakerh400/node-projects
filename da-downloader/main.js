'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('assert');
const https = require('https');
const zlib = require('zlib');
const O = require('../omikron');
const download = require('../download');
const logStatus = require('../log-status');

const COOKIES = null
const USER = null
const START = 1
const END = 292
const DIR_INDEX = '012'

const cwd = __dirname
const baseDir = 'F:/Folder/Images/001/000/Raw'
const imgsDir = path.join(baseDir, `${DIR_INDEX}-${USER}`)

const batchSize = 24

const skipTypes = O.arr2obj([
  'literature',
  'pdf',
]);

const main = async () => {
  assert(START > 0);
  
  let i = START - 1;
  
  while(1){
    const info = await getInfo(i);
    
    for(const {deviation: result} of info.results){
      logStatus(++i, END, 'image');
      
      if(O.has(result, 'premiumFolderData')){
        const info = result.premiumFolderData;
        const {type} = info;
        
        if(type !== 'paid') err(type);
        
        continue;
      }
      
      const {type} = result;
      
      if(type !== 'image'){
        if(O.has(skipTypes, type)) continue;
        
        err(type);
      }
      
      const {media} = result;
      const {types} = media;
      
      if(types.length === 0) continue;
      
      const {baseUri, prettyName, token: tokens} = media;
      const token = O.isArr(tokens) ? tokens[0] : tokens;
      const fullView = types.find(a => a.t === 'fullview');
      const name = O.has(fullView, 'c') ? fullView.c.replace('<prettyName>', prettyName) : null;
      const nameStr = name !== null ? `/${name}` : '';
      const url = `${baseUri}${nameStr}?token=${token}`.replace(/\/+/g, '/');
      const fileName = `${String(END - (i - 1)).padStart(5, '0')}.png`;
      
      await download(url, path.join(imgsDir, fileName));
    }
    
    if(!info.hasMore) break;
    
    if(i !== info.nextOffset)
      err(i, info.nextOffset);
  }
};

const getInfo = offset => new Promise((resolve, reject) => {
  const headers = {
    'accept': `text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9`,
    'accept-encoding': `gzip, deflate, br`,
    'accept-language': `en-US,en;q=0.9`,
    'cache-control': `no-cache`,
    'cookie': COOKIES,
    'pragma': `no-cache`,
    'referer': `https://www.deviantart.com/${USER}/gallery/all`,
    'sec-ch-ua': `"Google Chrome";v="99", " Not;A Brand";v="99", "Chromium";v="99"`,
    'sec-ch-ua-mobile': `?0`,
    'sec-fetch-dest': `document`,
    'sec-fetch-mode': `navigate`,
    'sec-fetch-site': `same-origin`,
    'sec-fetch-user': `?1`,
    'upgrade-insecure-requests': `1`,
    'user-agent': `Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4472.77 Safari/537.36`,
  };
  
  const req = new https.request({
    host: `www.deviantart.com`,
    path: `/_napi/da-user-profile/api/gallery/contents?username=${USER}&limit=24&all_folder=true&offset=${offset}`,
    headers,
  });
  
  const bufs = [];
  
  req.on('response', res => {
    res.on('data', buf => bufs.push(buf));
    
    res.on('end', () => {
      const buf = zlib.gunzipSync(Buffer.concat(bufs));
      resolve(JSON.parse(String(buf)));
    });
  });
  
  req.end();
});

const err = (...args) => {
  if(args.length !== 0){
    O.logb();
    log(...args);
    O.logb();
  }else{
    O.logb();
  }
  
  assert.fail();
};

main().catch(log);