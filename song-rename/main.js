'use strict';

const fs = require('fs');
const path = require('path');
const O = require('../omikron');
const assert = require('../assert');

const dirs = [
  //
];

const regex = null;

const main = () => {
  let cnt = 0;
  
  for(const dir of dirs){
    const files = fs.readdirSync(dir);
    
    for(const base of files){
      if(!base.includes('.'))
        continue;
      
      let base1 = base;
      let i = 0;
      
      while(1){
        const base2 = base1.replace(regex, ' ')
        if(base2 === base1) break;
        
        i++;
        base1 = base2;
      }
      
      base1 = base1.replace(/\s{2,}/g, ' ');
      if(base1 === base) continue;
      
      if(cnt++ !== 0) log()
      log(base);
      log(base1);
      
      const pth = path.join(dir, base)
      const pth1 = path.join(dir, base1)
      
      fs.renameSync(pth, pth1);
    }
  }
};

main();