"use strict"

const fs = require("fs")
const path = require("path")
const O = require("../js-util")
const assert = require("../assert")
const sat = require(".")

const main = async () => {
  let clauses = [
    [1],
    [-2],
    [3],
    [4],
  ]
  
  let res = await sat.solve(clauses)
  
  log(res)
}

main().catch(log)