"use strict"

const fs = require("fs")
const path = require("path")
const cp = require("child_process")
const O = require("../js-util")
const assert = require("../assert")
const config = require("../config")
const simple_proc = require("../simple-proc")

const solve = async clauses => {
  let clauses_num = clauses.length
  if(clauses_num === 0) return []
  
  let vars_num = 0
  
  for(const clause of clauses)
    for(const term of clause)
      vars_num = Math.max(vars_num, Math.abs(term))
  
  let xs = [`p cnf ${vars_num} ${clauses_num}`]
  
  for(const clause of clauses)
    xs.push(`${clause.join(" ")} 0`.trim())
  
  let exe = config.exe.sat
  let inp = xs.join("\n")
  let args = []
  
  let opts = {
    allow_nzec: 1,
  }
  
  let out = await simple_proc.run(exe, args, inp, opts)
  
  if(out.startsWith("s U"))
    return null
  
  return O.match(out, /\-?\d+/g)
    .slice(0, -1).map(a => a > 0 | 0)
}

module.exports = {
  solve,
}