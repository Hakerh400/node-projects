'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('../assert');

const {platform} = process;

module.exports = {
  "author": "Hakerh400",
  
  "dirs": {
    "appData": null,
  },
  
  "exe": {
    "node": null,
    "electron": (
      platform === 'linux' ? '/usr/local/lib/node_modules/electron/dist/electron' :
      platform === 'win32' ? 'C:/Users/User/AppData/Roaming/npm/node_modules/electron/dist/electron.exe' :
      null
    ),
    "php": "C:/wamp/bin/php/php7.2.4/php.exe",
    "mysql": "C:/wamp/bin/mysql/mysql5.7.21/bin/mysql.exe",
    "python": "C:/Program Files/Python311/python.exe",
    "haskell": "C:/Program Files/GHC/bin/runghc.exe",
    "agda": "C:/Program Files/Agda/bin/agda.exe",
    "lisp": "C:/Program Files/clisp-2.49/clisp.exe",
    "ffmpeg": (
      platform === 'linux' ? '/usr/bin/ffmpeg' :
      platform === 'win32' ? 'C:/Program Files/FFmpeg/bin/original/ffmpeg.exe' :
      null
    ),
    "z3": "C:/Program Files/Z3/bin/z3.exe",
    "cvc": "C:/Program Files/CVC4/cvc4.exe",
    "sat": "C:/Projects/Other/cadical/cadical.exe",
  },

  "isTravis": null,
  "pDir": "E:/RECOVERED/#Root/MSI21008.tmp",
};