"use strict"

global.isElectron = 1

const fs = require("fs")
const path = require("path")
const O = require("../js-util")
const assert = require("../assert")
const media = require("../media")
const log_status = require("../log-status")
const format = require("../format")
const physics = require("./physics")

const HD = 1

const w = HD ? 1920 : 640
const h = HD ? 1080 : 480
const fps = 60
const fast = !HD

const [wh, hh] = [w, h].map(a => a >> 1)
const [w1, h1] = [w, h].map(a => a - 1)

const duration = 60 * 60
const framesNum = fps * duration

const main = async () => {
  // const out_file = get_out_file()
  const out_file = "/home/user/Git/Render/1.mp4"
  
  physics.main(w, h)
  
  media.renderVideo(out_file, w, h, fps, fast, (w, h, g, f) => {
    log_status(f, framesNum)

    physics.tick()
    
    if(f === framesNum) return false
    if(f % fps === 0 && O.rfs("/home/user/Downloads/1.txt", 1) === "stop")
      return false
    
    return true
  }, () => {
    window.close()
  })
}

const get_out_file = (vid=0) => {
  if(vid || !HD) return format.path("-render/1.mp4")
  const project = path.parse(__dirname).name
  return format.path(`-render/${project}.mp4`)
}

main().catch(log)