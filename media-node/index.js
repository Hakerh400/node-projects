"use strict"

const fs = require("fs")
const path = require("path")
const cp = require("child_process")
const O = require("../js-util")
const assert = require("../assert")
const config = require("../config")

const renderVideo = (output, w, h, fps, fast, frameFunc=O.nop, exitCb, options={}) => {
  const pixelFormat = "-f rawvideo -pix_fmt rgba"
  const videoPreset = `-preset slow -profile:v high -coder 1
    -movflags +faststart -bf 2 -c:a aac -b:a 384k
    -profile:a aac_low -max_muxing_queue_size 100000
    -global_quality 18 -look_ahead 1 -pix_fmt nv12`.replace(/\s+/g, " ")
  const fastPreset = `-c:v h264_qsv ${videoPreset}`
  const hdPreset = `-c:v h264_qsv ${videoPreset}`
  const trunc = ""
  const vflip = 0
  
  const proc = spawnFfmpeg(`${pixelFormat} -s ${w}x${h} -framerate ${
    fps} -i - -y -framerate ${fps}${
    vflip ? " -vf vflip" : ""
  } ${
    fast ? fastPreset : hdPreset
  } ${trunc} "${output}"`, exitCb)
  
  const s = proc.stdin
  const buf = Buffer.alloc(w * h << 2)
  const imgd = new O.ImageData()
  
  imgd.w = w
  imgd.h = h
  imgd.d = buf
  
  const col = Buffer.alloc(4)
  col[3] = 255
  imgd.iter(() => col, 1)
  
  let f = 0
  
  const frame = () => {
    let value = frameFunc(w, h, imgd, ++f);
    let buf;

    if(value instanceof Buffer){
      buf = value;
    }else if(value === null){
      f--;
      setTimeout(frame);
      return;
    }else if(value === -1){
      s.end();
      return;
    }else{
      buf = imgd.d
    }

    if(value) s.write(buf, frame);
    else s.end(buf);
  }
  
  frame()
}

const spawnFfmpeg = (args, exitCb) => {
  return spawnProc("ffmpeg", args, exitCb)
}

const spawnProc = (name, args, exitCb=O.nop) => {
  const fg_dir = path.join(config.exe.ffmpeg, '..');
  
  name = path.join(fg_dir, name)

  args = [
    "-hide_banner",
    ...args.match(/"[^"]*"|\S+/g).map(a => a[0] == '"' ? a.substring(1, a.length - 1) : a)
  ]

  const options = {
    windowsHide: false,
  }

  const proc = cp.spawn(name, args, options)

  proc.stderr.on("data", a => {O.nop})
  proc.on("exit", code => exitCb(code))
  
  proc.stdin.on("error", a => {O.nop})
  proc.stdout.on("error", a => {O.nop})
  proc.stderr.on("error", a => {O.nop})
  proc.on("error", a => {O.nop})
  
  return proc
}

module.exports = {
  renderVideo,
}