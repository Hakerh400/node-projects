'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('assert');
const O = require('../omikron');

const {
  min, max, abs, floor,
  ceil, round, sqrt, sin, cos,
  atan, atan2, clz32,
  log2,
} = Math;

const n = 1e7;
const fac = 1 / 5;
const threshold = 1e-3;

const main = () => {
  const d = O.obj();
  
  const put = function*(i, n){
    if(!O.has(d, i)) d[i] = 0;
    
    const a = n * fac;
    const b = (n - a) / 2;
    
    d[i] += a;
    
    if(n < 1) return;
    
    yield [put, i - 1, b];
    yield [put, i + 1, b];
  };
  
  O.rec(put, 0, n);
  
  const inds = O.keys(d).map(a => a | 0);
  const indMin = inds.reduce((a, b) => min(a, b), O.N);
  const indsNum = inds.length;
  
  let arr = O.ca(indsNum, i => {
    return d[indMin + i];
  });
  
  // const arrMax = arr.reduce((a, b) => max(a, b), 0);
  
  arr = arr.map(a => {
    return a / n * 1e3;
  });
  
  while(arr.length !== 0 && arr[0] < threshold)
    arr.shift();
  
  while(arr.length !== 0 && O.last(arr) < threshold)
    arr.pop();
  
  log(arr.join('\n'));
};

main();