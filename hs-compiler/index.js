'use strict';

const fs = require('fs');
const path = require('path');
const cp = require('child_process');
const O = require('../js-util');
const assert = require('../assert');

fs.rmSync = (pth, opts) => {
  cp.spawnSync('rm', ['-rf', pth]);
};

const {platform} = process;

const flag_force_reset_dir = 1;

const cwd = __dirname;
const proj_name = path.parse(cwd).base;
const test_dir = path.join(cwd, '../../test');
const data_dir = path.join(test_dir, proj_name);
const info_file = path.join(data_dir, 'info.json');
const hs_src_dir = path.join(data_dir, 'src');
const hs_build_dir = path.join(data_dir, 'build');
const main_src_file_pth = path.join(hs_src_dir, 'Main.hs');

const tab_size = 2;
const tab = ' '.repeat(tab_size);

let info = null;

const init = () => {
  if(!flag_force_reset_dir && O.exi(info_file)){
    const s = O.rfs(info_file, 1);
    info = JSON.parse(s);
    return;
  }

  O.reset_dirs([
    data_dir,
    hs_src_dir,
    hs_build_dir,
  ]);

  info = {
    files_num: 0,
    files_mp: {},
  };

  save_info();
};

const save_info = () => {
  O.wfs(info_file, JSON.stringify(info));
};

const compile = pth => {
  const index = transpile(pth);
  const module_name = calc_module_name(index);
  const exe_file_pth = calc_exe_file_pth(index);

  const main_src = `import ${module_name}`;
  O.wfs(main_src_file_pth, main_src);

  const res = cp.spawnSync('ghc', [
    '-O',
    '-outputdir', hs_build_dir,
    '-o', exe_file_pth,
    '-rtsopts=ignoreAll',
    main_src_file_pth,
  ], {
    cwd: hs_src_dir,
  });

  if(res.status !== 0){
    log(res.stdout + res.stderr);
    return null;
  }

  return exe_file_pth;
};

const transpile = pth => {
  const pth_orig = pth;

  const {files_mp: mp} = info;

  if(!O.exi(pth) && path.parse(pth).ext !== '.hs')
    pth += '.hs';

  const stat = fs.statSync(pth);

  if(stat.isDirectory())
    pth = path.join(pth, 'index.hs');

  const key = norm_path(pth);
  const has_pth = O.has(mp, key);
  
  const pth_info = has_pth ? mp[key] : null;
  const time = stat.mtimeMs;
  const flag = !has_pth || time !== pth_info.time;
  
  const calc_index = () => {
    if(has_pth) return pth_info.index;
    return info.files_num++;
  };
  
  const index = calc_index();

  if(!has_pth)
    mp[key] = {time, index};

  if(!flag) return index;

  // Cache miss

  const module_name = calc_module_name(index);
  const file_pth = calc_src_file_pth(index);

  let str = O.rfs(pth, 1);

  str = O.lf(str).
    replace(/\t/g, tab).
    replace(/^ +$/g, '');
    // replace(/^module\b.*?\n(?:\n|$)/gm, '');
  
  const is_native = /^import [A-Z]/.test(str);
  const is_hsx = !is_native;

  let lines = O.sanl(str);

  const modules = [index];

  lines = lines.flatMap(line => {
    let m;

    if(m = line.match(/^import\s*\"(.*?)\"$/)){
      assert(is_hsx);

      const pth_new_rel = m[1];
      const pth_new = path.join(pth, '..', pth_new_rel);
      const i = transpile(pth_new);
      const module_name = calc_module_name(i);

      modules.push(i);

      return `import ${module_name}`;
    }

    return [line];
  });

  str = lines.join('\n');
  O.sortAsc(modules);

  const modules_num = modules.length;
  let s = `module ${module_name}\n${tab}(`;

  for(let i = 0; i !== modules_num; i++){
    if(i !== 0) s += `,`;
    s += ` module ${calc_module_name(modules[i])}\n${tab}`;
  }

  s += `) where`;
  str = `${s}\n\n${str}`;

  str = str.trim().replace(/\n\s*\n/g, '\n\n');
  O.wfs(file_pth, str.trim());

  info[key] = {
    index,
    time,
  };

  save_info();

  return index;
};

const calc_module_name = index => {
  return `A${index}`;
};

const calc_src_file_name = index => {
  const module_name = calc_module_name(index);
  return `${module_name}.hs`;
};

const calc_src_file_pth = index => {
  const file_name = calc_src_file_name(index);
  return path.join(hs_src_dir, file_name);
};

const calc_exe_file_name = index => {
  const module_name = calc_module_name(index);
  
  const ext = platform === 'linux' ? '' :
    platform === 'win32' ? '.exe' :
    assert.fail();
  
  return `${module_name}${ext}`;
};

const calc_exe_file_pth = index => {
  const file_name = calc_exe_file_name(index);
  return path.join(hs_build_dir, file_name);
};

const norm_path = pth => {
  return path.posix.normalize(pth).
    replace(/\\/g, '/');
};

init();

module.exports = {
  compile,
};