'use strict';

const fs = require('fs');
const path = require('path');
const cp = require('child_process');
const O = require('../js-util');
const assert = require('../assert');
const hs_compiler = require('.');

const cwd = __dirname;
const hs_dir = path.join(cwd, 'hs');
const main_src_file = path.join(hs_dir, 'main');

const main = () => {
  const exe_pth = hs_compiler.compile(main_src_file);
  if(exe_pth === null) return;

  const res = cp.spawnSync(exe_pth);
  fs.writeSync(process.stdout.fd, res.stdout);
};

main();