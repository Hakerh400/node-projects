'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('assert');
const O = require('../omikron');
const logStatus = require('../log-status');
const CSP = require('.');

Error.stackTraceLimit = 1e9;

if(0){
  const seed = O.rand(1e9);
  
  log(seed);
  O.logb();
  
  O.enhanceRNG();
  O.randSeed(seed);
}

const main = async () => {
  {
    // return await sudoku();
    
    // const csp = new CSP();
    // 
    // const vars = csp.var([3, 1]);
    // const [a, b, c] = vars.map(a => a[0]);
    // 
    // const f = (a, b) => {
    //   return csp.and(csp.not(a), b);
    // };
    // 
    // // csp.assert(f(a, f(b, a)));
    // // csp.assert(f(f(a, f(b, c)), f(f(a, b), f(a, c))));
    // csp.assert(csp.andv([
    //   csp.not(f(a, b)),
    //   csp.not(a),
    //   b,
    // ]));
    // 
    // const sol = await csp.solve(vars);
    // 
    // if(sol === null){
    //   log(null);
    //   return;
    // }
    // 
    // log(sol.join(''));
    // 
    // return;
    
    // {
    //   const csp = new CSP();
    // 
    //   const targetWeight = 40;
    //   const weightsNum = 4;
    // 
    //   const wSizeM = bitsNum(targetWeight);
    //   const wSize = bitsNum(targetWeight * weightsNum);
    //   const w0 = csp.int(wSize, 0);
    // 
    //   const difs = csp.var([weightsNum, wSize]);
    //   const constructions = csp.var([targetWeight, weightsNum, 2]);
    //   const weights = [];
    //   const weightsNeg = [];
    // 
    //   const add = (a, b, strict=0) => {
    //     if(!strict)
    //       return csp.add(a, b);
    // 
    //     const a1 = [...a, csp.b0];
    //     const b1 = [...b, csp.b0];
    //     const c = csp.add(a1, b1);
    // 
    //     csp.assert(csp.not(c.pop()));
    //     assertValidWeight(c);
    // 
    //     return c;
    //   };
    // 
    //   const assertValidWeight = w => {
    //     csp.assert(csp.norv(w.slice(wSizeM)));
    //     csp.assert(csp.imp(w[5], csp.not(w[4])));
    //     csp.assert(csp.imp(csp.and(w[5], w[3]), csp.norv(w.slice(0, 3))));
    //   };
    // 
    //   let w = w0;
    // 
    //   difs.forEach((dif, i) => {
    //     assertValidWeight(dif);
    //     w = add(w, dif, 1);
    //     weights.push(w);
    // 
    //     // weightsNeg.push(csp.sub(w0, w));
    //     weightsNeg.push(csp.inc(csp.not(w)));
    //   });
    // 
    //   constructions.forEach((con, i) => {
    //     let w = w0;
    // 
    //     con.forEach(([bPos, bNeg], j) => {
    //       csp.assert(csp.nand(bPos, bNeg));
    // 
    //       const wPos = weights[j].map(a => csp.and(a, bPos));
    //       const wNeg = weightsNeg[j].map(a => csp.and(a, bNeg));
    // 
    //       w = add(w, csp.or(wPos, wNeg));
    //     });
    // 
    //     csp.assert(csp.eq(w, csp.int(wSize, i + 1)));
    //   });
    // 
    //   const sol = await csp.solve(O.concat([weights, ...constructions]), 0);
    // 
    //   if(sol === null){
    //     log(null);
    //     return;
    //   }
    // 
    //   const ws = sol.slice(0, weightsNum);
    // 
    //   log(ws.join(' '));
    //   log();
    // 
    //   for(let i = 0; i !== targetWeight; i++){
    //     const offset = weightsNum + i * weightsNum;
    //     log(`${`${i + 1}:`.padEnd(5)}${
    //       sol.slice(offset, offset + weightsNum).
    //       map((a, i) => a ? ws[i] * (a === 1 ? 1 : -1) : null).
    //       filter(a => a !== null).join(' ')}`);
    //   }
    // 
    //   return;
    // }
    
    // return await connectedShapes();
  }
  
  const csp = new CSP();
  
  const size          = 5
  const movesNum      = 50
  const exactBoxesNum = null
  
  const w = size;
  const h = size;
  const w1 = w - 1;
  const h1 = h - 1;
  const area = w * h;
  
  const areaSize = bitsNum(area);
  const tileSize = 4;
  const moveSize = 2;
  const shape = [h, w, tileSize];
  const grid = csp.var(shape);
  const grids = [grid];
  const moves = csp.var([movesNum, moveSize]);
  
  const iter = f => {
    for(let y = 0; y !== h; y++)
      for(let x = 0; x !== w; x++)
        f(x, y);
  };
  
  const bitsObj = {
    '0000': ' ',
    '1000': '@',
    '1010': '%',
    '0100': '+',
    '0110': '*',
    '0010': '.',
    '0001': '#',
  };
  
  const validVals = O.keys(bitsObj).map(a => {
    return csp.int(tileSize, bitsToInt([...a].map(a => a | 0)));
  });
  
  const isValid = tile => {
    return csp.orv(validVals.map(a => {
      return csp.eq(tile, a);
    }));
  };
  
  const f = (csp, grid) => {
    let playersNum = csp.int(areaSize, 0);
    let boxesNum = csp.int(areaSize, 0);
    let goalsNum = csp.int(areaSize, 0);
    
    iter((x, y) => {
      const tile = grid[y][x];
      csp.assert(isValid(tile));
      
      playersNum = csp.inc(playersNum, tile[0]);
      boxesNum = csp.inc(boxesNum, tile[1]);
      goalsNum = csp.inc(goalsNum, tile[2]);
    });
    
    // Only one player
    csp.assert(csp.eq(playersNum, csp.int(areaSize, 1)));
    
    // Same number of boxes and goals
    csp.assert(csp.eq(boxesNum, goalsNum));
    
    if(exactBoxesNum !== null){
      assert(exactBoxesNum <= area);
      csp.assert(csp.eq(boxesNum, csp.int(areaSize, exactBoxesNum)));
    }
    
    let iterGrid = grid;
    
    const isSolved = grid => {
      const ands = [];
      
      iter((x, y) => {
        const box = grid[y][x][1];
        const goal = grid[y][x][2];
        
        ands.push(csp.eq(box, goal));
      });
      
      return csp.andv(ands);
    };
    
    const inside = (x, y) => {
      return (
        x >= 0 && y >= 0 &&
        x < w && y < h
      );
    };
    
    const ds = [[0, -1], [1, 0], [0, 1], [-1, 0]];
    
    for(const move of moves){
      csp.assert(csp.not(isSolved(iterGrid)));
      
      const dirs = O.ca(4, i => csp.eq(move, csp.int(moveSize, i)));
      const playerVars = [];
      
      iterGrid = O.ca(h, y => O.ca(w, x => {
        const goal = iterGrid[y][x][2];
        const wall = iterGrid[y][x][3];
        const playerNew = csp.var();
        const boxNew = csp.var();
        const tile = [playerNew, boxNew, goal, wall];
        
        csp.assert(isValid(tile));
        playerVars.push(playerNew);
        
        dirs.forEach((dir, di) => {
          const [dx, dy] = ds[di];
          
          const get = (rel, k) => {
            const x1 = x + dx * rel;
            const y1 = y + dy * rel;
            if(!inside(x1, y1)) return csp.b0;
            
            return iterGrid[y1][x1][k];
          };
          
          const ands = [];

          // Player moved
          ands.push(csp.eq(playerNew, get(-1, 0)));
          
          // Box moved
          ands.push(csp.eq(boxNew, csp.and(
            csp.not(playerNew),
            csp.or(get(0, 1), csp.and(get(-2, 0), get(-1, 1))),
          )));
          
          csp.assert(csp.imp(dir, csp.andv(ands)));
        });
        
        return tile;
      }));
      
      grids.push(iterGrid);
      
      // Player did not disappear
      csp.orv(playerVars);
    }
    
    // All tiles must be used
    iter((x, y) => {
      const [playerInit, boxInit, goal, wall] = grid[y][x];
      
      csp.assert(csp.imp(csp.not(wall), csp.orv(grids.map(iterGrid => {
        const [player, box] = iterGrid[y][x];
        
        return csp.or(
          csp.neq(player, playerInit),
          csp.neq(box, boxInit),
        );
      }))));
    });
    
    // Same state is not reached two times
    csp.assert(csp.neqv(grids.map(grid => {
      return grid.map(row => {
        return row.map(tile => tile.slice(0, 2));
      });
    })));
    
    // At the end, the grid is solved
    csp.assert(isSolved(iterGrid));
  };
  
  f(csp, grid);
  
  const vars = [];
  iter((x, y) => vars.push(grid[y][x]));
  
  for(const move of moves)
    vars.push(move);
  
  const sol = await csp.solve(vars, area);
  
  if(sol === null){
    log(null);
    return;
  }
  
  const finalGrid = O.ca(h, y => O.ca(w, x => sol[y * w + x]));
  
  if(0){
    logStatus.reset();
    
    const order = O.shuffle(O.concat(O.ca(h, y => O.ca(w, x => [x, y]))));
    const tilesNum = order.length;
    
    for(let i = 0; i !== tilesNum; i++){
      logStatus(i + 1, tilesNum, 'tile');
      
      const [x, y] = order[i];
      
      const csp = new CSP();
      const grid1 = csp.var(shape);
      
      f(csp, grid1);
      
      const ands = [];
      const ors = [];
      
      iter((x1, y1) => {
        const tile = grid1[y1][x1];
        const fgV = finalGrid[y1][x1];
        
        if(fgV !== null && !(x1 === x && y1 === y)){
          ands.push(fgV ? tile : csp.not(tile));
          return;
        }
        
        ors.push(sol[y1 * w + x1] ? csp.not(tile) : tile);
      });
      
      if(ands.length !== 0)
        csp.assert(csp.andv(ands));
      
      if(ors.length !== 0)
        csp.assert(csp.orv(ors));
      
      if(await csp.solve([]))
        continue;
      
      finalGrid[y][x] = null;
    }
    
    O.logb();
  }
  
  const valToStr = v => {
    if(v === null) return '~';
    
    const player = v & 1 ? 1 : 0;
    const box = v & 2 ? 1 : 0;
    const goal = v & 4 ? 1 : 0;
    const wall = v & 8 ? 1 : 0;
    const bits = [player, box, goal, wall].join('');
    
    if(O.has(bitsObj, bits))
      return bitsObj[bits];
    
    assert.fail();
  };
  
  log(finalGrid.map((row, y) => `|${row.map((a, x) => {
    return valToStr(sol[y * w + x]);
  }).join('')}|`).join('\n'));
  log();
  log(moves.map((a, i) => sol[area + i]).join(''));
  // log();
  // log(finalGrid.map(row => row.map(v => {
  //   return valToStr(v);
  // }).join('')).join('\n'));
};

const connectedShapes = async () => {
  const csp = new CSP();
  // return await sudoku(csp);
  
  // {
  //   const a = csp.var();
  //   const b = csp.var();
  // 
  //   csp.assert(csp.neq(a, b))
  // 
  //   const sol = await csp.solve([[a], [b]]);
  // 
  //   if(sol === null){
  //     log(null);
  //     return;
  //   }
  // 
  //   log(sol.join(' '))
  // 
  //   return;
  // }
    
  // {
  //   const a = csp.var();
  //   const b = csp.var([10]);
  // 
  //   csp.assert(csp.eq(b, csp.if(a,
  //     csp.int(10, 123),
  //     csp.int(10, 45),
  //   )));
  // 
  //   await csp.solve();
  // 
  //   log(a.val, b.val)
  //   return;
  // }
  
  const size = 20;
  const w = size;
  const h = size;
  const w1 = w - 1;
  const h1 = h - 1;
  const area = w * h;
  const intSize = Math.ceil(Math.log2(area));
  
  const shape = [h, w];
  const shape2 = [...shape, 2];
  const grid = csp.var(shape);
  
  const iter = f => {
    for(let y = 0; y !== h; y++)
      for(let x = 0; x !== w; x++)
        f(x, y);
  };
  
  const isConnected = (csp, grid) => {
    // csp.assert(csp.not(grid[0][0]));
    // csp.assert(grid[0][1]);
    // 
    // let iterGrid = O.ca(h, y => O.ca(w, x => {
    //   const isActive = csp.bit(y === 0 && x <= 1);
    //   const state = csp.not(isActive);
    // 
    //   return [state, isActive];
    // }));
    // 
    // for(let i = 0; i !== area; i++){
    //   iterGrid = O.ca(h, y => O.ca(w, x => {
    //     const col = grid[y][x];
    //     const [state, isActive] = iterGrid[y][x];
    //     const adjs = [];
    // 
    //     if(x !== 0) adjs.push([x - 1, y]);
    //     if(y !== 0) adjs.push([x, y - 1]);
    //     if(x !== w1) adjs.push([x + 1, y]);
    //     if(y !== h1) adjs.push([x, y + 1]);
    // 
    //     const isActiveNew = csp.orv(adjs.map(([x, y]) => {
    //       const adjCol = grid[y][x];
    //       const isAdjActive = iterGrid[y][x][1];
    // 
    //       return csp.and(isAdjActive, csp.eq(adjCol, col));
    //     }));
    // 
    //     const stateNew = csp.and(state, csp.not(isActiveNew));
    // 
    //     return [stateNew, isActiveNew];
    //   }));
    // }
    // 
    // const finalIterStates = [];
    // 
    // iter((x, y) => {
    //   finalIterStates.push(iterGrid[y][x][0]);
    // });
    // 
    // csp.assert(csp.norv(finalIterStates));
    
    const blackIndex = csp.var([intSize]);
    const whiteIndex = csp.var([intSize]);
    const distsGrid = csp.var([h, w, intSize]);
    
    csp.assert(csp.neq(blackIndex, whiteIndex));
    // csp.assert(csp.eq(distsGrid[0][0], csp.int(intSize, 0)));
    // csp.assert(csp.eq(distsGrid[0][1], csp.int(intSize, 0)));
    
    const intSize2 = Math.ceil(Math.log2((w + h) * 2 - 4));
    let edgeOppositeCols = csp.int(intSize2, 0);
    
    iter((x, y) => {
      const index = csp.int(intSize, y * w + x);
      const isBlack = grid[y][x];
      const dist = distsGrid[y][x];
      const isNonZero = csp.orv(dist);
      const caseZero = csp.eq(index, csp.if(isBlack, blackIndex, whiteIndex));
    
      const adjs = [];
    
      if(x !== 0) adjs.push([x - 1, y]);
      if(y !== 0) adjs.push([x, y - 1]);
      if(x !== w1) adjs.push([x + 1, y]);
      if(y !== h1) adjs.push([x, y + 1]);
    
      const incDist = csp.inc(dist);
      const decDist = csp.dec(dist);
    
      const caseNonZero = csp.orv(adjs.map(([x1, y1]) => {
        const isAdjBlack = grid[y1][x1];
        const adjDist = distsGrid[y1][x1];
        const sameCol = csp.eq(isAdjBlack, isBlack);
    
        if(x1 < x || y1 < y){
          csp.assert(csp.imp(sameCol, csp.or(
            csp.eq(adjDist, incDist),
            csp.eq(adjDist, decDist),
          )));
        }
    
        return csp.and(sameCol, csp.eq(adjDist, decDist));
      }));
    
      csp.assert(csp.ifp(isNonZero, caseNonZero, caseZero));
    
      // Extract check 1: opposite colors diagonally
      {
        if(x !== w1 && y !== h1){
          // a b
          // c d
          const a = grid[y][x];
          const b = grid[y][x + 1];
          const c = grid[y + 1][x];
          const d = grid[y + 1][x + 1];
          
          csp.assert(csp.nandv([a, d, csp.not(b), csp.not(c)]));
          csp.assert(csp.nandv([b, c, csp.not(a), csp.not(d)]));
        }
      }
      
      // Extra check 2: opposite colors at edges
      {
        if((y === 0 || y === h1) && x !== w1){
          const isOpp = csp.neq(isBlack, grid[y][x + 1]);
          edgeOppositeCols = csp.inc(edgeOppositeCols, isOpp);
        }
        
        if((x === 0 || x === w1) && y !== h1){
          const isOpp = csp.neq(isBlack, grid[y + 1][x]);
          edgeOppositeCols = csp.inc(edgeOppositeCols, isOpp);
        }
      }
    });
    
    csp.assert(csp.orv([
      csp.eq(edgeOppositeCols, csp.int(intSize2, 0)),
      csp.eq(edgeOppositeCols, csp.int(intSize2, 2)),
    ]));
    
    // const vars = [];
    // iter((x, y) => vars.push([grid[y][x]]));
    // // iter((x, y) => vars.push(distsGrid[y][x]));
    // 
    // const sol = await csp.solve(vars);
    // 
    // log(grid.map((row, y) => row.map((a, x) => {
    //   const v = sol[y * w + x];
    //   return '.#'[v];
    // }).join('')).join('\n'));
    // // log();
    // // log(grid.map((row, y) => row.map((a, x) => {
    // //   const n = sol[w * h + y * w + x];
    // //   return String(n).padStart(2);
    // // }).join(' ')).join('\n'));
    // 
    // O.exit();
  };
  
  const f = (csp, grid) => {
    isConnected(csp, grid)//.catch(O.exit);
  };
  
  f(csp, grid);
  
  O.sanl(`
  ~~~~~~~~~~~~~~~~~~~~
  ~~~#~~~~~~~~~~~~~~~~
  ~~~~~~~~~~~~~~~~~~~~
  ~~~~~~~~~~~~#~~~~~~~
  #~~~~.~.~~~~~~~~~~~~
  ~~~~~~~~~~~~~~~~~~~~
  ~~~~~~~~~~~~~~~~~~~~
  ~~~~~~~~~~~~~~~~~~~~
  ~~~.~~~~~~~~~~~~~~~~
  ~~~~~~~#~~~~~~~~~.~~
  ~~~~~~~~~~~~~~~~~~~~
  ~~~~~~~~~~~~~~~~~~~~
  ~~~~~~~~~~~~~~~~~~~~
  ~~~~~~~~~~~.~~~~~~~~
  ~~~~~~~~~~~~~~~~~~~~
  ~~~~~~~~~~~~~~.~~~~~
  ~~~~~~~~~~~~~~~~~~~~
  ~~~~~~~~~~~~~~~~~~~~
  ~~.~~~~~~~~~~.~~~~~~
  .~~~~~~~~~~~~~~~~~~~
  `.trim()).map((a, y) => a.trim().split('').map((a, x) => {
    if(a === '~') return;
    const v = a === '#' ? 1 : 0;
    const k = grid[y][x];
    csp.assert(v ? k : csp.not(k));
  }));
  
  const tiles = [];
  iter((x, y) => tiles.push([grid[y][x]]));
  
  let sol;
  
  if(0){
    sol = O.ca(area, i => {
      const x = i % w;
      const y = i / w | 0;
      
      if(x === 0 || y === 0 || x === w1)
      return 0;
      
      if(y === h1 || x === (w >> 1) && y >= h / 2)
      return 1;
      
      return O.dist(x, y, w / 2, h / 2) < (w + h) / 6 ? 1 : 0;
    });
  }else{
    sol = await csp.solve(tiles);
  }
  
  if(sol === null){
    log(null);
    return;
  }
  
  const finalGrid = O.ca(h, y => O.ca(w, x => sol[y * w + x]));
  
  if(1){
    logStatus.reset();
    
    const order = O.shuffle(O.concat(O.ca(h, y => O.ca(w, x => [x, y]))));
    const tilesNum = order.length;
    
    for(let i = 0; i !== tilesNum; i++){
      logStatus(i + 1, tilesNum, 'tile');
      
      const [x, y] = order[i];
      
      const csp = new CSP();
      const grid1 = csp.var(shape);
      
      f(csp, grid1);
      
      const ands = [];
      const ors = [];
      
      iter((x1, y1) => {
        const tile = grid1[y1][x1];
        const fgV = finalGrid[y1][x1];
        
        if(fgV !== null && !(x1 === x && y1 === y)){
          ands.push(fgV ? tile : csp.not(tile));
          return;
        }
        
        ors.push(sol[y1 * w + x1] ? csp.not(tile) : tile);
      });
      
      if(ands.length !== 0)
        csp.assert(csp.andv(ands));
      
      if(ors.length !== 0)
        csp.assert(csp.orv(ors));
      
      if(await csp.solve([]))
        continue;
      
      finalGrid[y][x] = null;
    }
    
    O.logb();
  }
  
  log(finalGrid.map((row, y) => row.map((a, x) => {
    const v = sol[y * w + x];
    return '.#'[v];
  }).join('')).join('\n'));
  log();
  log(finalGrid.map(row => row.map(v => {
    if(v === null) return '~';
    return '.#'[v];
  }).join('')).join('\n'));
};

const sudoku = async () => {
  const csp = new CSP();
  
  const m = 4;
  const n = m ** 2;
  const w = n;
  const h = n;
  
  const iter = f => {
    for(let y = 0; y !== h; y++)
      for(let x = 0; x !== w; x++)
        f(x, y);
  };
  
  const grid = csp.var([n, n, 4]);
  
  if(m === 3){
    for(let y = 0; y !== n; y++){
      for(let x = 0; x !== n; x++){
        const a = grid[y][x];
        csp.assert(csp.imp(a[3], csp.not(csp.orv(a.slice(0, 3)))));
      }
    }
  }
  
  for(let y = 0; y !== n; y++)
    csp.assert(csp.neqv(grid[y]));
  
  for(let x = 0; x !== n; x++)
    csp.assert(csp.neqv(grid.map(a => a[x])));
  
  for(let y = 0; y !== n; y += m){
    for(let x = 0; x !== n; x += m){
      const arr = [];
  
      for(let j = 0; j !== m; j++)
        for(let i = 0; i !== m; i++)
          arr.push(grid[y + j][x + i]);
  
      csp.assert(csp.neqv(arr));
    }
  }
  
  const tiles = [];
  iter((x, y) => tiles.push(grid[y][x]));
  
  const sol = await csp.solve(tiles);
  
  if(sol === null){
    log(null);
    return;
  }
  
  if(m === 3){
    log(grid.map((row, y) => row.map((a, x) => String(sol[y * w + x] + 1).padStart(1)).join('')).join('\n'));
  }else{
    log(grid.map((row, y) => row.map((a, x) => String(sol[y * w + x] + 1).padStart(2)).join(' ')).join('\n\n'));
  }
};

const bitsToInt = bits => {
  return bits.reduceRight((a, b) => (a << 1) | b, 0);
};

const bitsNum = n => {
  return Math.ceil(Math.log2(n));
};

main().catch(log);