'use strict';

const fs = require('fs');
const path = require('path');
const cp = require('child_process');
const assert = require('assert');
const O = require('../omikron');
const config = require('../config');
const logStatus = require('../log-status');
const format = require('../format');
const debug = require('../debug');

const DEBUG = 0;
const ENABLE_CACHE = 0;

const isScalar = a => {
  return !Array.isArray(a);
};

const opNames = [
  'not',
  'and',
  'or',
];

class CSP{
  unsat = 0;
  clauses = new Map();
  idents = new Map();
  activeIdents = new Set();
  activeClauses = new Set();
  assignments = new Map();
  relations = new Map();
  
  constructor(){
    const opsCache = this.opsCache = O.obj();
    
    for(const name of opNames)
      opsCache[name] = new Map();
    
    const b1 = this.b1 = this.var();
    const b0 = this.b0 = this.not(b1);
    
    this.assert(b1);
    this.bits = [b0, b1];
  }
  
  mkSym(){
    const sym = mkSym(this);
    this.activeIdents.add(sym);
    return sym;
  }
  
  var(shape){
    return O.rec([this, 'varRec'], shape);
  }
  
  *varRec(shape=null){
    const csp = this;
    
    if(shape === null || shape.length === 0){
      const {idents} = this;
      const k = this.mkSym();
      
      idents.set(k, O.ca(2, () => new Set()));
      
      return k;
    }
    
    const shapeLen = shape.length;
    const [n, ...shapeNew] = shape;
    const arr = [];
    
    for(let i = 0; i !== n; i++)
      arr.push(yield [[this, 'varRec'], shapeNew]);
    
    return arr;
  }
  
  // getVal(k){
  //   const {sol} = this;
  //   if(sol === null) return null;
  //   return sol.get(k);
  // }
  
  not(a){
    return O.rec([this, 'notRec'], a);
  }
  
  *notRec(a){
    if(isScalar(a)){
      let cache;
      
      if(ENABLE_CACHE){
        cache = this.opsCache.not;
        
        if(cache.has(a))
          return cache.get(a);
      }
      
      const {clauses} = this;
      const b = this.var();
      
      this.addClause([a, 0, b, 0]);
      this.addClause([a, 1, b, 1]);
      
      if(ENABLE_CACHE){
        cache.set(a, b);
        cache.set(b, a);
      }
      
      return b;
    }
    
    const arr = [];
    
    for(const x of a)
      arr.push(yield [[this, 'notRec'], x]);
    
    return arr;
  }
  
  *binOpRec(a, b, f){
    if(isScalar(a)){
      assert(isScalar(b));
      return f(a, b)
    }
    
    assert(!isScalar(b));
    
    const len = a.length;
    assert(b.length === len);
    
    const arr = [];
    
    for(let i = 0; i !== len; i++){
      const x = a[i];
      const y = b[i];
      
      arr.push(yield [[this, 'binOpRec'], x, y, f]);
    }
    
    return arr;
  }
  
  ands(a, b){
    assert(isScalar(a));
    assert(isScalar(b));
    
    if(a === b)
      return a;
    
    let cache, cacheA;
    
    if(ENABLE_CACHE){
      if(cmpSym(a, b) > 0){
        const t = a;
        a = b;
        b = t;
      }
      
      cache = this.opsCache.and;
      
      if(!cache.has(a))
        cache.set(a, new Map());
      
      cacheA = cache.get(a);
      
      if(cacheA.has(b))
        return cacheA.get(b);
    }
    
    const {clauses} = this;
    const c = this.var();
    
    this.addClause([c, 1, a, 0, b, 0]);
    this.addClause([c, 0, a, 1]);
    this.addClause([c, 0, b, 1]);
    
    if(ENABLE_CACHE)
      cacheA.set(b, c);
    
    return c;
  }
  
  and(a, b){
    return O.rec([this, 'binOpRec'], a, b, this.ands.bind(this));
  }
  
  andv(arr){
    if(arr.length === 0)
      return this.b1;
    
    return arr.reduce((a, b) => this.and(a, b));
  }
  
  ors(a, b){
    assert(isScalar(a));
    assert(isScalar(b));
    
    if(a === b)
      return a;
    
    let cache, cacheA;
    
    if(ENABLE_CACHE){
      if(cmpSym(a, b) > 0){
        const t = a;
        a = b;
        b = t;
      }
      
      cache = this.opsCache.and;
      
      if(!cache.has(a))
        cache.set(a, new Map());
      
      cacheA = cache.get(a);
      
      if(cacheA.has(b))
        return cacheA.get(b);
    }
    
    const {clauses} = this;
    const c = this.var();
    
    this.addClause([c, 1, a, 0]);
    this.addClause([c, 1, b, 0]);
    this.addClause([c, 0, a, 1, b, 1]);
    
    if(ENABLE_CACHE)
      cacheA.set(b, c);
    
    return c;
  }
  
  or(a, b){
    return O.rec([this, 'binOpRec'], a, b, this.ors.bind(this));
  }
  
  orv(arr){
    if(arr.length === 0)
      return this.b0;
    
    return arr.reduce((a, b) => this.or(a, b));
  }
  
  eqs(a, b){
    assert(isScalar(a));
    assert(isScalar(b));
    
    if(a === b)
      return this.b1;
    
    const {clauses} = this;
    const c = this.var();
    
    this.addClause([c, 0, a, 1, b, 0]);
    this.addClause([c, 0, a, 0, b, 1]);
    this.addClause([c, 1, a, 1, b, 1]);
    this.addClause([c, 1, a, 0, b, 0]);
    
    return c;
    // assert(isScalar(a));
    // assert(isScalar(b));
    // 
    // return this.or(
    //   this.and(a, b),
    //   this.and(this.not(a), this.not(b)),
    // );
  }
  
  *eqRec(a, b){
    if(isScalar(a)){
      assert(isScalar(b));
      return this.eqs(a, b);
    }
    
    assert(!isScalar(b));
    
    const len = a.length;
    assert(b.length === len);
    
    const ands = [];
    
    for(let i = 0; i !== len; i++){
      const x = a[i];
      const y = b[i];
      
      ands.push(yield [[this, 'eqRec'], x, y]);
    }
    
    return this.andv(ands);
  }
  
  eq(a, b){
    return O.rec([this, 'eqRec'], a, b);
  }
  
  eqv(arr){
    const len = arr.length;
    const fst = arr[0];
    
    const ands = [];
    
    for(let i = 1; i !== len; i++)
      ands.push(this.eq(fst, arr[i]));
    
    return this.andv(ands);
  }
  
  neq(a, b){
    return this.not(this.eq(a, b));
  }
  
  neqv(arr){
    const len = arr.length;
    const ands = [];
  
    for(let i = 0; i !== len; i++){
      const a = arr[i];
  
      for(let j = i + 1; j !== len; j++){
        const b = arr[j];
        ands.push(this.neq(a, b));
      }
    }
  
    return this.andv(ands);
  }
  
  xors(a, b){
    return this.xor(a, b);
  }
  
  xor(a, b){
    return this.or(
      this.and(a, this.not(b)),
      this.and(this.not(a), b),
    );
  }
  
  xorv(arr){
    assert(arr.length !== 0);
    return arr.reduce((a, b) => this.xor(a, b));
  }
  
  imp(a, b){
    return this.or(this.not(a), b);
  }
  
  nand(a, b){
    return this.not(this.and(a, b));
  }
  
  nor(a, b){
    return this.not(this.or(a, b));
  }
  
  nandv(arr){
    return this.not(this.andv(arr));
  }
  
  norv(arr){
    return this.not(this.orv(arr));
  }
  
  bit(b){
    return this.bits[b ? 1 : 0];
  }
  
  int(size, val){
    const {bits} = this;
    return O.ca(size, i => bits[(val >> i) & 1]);
  }
  
  lt(a, b){
    assert(!isScalar(a));
    assert(!isScalar(b));
    
    const len = a.length;
    
    if(len === 0)
      return this.b0;
    
    assert(b.length === len);
    assert(isScalar(a[0]));
    assert(isScalar(b[0]));
    
    let lt = this.b0;
    
    for(let i = 0; i !== len; i++){
      const x = a[i];
      const y = b[i];
      
      lt = this.or(
        this.and(this.not(x), y),
        this.and(this.eq(x, y), lt),
      );
    }
    
    return lt;
  }
  
  inc(a, c=this.b1){
    assert(!isScalar(a));
    
    const len = a.length;
    
    assert(len !== 0);
    assert(isScalar(a[0]));
    
    const r = [];
    
    for(let i = 0; i !== len; i++){
      const x = a[i];
      
      r.push(this.xor(x, c));
      
      if(i === len - 1) break;
      
      const cNew = this.var();
      this.assert(this.eq(cNew, this.and(x, c)));
      c = cNew;
    }
    
    return r;
  }
  
  dec(a, c=this.b1){
    assert(!isScalar(a));
  
    const len = a.length;
  
    assert(len !== 0);
    assert(isScalar(a[0]));
  
    const r = [];
  
    for(let i = 0; i !== len; i++){
      const x = a[i];
  
      r.push(this.xor(x, c));
  
      if(i === len - 1) break;
  
      const cNew = this.var();
      this.assert(this.eq(cNew, this.and(this.not(x), c)));
      c = cNew;
    }
  
    return r;
  }
  
  add(a, b, c=this.b0){
    assert(!isScalar(a));
    assert(!isScalar(b));
    
    const len = a.length;
    
    assert(len !== 0);
    assert(b.length === len);
    assert(isScalar(a[0]));
    assert(isScalar(b[0]));
    
    const r = [];
    
    for(let i = 0; i !== len; i++){
      const x = a[i];
      const y = b[i];
      
      r.push(this.xorv([x, y, c]));
      
      if(i === len - 1) break;
      
      const cNew = this.var();
      
      this.assert(this.eq(cNew, this.orv([
        this.and(x, y),
        this.and(x, c),
        this.and(y, c),
      ])));
      
      c = cNew;
    }
    
    return r;
  }
  
  sub(a, b){
    return this.add(a, this.not(b));
  }
  
  if(a, b, c){
    assert(isScalar(a));
    
    const shape = this.getShape(b);
    
    return this.or(
      this.and(this.shapeOf(shape, a), b),
      this.and(this.shapeOf(shape, this.not(a)), c),
    );
  }
  
  ifp(a, b, c){
    return this.and(
      this.imp(a, b),
      this.imp(this.not(a), c),
    );
  }
  
  ite(a, b, c){
    return this.if(a, b, c);
  }
  
  shapeOf(shape, bit){
    assert(isScalar(bit));
    return O.rec([this, 'shapeOfRec'], shape, bit);
  }
  
  *shapeOfRec(shape, bit){
    if(shape.length === 0)
      return bit;
    
    const [len, ...shapeNew] = shape;
    const arr = [];
    
    for(let i = 0; i !== len; i++)
      arr.push(yield [[this, 'shapeOfRec'], shapeNew, bit]);
    
    return arr;
  }
  
  shapeOfBit(shape, bit){
    return this.shapeOf(shape, this.bit(bit));
  }
  
  getShape(a){
    const shape = [];
    
    while(!isScalar(a)){
      const len = a.length;
      assert(len !== 0);
      
      shape.push(len);
      a = a[0];
    }
    
    return shape;
  }
  
  assert(a){
    assert(isScalar(a));
    this.addClause([a, 1]);
  }
  
  addRef(k, v, clause){
    const {idents} = this;
    assert(idents.has(k));
    
    const refs = idents.get(k);
    refs[v].add(clause);
  }
  
  removeRef(k, v, clause){
    const {idents} = this;
    assert(idents.has(k));
    
    const refs = idents.get(k)[v];
    assert(refs.has(clause));
    
    refs.delete(clause);
  }
  
  hasClause(clause, exact=0){
    const len = clause.length;
    const set = this.getClauseSet(clause);
    
    if(set === null)
      return 0;
    
    if(exact)
      return set.has(clause);
    
    loop: for(const c of set){
      assert(c.length === len);
      
      for(let i = 0; i !== len; i += 2){
        const v1 = clause[i + 1];
        const v2 = c[i + 1];
        
        if(v1 !== v2)
          continue loop;
      }
      
      return 1;
    }
    
    return 0;
  }
  
  getClauseSet(clause){
    const {clauses} = this;
    const len = clause.length;
    
    assert((len & 1) === 0);
    
    let curMap = clauses;
    
    for(let i = 0; i !== len; i += 2){
      const k = clause[i];
      
      if(!curMap.has(k))
        return null;
      
      curMap = curMap.get(k);
    }
    
    if(!curMap.has(null))
      return null;
    
    return curMap.get(null);
  }
  
  addClause(clause){
    const {clauses} = this;
    const len = clause.length;
    // if(!clause.cid) clause.cid = O.z=-~O.z;
    
    if(len === 0)
      this.unsat = 1;
    
    sortClause(clause);
    this.activeClauses.add(clause);
    
    let curMap = clauses;
    
    for(let i = 0; i !== len; i += 2){
      const k = clause[i];
      const v = clause[i + 1];
      
      this.addRef(k, v, clause);
      
      if(!curMap.has(k))
        curMap.set(k, new Map());
      
      curMap = curMap.get(k);
    }
    
    if(!curMap.has(null))
      curMap.set(null, new Set());
    
    curMap.get(null).add(clause);
  }
  
  removeClause(clause, addActiveIdents=0){
    const {
      clauses,
      activeIdents, activeClauses,
    } = this;
    
    const len = clause.length;
    
    // sortClause(clause);
    // this.activeClauses.add(clause);
    
    activeClauses.delete(clause);
    
    const maps = [clauses];
    
    for(let i = 0; i !== len; i += 2){
      const k = clause[i];
      const v = clause[i + 1];
      
      if(addActiveIdents)
        activeIdents.add(k);
      
      this.removeRef(k, v, clause);
      
      const curMap = O.last(maps);
      assert(curMap.has(k));
      
      maps.push(k, curMap.get(k));
    }
    
    const lastMap = O.last(maps);
    
    assert(lastMap.has(null));
    const set = lastMap.get(null);
    
    if(set.size !== 1){
      set.delete(clause);
      return;
    }
    
    maps.push(null);
    
    while(maps.length !== 0){
      const k = maps.pop();
      const map = maps.pop();
      
      if(map.size !== 1){
        map.delete(k);
        return;
      }
    }
    
    clauses.clear();
  }
  
  removeIdentFromClause(clause, k){
    this.removeClause(clause);
    
    const index = clause.indexOf(k);
    assert(index !== -1);
    
    clause.splice(index, 2);
    this.addClause(clause);
  }
  
  // checkRefs(){
  //   const {varClauseRefs, clauses} = this;
  // 
  //   for(const clause of clauses){
  //     for(const [k, v] of clause){
  //       assert(varClauseRefs.has(k));
  // 
  //       const refs = varClauseRefs.get(k)[v];
  //       assert(refs.has(clause));
  //     }
  //   }
  // }
  
  *getClauses(){
    const csp = this;
    const {clauses} = this;
    
    const getClauses = function*(curMap){
      for(const [k, m] of curMap){
        if(k === null){
          yield O.yieldAll(m);
          continue;
        }
        
        yield [getClauses, m];
      }
    };
    
    yield* O.recg(getClauses, clauses);
  }
  
  assign(k, v){
    const {
      idents, clauses,
      activeIdents, activeClauses,
      assignments, relations,
    } = this;
    
    assert(!assignments.has(k));
    assignments.set(k, v);
    
    const vs = idents.get(k);
    const v0 = vs[v ^ 1];
    const v1 = vs[v];
    
    for(const clause of v1)
      this.removeClause(clause, 1);
    
    for(const clause of v0)
      this.removeIdentFromClause(clause, k);
    
    idents.delete(k);
    activeIdents.delete(k);
    
    const stack = [k, v];
      
    while(stack.length !== 0){
      const v = stack.pop();
      const k1 = stack.pop();
      if(!relations.has(k1)) continue;
      
      const rel = relations.get(k1);
      relations.delete(k1);
      
      for(const [k2, eq] of rel){
        const vNew = v ^ !eq;
        
        assert(!assignments.has(k2));
        assignments.set(k2, vNew);
        
        idents.delete(k2);
        activeIdents.delete(k2);
        
        stack.push(k2, vNew);
      }
    }
  }
  
  substIdent(k2, k1, eq){
    const {
      idents, clauses,
      activeIdents, activeClauses,
      relations,
    } = this;
    
    // assert(!relations.has(k1));
    // relations.set(k1, [k2, eq]);
    
    if(!relations.has(k1))
      relations.set(k1, new Map());
    
    const rel = relations.get(k1);
    assert(!rel.has(k2));
    
    rel.set(k2, eq);
    
    const neq = eq ^ 1;
    
    for(const refs of idents.get(k2)){
      for(const clause of refs){
        const index2 = clause.indexOf(k2);
        const index1 = clause.indexOf(k1);
        
        assert(index2 !== -1);
        
        if(index1 === -1){
          this.removeClause(clause);
          
          clause[index2] = k1;
          clause[index2 + 1] ^= neq;
          
          sortClause(clause);
          this.addClause(clause);
          
          continue;
        }
        
        const v2 = clause[index2 + 1];
        const v1 = clause[index1 + 1];
        
        if(v1 ^ v2 ^ eq){
          this.removeIdentFromClause(clause, k2);
          continue;
        }
        
        this.removeClause(clause, 1);
      }
    }
    
    idents.delete(k2);
    activeIdents.delete(k2);
  }
  
  clauseToStr(clause){
    const len = clause.length;
    const line = [];
    
    for(let i = 0; i !== len; i += 2){
      const k = clause[i];
      const v = clause[i + 1];
      
      line.push(identToStr(k, v));
    }
    
    if(line.length === 0)
      return '/';
    
    // const cid = `[${clause.cid}] `;
    return line.join(' ');
  }
  
  clausesToStr(){
    const lines = [];
    
    for(const clause of this.getClauses())
      lines.push(this.clauseToStr(clause));
    
    if(lines.length === 0)
      return `(empty)`;
    
    return lines.join('\n');
  }
  
  assignmentsToStr(){
    const {assignments} = this;
    const line = [];
    
    for(const [k, v] of assignments)
      line.push(identToStr(k, v));
    
    return `(${line.join(' ')})`;
  }
  
  relationsToStr(){
    const {relations} = this;
    const line = [];
    
    for(const [k1, rel] of relations)
      for(const [k2, eq] of rel)
        line.push(`${identToStr(k2)}: ${identToStr(k1, eq)}`);
    
    return `(${line.join(', ')})`;
  }
  
  debug(force=0){
    if(!(DEBUG || force)) return;
    
    O.logb();
    
    if(this.unsat){
      log('UNSAT');
    }else{
      log(this.assignmentsToStr());
      log(this.relationsToStr());
      log();
      log(this.clausesToStr());
    }
    
    debug();
    O.logb();
  }
  
  simplify(){
    if(this.unsat) return 0;
    
    const {
      idents, clauses,
      activeIdents, activeClauses,
      assignments,
    } = this;
    
    this.debug();
    
    loop: while(1){
      let found = 0;
      
      for(const k of activeIdents){
        if(this.unsat) return 0;
        if(DEBUG) log(`Ident: ${identToStr(k)}`);
        assert(!assignments.has(k));
        
        found = 1;
        activeIdents.delete(k);
        
        const [v0, v1] = idents.get(k);
        const emp0 = v0.size === 0;
        const emp1 = v1.size === 0;
        
        if(emp0 && emp1){
          this.assign(k, O.rand());
          this.debug();
          continue;
        }
        
        if(emp0){
          this.assign(k, 1);
          this.debug();
          continue;
        }
        
        if(emp1){
          this.assign(k, 0);
          this.debug();
          continue;
        }
      }
      
      clausesLoop: for(const clause of activeClauses){
        if(this.unsat) return 0;
        if(DEBUG) log(`Clause: ${this.clauseToStr(clause)}`);
        assert(this.hasClause(clause, 1));
        
        found = 1;
        activeClauses.delete(clause);
        
        const len = clause.length;
        const identsNum = len >> 1;
        const set = this.getClauseSet(clause);
        
        if(identsNum === 0){
          this.unsat = 1;
          this.debug();
          return 0;
        }
        
        if(identsNum === 1){
          const k = clause[0];
          const v = clause[1];
          
          this.assign(k, v);
          this.debug();
          
          continue;
        }
        
        if(identsNum === 2){
          setLoop: for(const c of set){
            if(c === clause) continue;
            
            const v11 = c[1];
            const v12 = c[3];
            const v21 = clause[1];
            const v22 = clause[3];
            
            const eq1 = (v11 === v21) | 0;
            const eq2 = (v12 === v22) | 0;
            
            this.removeClause(clause);
            
            if(eq1 !== eq2){
              const k = c[eq1 ? 2 : 0];
              
              this.removeIdentFromClause(c, k);
              activeIdents.add(k);
              this.debug();
              
              continue clausesLoop;
            }
            
            if(eq1 && eq2){
              this.debug();
              continue clausesLoop;
            }
            
            const k1 = clause[0];
            const k2 = clause[2];
            
            this.substIdent(k2, k1, v11 ^ v12);
            this.debug();
            
            continue clausesLoop;
          }
          
          continue;
        }
        
        setLoop: for(const c of set){
          if(c === clause) continue;
          if(c.length !== len) continue;
          
          let kDif = null;
          
          for(let i = 0; i !== len; i += 2){
            const k1 = clause[i];
            const k2 = c[i];
            
            if(k1 !== k2) continue setLoop;
            
            const v1 = clause[i + 1];
            const v2 = c[i + 1];
            
            if(v1 === v2) continue;
            if(kDif !== null) continue setLoop;
            
            kDif = k1;
          }
          
          this.removeClause(clause);
          
          if(kDif === null){
            this.debug();
            break setLoop;
          }
          
          this.removeIdentFromClause(c, kDif);
          activeIdents.add(kDif);
          
          this.debug();
          
          break setLoop;
        }
      }
      
      if(!found) break;
    }
    
    return !this.unsat;
  }
  
  getCnf(){
    assert(!this.unsat);
    
    const identsIdsMap = new Map();
    const idsIdentsMap = new Map();
    const clauses = [];
    
    for(const clause of this.getClauses()){
      const len = clause.length;
      assert((len & 1) === 0);
      
      const c = [];
      
      for(let i = 0; i !== len; i += 2){
        const k = clause[i];
        const v = clause[i + 1];
        
        if(!identsIdsMap.has(k)){
          const id = identsIdsMap.size + 1;
          identsIdsMap.set(k, id);
          idsIdentsMap.set(id, k);
        }
        
        const id = identsIdsMap.get(k);
        c.push(idToStr(id, v));
      }
      
      c.push(0);
      clauses.push(c.join(' '));
    }
    
    const varsNum = identsIdsMap.size;
    const clausesNum = clauses.length;
    
    log(`---> ${format.num(varsNum)} ${format.num(clausesNum)}`);
    const cnf = `p cnf ${varsNum} ${clausesNum}\n${clauses.join('\n')}`;
    
    return {
      identsIdsMap,
      idsIdentsMap,
      cnf,
    };
  }
  
  async solve(varGroups, varGroupsNum=varGroups.length){
    const {
      idents, clauses,
      assignments, relations,
    } = this;
    
    const vars = O.concat(varGroups.slice(0, varGroupsNum));
    const varsNum = vars.length;
    
    // this.getCnf();
    // O.exit();
    // const a = this.getCnf().cnf.match(/^.*/m)[0];
    if(!this.simplify()){
      this.unsat = 1;
      return null;
    }
    // const b = this.getCnf().cnf.match(/^.*/m)[0];
    // log(a);
    // log(b);
    // O.exit();
    
    const saveCnf = cnfStr => {
      if(!DEBUG) return;
      log();
      debug(cnfStr);
      // O.wfs(require('../format').path('-dw/csp-cnf.txt'), cnfStr);
    };
    
    // saveCnf();
    // O.exit();
    
    let identsIdsMap = null;
    let idsIdentsMap = null;
    let satAssignments = null;
    
    const check = async () => {
      const {
        identsIdsMap: identsIdsMapNew,
        idsIdentsMap: idsIdentsMapNew,
        cnf,
      } = this.getCnf();
      
      saveCnf(cnf);
      
      if(DEBUG){
        O.logb();
        log('SAT SOLVER');
        O.logb();
      }
      
      const assignmentsNew = await runCms(cnf);
      
      if(assignmentsNew !== null){
        identsIdsMap = identsIdsMapNew;
        idsIdentsMap = idsIdentsMapNew;
        satAssignments = assignmentsNew;
        return 1;
      }
      
      return 0;
    };
    
    if(!await check()){
      this.unsat = 1;
      return null;
    }
    
    let n = 0;
    const order = O.concat(O.shuffle(O.ca(varGroups.length, i => O.shuffle(O.ca(varGroups[i].length, () => n++)))));
    
    for(let i = 0; i !== varsNum; i++){
      logStatus(i + 1, varsNum, 'variable');
      
      const k = vars[order[i]];
      if(!idents.has(k)) continue;
      
      const id = identsIdsMap.get(k);
      const v = O.rand();
      
      const clauseNew = [k, v];
      this.addClause(clauseNew);
      
      if(satAssignments.get(id) === v){
        assert(this.simplify());
        continue;
      }
      
      // const size = Math.sqrt(order.length);
      // const w = size;
      // const h = size;
      // 
      // log(O.ca(h, y => O.ca(w, x => {
      //   const i = y * w + x;
      //   const k = vars[i][0];
      //   const v = sol.has(k) ? sol.get(k) : null;
      //   return v !== null ? '.#'[v] : '~';
      // }).join('')).join('\n'));
      
      if(!await check()){
        this.removeClause(clauseNew);
        clauseNew[1] ^= 1;
        this.addClause(clauseNew);
      }
      
      assert(this.simplify());
      // assert(await check());
      
      // O.logb()
      // log(k, sol.get(k))
      // log()
      // log(clauses)
      // this.simplify(k);
      // O.logb()
      // log(clauses)
      // O.exit()
      
      // O.logb();
      // log(k);
      // log();
      // log(clauses)
      // log(O.sanl(this.getCnf()).slice(1).join('\n'))
      // O.logb();
      // assert(O.match(O.sanl(this.getCnf()).slice(1).join('\n'), /\d+/g).every(a => (a | 0) !== k + 1))
    }
    
    for(const [id, v] of satAssignments){
      const k = idsIdentsMap.get(id);
      if(!idents.has(k)) continue;
      
      this.assign(k, v);
    }
    
    if(0){
      // assert(this.simplify());
      // 
      // if(clauses.size !== 0){
      //   this.debug(1);
      //   throw new TypeError('Symmetry');
      // }
    }else{
      for(const [id, v] of satAssignments){
        assert(idsIdentsMap.has(id));
      
        const k = idsIdentsMap.get(id);
        if(assignments.has(k)) continue;
      
        assignments.set(k, v);
      }
    }
    
    if(varGroups.length !== 0)
      O.logb();
    
    // saveCnf();
    
    return varGroups.map(group => {
      return group.reduceRight((n, k) => {
        assert(assignments.has(k));
        const v = assignments.get(k);
        return (n << 1) | v;
      }, 0);
    });
  }
}

const sortClause = clause => {
  const len = clause.length;
  assert((len & 1) === 0);
  
  for(let i = 0; i !== len; i += 2){
    for(let j = i + 2; j !== len; j += 2){
      const s1 = clause[i];
      const s2 = clause[j];
      assert(s1 !== s2);
      
      if(cmpSym(s1, s2) < 0)
        continue;
      
      const v1 = clause[i + 1];
      const v2 = clause[j + 1];
      
      clause[i] = s2;
      clause[j] = s1;
      clause[i + 1] = v2;
      clause[j + 1] = v1;
    }
  }
  
  return clause;
};

const runCms = input => new Promise((res, rej) => {
  const args = ['--verb', '0', '-r', '0'];
  const proc = cp.spawn(config.exe.cms, args);
  
  const stdoutBufs = [];
  const stderrBufs = [];
  
  proc.stdout.on('data', a => stdoutBufs.push(a));
  proc.stderr.on('data', a => stderrBufs.push(a));
  
  proc.on('exit', code => {
    const out = Buffer.concat(stdoutBufs).toString();
    const err = Buffer.concat(stderrBufs).toString();
    
    (async () => {
      if(err.length !== 0)
        O.exit()//throw err;
      
      const lines = O.sanl(out);
      const linesNum = lines.length;
      const c = lines[0][2];
      
      if(c === 'U')
        return null;
  
      if(c === 'S'){
        const assignments = new Map();
        
        for(let i = 1; i !== linesNum; i++){
          const vars = lines[i].trim().split(' ').slice(1);
          
          for(const s of vars){
            if(s === '0') continue;
            
            const match = s.match(/^(\-?)(\d+)$/);
            assert(match !== null);
            
            const v = match[1] === '' ? 1 : 0;
            const k = match[2] | 0;
            
            assignments.set(k, v);
          }
        }
        
        return assignments;
      }
      
      throw out;
    })().then(res, rej);
  });
  
  proc.stdin.end(input);
});

const {mkSym, cmpSym, identToStr} = (() => {
  const cspMap = new WeakMap();
  const symMap = new WeakMap();
  
  const mkSym = csp => {
    if(!cspMap.has(csp))
      cspMap.set(csp, 1);
    
    const id = cspMap.get(csp);
    cspMap.set(csp, id + 1);
    
    const sym = O.obj()
    symMap.set(sym, id);
    sym.id = id;
    
    return sym;
  };
  
  const cmpSym = (sym1, sym2) => {
    const id1 = symMap.get(sym1);
    const id2 = symMap.get(sym2);
    
    return id1 - id2;
  };
  
  const identToStr = (k, v) => {
    const id = O.has(k, 'name') ? k.name : String(k.id);
    return idToStr(id, v);
  };
  
  return {mkSym, cmpSym, identToStr};
})();

const idToStr = (id, v=1) => {
  const sign = v ? '' : '-';
  return `${sign}${id}`;
};

const isNum = k => {
  return typeof k === 'number';
};

const isBit = v => {
  return v === 0 || v === 1;
};

module.exports = CSP;