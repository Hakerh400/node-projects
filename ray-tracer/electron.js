'use strict';

const fs = require('fs');
const path = require('path');
const O = require('../omikron');
O.isElectron = 1;
const media = require('../media');
const format = require('../format');
const logSync = require('../log-sync');

const HD = 1;

const w = HD ? 1920 : 640;
const h = HD ? 1080 : 480;
const fps = 60;
const fast = !HD;

const [wh, hh] = [w, h].map(a => a >> 1);

global.O = O;
global.iw = w;
global.ih = h;
global.is_electron = 1;
global.VIEW_DISTANCE = HD ? 1e3 : 50;
require('../../web/projects/ray-tracer/main');

const recordsFile = format.path('-dw/b/cam.txt');
const records = O.sanl(O.rfs(recordsFile, 1)).map(a => a.split(' ').map(a => +a));
const recordsNum = records.length;

const framesNum = (O.last(records)[0] * 60 / 1e3 | 0) + 5;

const outputFile = getOutputFile();

setTimeout(() => main().catch(log));

async function main(){
  const g = document.querySelector('canvas').getContext('2d');

  let i = 0;
  
  media.renderVideo(outputFile, w, h, fps, fast, (w, h, g1, f) => {
    media.logStatus(f, framesNum);
    
    const time = (f - 1) * 1e3 / 60;
    
    while(i !== recordsNum - 1){
      const t = records[i + 1][0];
      if(t > time) break;
      i++;
    }
    
    const r = records[i];
    const r1 = records[Math.min(i + 1, recordsNum - 1)];
    const t = time - r[0];
    const dt = r === r1 ? 1 : r1[0] - r[0];
    const k = t / dt;
    const k1 = 1 - k;
    
    cam.x = k1 * r[1] + k * r1[1];
    cam.y = k1 * r[2] + k * r1[2];
    cam.z = k1 * r[3] + k * r1[3];
    cam.pitch = k1 * r[4] + k * r1[4];
    cam.yaw = k1 * r[5] + k * r1[5];
    
    render(time);

    if(f === framesNum){
      g1.drawImage(g.canvas, 0, 0);
      return false;
    }
    
    const buf = media.toBuffer(g);
    
    O.animFrame();
    
    return buf;
  }, () => setTimeout(() => O.exit(), HD ? 5e3 : 1e3));
}

function getOutputFile(vid=0){
  // if(vid || !HD) return '-vid/1.mp4';
  const project = path.parse(__dirname).name;
  return `-render/${HD ? 'ray-tracer' : '1'}.mp4`;
}