'use strict';

const fs = require('fs');
const path = require('path');
const cp = require('child_process');
const assert = require('assert');
const O = require('../omikron');
const logStatus = require('../log-status');

const srcFile = 'E:/DISK/Videos/Videos/Videos/Data/002.txt';
const excFile = 'E:/DISK/Videos/Videos/Videos/Data/e.txt';
const destDir = 'E:/DISK/Videos/Videos/Videos/002';

const ytdlExts = [
  'ytdl',
  'part',
];

const ytdlArgs = [
  '--no-cache-dir',
  '--ffmpeg-location',
  'C:/Program Files/ffmpeg/bin/original/ffmpeg.exe',
  '--retries',
  'infinite',
  '--fragment-retries',
  'infinite',
];

const ytdlOpts = {
  cwd: destDir,
};

const main = async () => {
  const str = O.rfs(srcFile, 1);
  const exc = O.rfs(excFile, 1);
  const ids = extractIds(str);
  const idsExc = extractIds(exc);
  
  for(const id of idsExc)
    ids.delete(id);
  
  fixMkv();
  
  for(const fullName of fs.readdirSync(destDir)){
    const {name, ext} = parsePath(fullName);
    if(ytdlExts.includes(ext)) continue;
    
    const match = name.match(/\[([a-zA-Z0-9\-\x5F]{11})\]$/);
    if(match === null) continue;
    
    const id = match[1];
    ids.delete(id);
  }
  
  const idsArr = [...ids];
  const idsNum = idsArr.length;
  
  for(let i = 0; i !== idsNum; i++){
    if(i !== 0) log();
    logStatus(i + 1, idsNum, 'video');
    if(i !== idsNum - 1) log();
    
    const id = idsArr[i];
    const url = `https://www.youtube.com/watch?v=${id}`;
    const args = [...ytdlArgs, url];
    
    const code = await new Promise((res, rej) => {
      const proc = cp.spawn('yt-dlp', args, ytdlOpts);
      
      pipe(proc.stdout);
      pipe(proc.stderr);
      
      proc.on('exit', res);
      proc.on('error', rej);
    });
    
    if(code !== 0){
      // O.logb();
      // log('Interrupted');
      return;
    }
  }
  
  fixMkv();
  
  O.logb();
  log('Done');
};

const fixMkv = () => {
  for(const fullName of fs.readdirSync(destDir)){
    const {name, ext} = parsePath(fullName);
    if(ext !== 'mkv') continue;
    
    const pth1 = path.join(destDir, fullName);
    const pth2 = path.join(destDir, `${name}.webm`);
    
    fs.renameSync(pth1, pth2);
  }
};

const parsePath = pth => {
  const info = path.parse(pth);
  const {ext} = info;
  
  if(ext.startsWith('.'))
    info.ext = ext.slice(1);
  
  return info;
};

const extractIds = str => {
  return new Set(O.match(str, /\?v=[a-zA-Z0-9\-\x5F]{11}/g).map(a => a.slice(-11)));
};

const pipe = st => {
  st.pipe(process.stdout);
  
  // st.setEncoding('utf8');
  // 
  // st.on('data', buf => {
  //   const str = buf.toString().replace(/[^\x00-~]/g, '?');
  //   process.stdout.write(str);
  // });
};

main().catch(log);