'use strict';

const fs = require('fs');
const path = require('path');
const crypto = require('crypto');
const O = require('../omikron');
const assert = require('../assert');
const logStatus = require('../log-status');

const cwd = __dirname;
const jsDir = path.join(cwd, '..');

const main = () => {
  let n = 0n;
  
  main_loop: while(1){
    if(n % BigInt(1e5) === 0n)
      logStatus(n, 1n << 32n, 'buffer');
    
    const str = String(n);
    const hash = crypto.createHash('sha256');
    
    hash.update(str);
    
    const buf = hash.digest();
    
    for(let i = 0; i !== 4; i++){
      if(buf[i] !== 0){
        n++;
        continue main_loop;
      }
    }
    
    break;
  }
  
  log(String(n));
};

main();