'use strict';

const fs = require('fs');
const path = require('path');
const cp = require('child_process');
const assert = require('assert');
const O = require('../js-util');
const strs = require('../strs');
const fsr = require('../fs-rec');
const readline = require('../readline');

const world_name = 'Main';

const cwd = __dirname;
const main_dir = path.normalize(`C:/Users/User/AppData/Roaming/.${strs.m}`);
const saves_dir = path.join(main_dir, 'saves');
const saves_world_dir = path.join(saves_dir, world_name);
const saves_bkp_dir = path.join(main_dir, 'saves_bkp');
const saves_bkp_old_world_dir = path.join(saves_bkp_dir, `${world_name}_old`);
const saves_bkp_new_world_dir = path.join(saves_bkp_dir, `${world_name}_new`);

const bkp_dir = 'D:/M';
const bkp_world_dir = path.join(bkp_dir, world_name);

const args = process.argv.slice(2);

const main = () => {
  const rl = readline.rl();
  
  try{
    if(!fs.existsSync(saves_bkp_dir))
      fs.mkdirSync(saves_bkp_dir);
    
    assert(args.length === 1);
    
    const arg = args[0];
    
    exec_cmd(arg);
    log('Done');
  }catch(e){
    log(e);
  }
  
  rl.ask('', () => {
    rl.close();
  });
}

const exec_cmd = cmd => {
  if(cmd === 'save'){
    save();
    return;
  }
  
  if(cmd === 'restore'){
    restore();
    return;
  }
  
  if(cmd === 'backup'){
    mk_bkp();
    return;
  }
  
  assert.fail(cmd);
};

const save = () => {
  if(!fs.existsSync(saves_world_dir))
    throw new Error('World not found');
  
  if(fs.existsSync(saves_bkp_old_world_dir)){
    log('Deleting the old temporary backup');
    del_dir(saves_bkp_old_world_dir);
  }
  
  if(fs.existsSync(saves_bkp_new_world_dir)){
    log('Renaming the new temporary backup to the old one');
    move_dir(saves_bkp_new_world_dir, saves_bkp_old_world_dir);
  }
  
  log('Creating temporary backup of the current world');
  copy_dir(saves_world_dir, saves_bkp_new_world_dir);
};

const restore = () => {
  if(fs.existsSync(saves_world_dir)){
    log('Deleting the current world');
    del_dir(saves_world_dir);
  }
  
  const dirs = [
    saves_bkp_new_world_dir,
    saves_bkp_old_world_dir,
    get_last_bkp_dir(),
  ];
  
  for(const dir of dirs){
    if(dir === null) continue;
    if(!fs.existsSync(dir)) continue;
    
    log(`Restoring the world from ${dir}`);
    copy_dir(dir, saves_world_dir);
    
    return;
  }
  
  throw new Error('Backup not found');
};

const mk_bkp = () => {
  if(!fs.existsSync(saves_world_dir))
    throw new Error('World not found');
  
  const dest = get_next_bkp_dir();
  
  log(`Saving backup to ${dest}`);
  copy_dir(saves_world_dir, dest);
};

const get_last_bkp_index = () => {
  const names = fs.readdirSync(bkp_world_dir);
  let index = null;
  
  for(const name of names){
    if(!/^\d{3}$/.test(name)) continue;
    
    const i = parseInt(name, 10);
    
    if(index === null || i > index)
      index = i;
  }
  
  return index;
};

const get_last_bkp_name = () => {
  const index = get_last_bkp_index();
  return index_to_name(index);
};

const get_last_bkp_dir = () => {
  const name = get_last_bkp_name();
  if(name === null) return null;
  
  return path.join(bkp_world_dir, name);
};

const get_next_bkp_index = () => {
  const index = get_last_bkp_index();
  if(index === null) return 1;
  return index + 1;
};

const get_next_bkp_name = () => {
  const index = get_next_bkp_index();
  return index_to_name(index);
};

const get_next_bkp_dir = () => {
  const name = get_next_bkp_name();
  return path.join(bkp_world_dir, name);
};

const index_to_name = index => {
  if(index === null) return null;
  return String(index).padStart(3, '0');
};

const move_dir = (src, dest) => {
  fs.renameSync(src, dest);
};

const copy_dir = (src, dest) => {
  fs.cpSync(src, dest, {recursive: true});
};

const del_dir = dir => {
  fs.rmSync(dir, {recursive: true, force: true});
};

main();