'use strict';

global.isElectron = 1;

const fs = require('fs');
const path = require('path');
const O = require('../js-util');

const media = require('../media');
const log_status = require('../log-status');
const format = require('../format');

const {
  min, max, sin, cos, tanh,
  floor, ceil, round,
  PI,
} = Math;

const HD = 0;

const c1 = 1;
const c2 = 0.5;
const kern_size = 3;
// const kern_size2 = (kern_size * 2 + 1) ** 2;

const w = HD ? 1920 : 640;
const h = HD ? 1080 : 480;
const fps = 60;
const fast = !HD;

const [wh, hh] = [w, h].map(a => a >> 1);
const [w1, h1] = [w, h].map(a => a + 1);

const duration = 120;
const framesNum = fps * duration;

const col = new Uint8ClampedArray(3);

const main = async () => {
  const out_file = get_out_file();
  
  let grid = new O.Grid(w, h, (x, y) => {
    return -1;
    // if(x < 200 && y < 200) return 1;
    // if(x >= w - 200 && y >= h - 200) return -1;
    // return O.randf(-1, 1);
  });
  
  let grid1 = new O.Grid(w, h, () => 0);
  
  let data = null;
  
  const init = g => {
    data = new O.ImageData(g);
  };

  media.renderVideo(out_file, w, h, fps, fast, (w, h, g, f) => {
    log_status(f, framesNum);
    if(f === 1) init(g);
    
    // if(f==1){
    //   for(let x = 0; x !== w; x++){
    //     O.hsv(1 - (x / w * 2 + 1) / 3, col);
    // 
    //     for(let y = 0; y !== h; y++){
    //       data.set(x, y, col);
    //     }
    //   }
    // 
    //   data.put();
    // }
    
    if(f !== 1){
      grid.iter((x, y, v0) => {
        const r_num = 4;
        
        let sum = 0;
        
        for(let r = 0; r <= r_num; r++){
          const m = c1 * r * (1 - tanh(c2 * r));
          let sum1 = 0;
        
          const angles_num = ceil(1 + r * 2 * PI);
          const angle_d = 2 * PI / angles_num;
        
          for(let i = 0; i !== angles_num; i++){
            const a = i * angle_d;
            const x1 = x + r * cos(a);
            const y1 = y + r * sin(a);
            const v1 = grid.get(floor(x1), floor(y1));
        
            if(v1 !== null) sum1 += v1;
          }
        
          sum += m * sum1 * angle_d;
        }
        
        // const x_start = O.bound(x - kern_size, 0, w1);
        // const y_start = O.bound(y - kern_size, 0, h1);
        // const x_end = O.bound(x + kern_size, 0, w1);
        // const y_end = O.bound(y + kern_size, 0, h1);
        // 
        // for(let y1 = y_start; y1 <= y_end; y1++){
        //   for(let x1 = x_start; x1 <= x_end; x1++){
        //     const r = O.dist(x, y, x1, y1);
        //     const v1 = grid.get(x1, y1);
        //     const m = c1 * (1 + r) * (1 - tanh(c2 * r));
        // 
        //     sum += m * v1;
        //   }
        // }
        
        const dif = fn(sum) - fn(-sum);
        const v1 = v0 + dif;
        
        grid1.set(x, y, v1);
      });
      
      [grid, grid1] = [grid1, grid];
    }
    
    grid.iter((x, y, v) => {
      const k = (O.bound(v, -1, 1) + 1) / 2;
      O.hsv(1 - (k * 2 + 1) / 3, col);
      data.set(x, y, col);
    });
    
    data.put();

    return f !== framesNum;
  }, () => {
    exit();
  });
};

const fn = x => {
  const a = 2 + x;
  return (1 - tanh(x)) * (0.5 - 1 / (1 + a * a));
};

const get_out_file = (vid=0) => {
  return format.path('-render/1.mp4');
  // if(vid || !HD) return format.path('-vid/1.mp4');
  // const project = path.parse(__dirname).name;
  // return format.path(`-render/${project}.mp4`);
};

const exit = (...args) => {
  if(args.length !== 0) log(...args);
  window.close();
};

process.on('uncaughtException', exit);
main().catch(exit);