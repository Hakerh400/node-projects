"use strict"

global.isElectron = 1

const fs = require("fs")
const path = require("path")
const O = require("../js-util")
const assert = require("../assert")
const media = require("../media")
const log_status = require("../log-status")
const format = require("../format")

const HD = 0

const w = HD ? 1920 : 640
const h = HD ? 1080 : 480
const fps = 60
const fast = !HD

const [wh, hh] = [w, h].map(a => a >> 1)
const [w1, h1] = [w, h].map(a => a - 1)

const duration = 10
const framesNum = fps * duration

const w_rad = 1
const update_frame_parity = 0

const cols = [
  [20, 20, 20],
  [0, 255, 0],
]

const main = async () => {
  const out_file = get_out_file()
  
  const buf = Buffer.alloc(w * h << 2)
  
  let frame_parity = 0
  
  const has = (x, y) => {
    return x >= 0 && y >= 0 && x < w && y < h
  }
  
  const grid = new O.Grid(w, h, (x, y) => {
    const xs = []
    
    const gen_ofs = () => {
      return O.rand(-w_rad, w_rad)
    }
    
    const add_inp = () => {
      let x1, y1
      
      if(O.rand(2)){
        x1 = x + gen_ofs()
        y1 = y + gen_ofs()
      }else{
        x1 = O.rand(w)
        y1 = O.rand(h)
      }
      
      if(!has(x1, y1))
        return
      
      if(xs.some(([x0, y0]) => x0 === x1 && y0 === y1))
        return
      
      xs.push([x1, y1])
    }
    
    while(O.rand(5))
      add_inp()
    
    return {
      val: O.rand(2),
      val_new: null,
      xs,
    }
  })
  
  const get = (x, y) => {
    return grid.get(x, y).val
  }
  
  const set = (x, y, b) => {
    b = b ? 1 : 0
    grid.get(x, y).val = b
    
    const col = cols[b ^ frame_parity]
    const i = y * w + x << 2
    
    buf[i] = col[0]
    buf[i + 1] = col[1]
    buf[i + 2] = col[2]
  }
  
  grid.iter((x, y, d) => {
    set(x, y, d.val)
  })
  
  const tick = () => {
    if(update_frame_parity)
      frame_parity ^= 1
    
    grid.iter((x, y, d) => {
      let b = 0
      
      for(const [x1, y1] of d.xs)
        b |= get(x1, y1)
      
      d.val_new = !b
    })
    
    grid.iter((x, y, d) => {
      set(x, y, d.val_new)
    })
  }
  
  media.renderVideo(out_file, w, h, fps, fast, (w, h, g, f) => {
    if(f > framesNum) return -1
    log_status(f, framesNum)
    
    tick()
    
    return buf
  }, () => {
    window.close()
  })
}

const get_out_file = (vid=0) => {
  if(vid || !HD) return format.path("-render/1.mp4")
  const project = path.parse(__dirname).name
  return format.path(`-render/${project}.mp4`)
}

main().catch(log)