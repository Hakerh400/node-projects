"use strict"

const fs = require("fs")
const path = require("path")
const O = require("../js-util")
const readline = require("../readline")
const format = require("../format")

const out_file = format.path("-dw/1.txt")

const main = async () => {
  let rl = readline.rl()
  let done = 0
  
  let n = null
  
  try{
    n = await O.bisecta(async n => {
      n = Number(n)
      
      if(done) return 1
      if(n === 0) return 0
      
      let s = "1".repeat(n)
      O.wfs(out_file, s)
      
      return !await new Promise((res, rej) => {
        rl.ask("> ", c => {
          if(c === ""){
            done = 1
            return res(0)
          }
          
          if(c === "0") return res(0)
          if(c === "1") return res(1)
          
          rej(new Error(c))
        })
      })
    })
  }finally{
    rl.close()
  }
  
  if(done) return
  
  n = Number(n) - 1
  
  O.logb()
  log(format.num(n))
}

main().catch(log)