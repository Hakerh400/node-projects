'use strict';

const fs = require('fs');
const path = require('path');
const O = require('../omikron');
O.isElectron = 1;
const media = require('../media');

const HD = 1;

const w = HD ? 1920 : 640;
const h = HD ? 1080 : 480;
const fps = 60;
const fast = !HD;

const [wh, hh] = [w, h].map(a => a >> 1);

global.O = O;
global.iw = w;
global.ih = h;
require('../../web/projects/distribution-arrows/main');

const duration = 60 * 5;
const framesNum = fps * duration;

const outputFile = getOutputFile();

setTimeout(() => main().catch(log));

async function main(){
  const g = document.querySelector('canvas').getContext('2d');
  
  media.renderVideo(outputFile, w, h, fps, fast, (w, h, g1, f) => {
    media.logStatus(f, framesNum);

    if(f === framesNum){
      g1.drawImage(g.canvas, 0, 0);
      return false;
    }
    
    const buf = media.toBuffer(g);
    
    O.animFrame();
    
    return buf;
  }, () => setTimeout(() => O.exit(), 1e3));
}

function getOutputFile(vid=0){
  if(vid || !HD) return '-vid/1.mp4';
  const project = path.parse(__dirname).name;
  return `-render/1.mp4`;
}