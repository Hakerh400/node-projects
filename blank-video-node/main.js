"use strict"

const fs = require("fs")
const path = require("path")
const cp = require("child_process")
const O = require("../js-util")
const assert = require("../assert")
const media = require("../media-node")
const log_status = require("../log-status")
const format = require("../format")

const HD = 0

const w = HD ? 1920 : 640
const h = HD ? 1080 : 480
const fps = 60
const fast = !HD

const [wh, hh] = [w, h].map(a => a >> 1)
const [w1, h1] = [w, h].map(a => a - 1)

const duration = 10
const framesNum = fps * duration

const main = () => {
  const out_file = get_out_file()
  const col = Buffer.alloc(3)
  
  media.renderVideo(out_file, w, h, fps, fast, (w, h, imgd, f) => {
    log_status(f, framesNum)
    
    return f !== framesNum
  })
}

const get_out_file = (vid=0) => {
  const project = path.parse(__dirname).name
  const name = vid ? project : "1"
  return format.path(`-render/${name}.mp4`)
}

main()