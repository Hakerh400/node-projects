'use strict';

const fs = require('fs');
const path = require('path');
const O = require('../js-util');
const assert = require('../assert');

const cwd = __dirname;
const src_file = path.join(cwd, '1.txt');

const Types = O.enum([
  'RED',
  'MASTER',
  'DOOR',
  'FROZEN',
  'GOAL',
]);

const main = () => {
  const toks = O.rfs(src_file, 1).
    match(/[\(\)\{\}a-z*]|\d+/g);
  
  const toks_num = toks.length;
  const ents_info = O.obj();
  
  let tok_index = 0;
  
  const eof = () => {
    return tok_index === toks.length;
  };
  
  const fetch_tok = () => {
    if(eof()) return null;
    return toks[tok_index];
  };
  
  const get_tok = () => {
    assert(!eof());
    return toks[tok_index++];
  };
  
  const get_exact = s => {
    const tok = get_tok();
    assert(tok === s);
  };
  
  const get_nat = () => {
    const tok = get_tok();
    
    if(!/^\d+$/.test(tok))
      assert.fail(tok);
    
    return Number(tok);
  };
  
  const parse_type_aux = () => {
    const type = get_tok();
    
    if(type === 'f'){
      const n = get_nat();
      return [Types.FROZEN, n];
    }
    
    if(type === 'd'){
      const n = get_nat();
      return [Types.DOOR, n];
    }
    
    if(type === 'r'){
      return [Types.RED];
    }
    
    if(type === 'm'){
      return [Types.MASTER];
    }
    
    if(type === '*'){
      return [Types.GOAL];
    }
    
    assert.fail(type);
  };
  
  const parse_type = () => {
    get_exact('(');
    const t = parse_type_aux();
    get_exact(')');
    return t;
  };
  
  const parse_scope_aux = () => {
    const scope = [];
    
    while(!eof() && fetch_tok() !== '}'){
      const i = get_nat();
      const t = parse_type();
      const scope1 = parse_scope_m();
      
      ents_info[i] = [t, scope1];
      scope.push(i);
    }
    
    O.sortAsc(scope);
    
    return scope;
  };
  
  const parse_scope = () => {
    get_exact('{');
    const scope = parse_scope_aux();
    get_exact('}');
    return scope;
  };
  
  const parse_scope_m = () => {
    if(fetch_tok() !== '{') return [];
    return parse_scope();
  };
  
  const get_ents_for_scope = scope => {
    return scope.map(i => {
      return [i, ents_info[i][0]];
    });
  };
  
  const get_ents_for_id = i => {
    return get_ents_for_scope(ents_info[i][1]);
  };
  
  const concat_ents = (ents, i) => {
    return ents.concat(get_ents_for_id(i));
  };
  
  const calc_new_ents = (ents, i) => {
    return concat_ents(ents.filter(e => e[0] !== i), i);
  };
  
  const calc_new_ents_by_index = (ents, index) => {
    const i = ents[index][0];
    return calc_new_ents(ents, i);
  };
  
  const r_num = get_nat();
  const m_num = get_nat();
  const main_scope = parse_scope_aux();
  const ents = get_ents_for_scope(main_scope);
  
  const state = [[r_num, m_num], ents];
  
  const solve = state => {
    const seen = new Set([state_to_str(state)]);
    const queue = [[[], state]];
    
    let sol = null;
    
    while(queue.length !== 0){
      if(sol !== null) break;
      
      const [moves, [keys, ents]] = queue.shift();
      const [r_num, m_num] = keys
      
      const push_state = (move, [keys, ents]) => {
        let [r_num, m_num] = keys;
        
        norm_state: while(1){
          if(r_num !== 0){
            ents = ents.map(ent => {
              const [i, info] = ent;
              
              if(info[0] === Types.FROZEN){
                return [i, [Types.DOOR, info[1]]];
              }
              
              return ent;
            });
          }
          
          if(r_num === 0){
            const index = ents.findIndex(([i, info]) => {
              return info[0] === Types.DOOR && info[1] === 0;
            });
            
            if(index !== -1){
              ents = calc_new_ents_by_index(ents, index);
              continue norm_state;
            }
          }
          
          const index = ents.findIndex(([i, info]) => {
            return info[0] === Types.MASTER;
          });
          
          if(index !== -1){
            m_num++;
            ents = calc_new_ents_by_index(ents, index);
            continue norm_state;
          }
          
          break norm_state;
        }
        
        const moves_new = [...moves, move];
        
        const done = ents.some(([i, info]) => {
          return info[0] === Types.GOAL;
        });
        
        if(done){
          sol = moves_new;
          return;
        }
        
        ents = ents.sort(([i], [j]) => {
          return (i > j) - (i < j);
        });
        
        keys = [r_num, m_num];
        const state_new = [keys, ents];
        
        const s = state_to_str(state_new);
        if(seen.has(s)) return;
        
        seen.add(s);
        queue.push([moves_new, state_new]);
      };
      
      for(const [i, info] of ents){
        if(sol !== null) break;
        const type = info[0];
        
        const new_ents = () => {
          return calc_new_ents(ents, i);
        };
        
        const modify_keys = (move, d_red=0, d_master=0) => {
          const keys_new = [r_num + d_red, m_num + d_master];
          push_state(move, [keys_new, new_ents()]);
        };
        
        if(type === Types.RED){
          modify_keys(i, 1);
          continue;
        }
        
        if(type === Types.DOOR){
          const n = info[1];
          
          if(n !== 0 && r_num >= n){
            modify_keys(i, -n);
            continue;
          }
          
          if(m_num !== 0){
            modify_keys(`m${i}`, 0, -1);
            continue;
          }
          
          continue;
        }
        
        if(type === Types.FROZEN){
          continue;
        }
        
        assert.fail(Types[type]);
      }
    }
    
    return sol;
  };
  
  const sol = solve(state);
  
  if(sol === null){
    log('/');
    return;
  }
  
  log(sol.join(' '));
};

const state_to_str = state => {
  return JSON.stringify(state);
};

main();