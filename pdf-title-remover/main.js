'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('assert');
const O = require('../omikron');
const format = require('../format');

const args = process.argv.slice(2);

const dir = format.path('-dw/a');
const n = 1;

const main = () => {
  if(args.length !== 0){
    const pth = args.join(' ').replace(/\"/g, '');
    remove_title(pth);
    return;
  }
  
  for(let i = 0; i !== n; i++){
    const name = `${(i + 1).toString().padStart(3, '0')}.pdf`;
    const pth = path.join(dir, name);
    
    remove_title(pth)
  }
};

const remove_title = (pth, name=null) => {
  if(name === null)
    name = path.parse(pth).name;
  
  const buf = O.rfs(pth);
  
  let found = 1;
  let cnt = 0;
  
  while(found){
    found = 0;
    
    for(let sp_num = 0; sp_num <= 1; sp_num++){
      const s = `/Title${' '.repeat(sp_num)}(`;
      
      let start = indexOf(buf, s);
      if(start === -1) continue;
      
      found = 1;
      log(++cnt);
      
      /////
      buf[start + 1] = O.cc('0');
      break;
      /////
      
      start += s.length;
      
      const end = indexOf(buf, ')', start);
      assert(end !== -1);
      
      let buf1 = buf.slice(start, end);
      buf1.fill(0x00);
      Buffer.from(name).copy(buf1);
      
      break;
    }
  }
  
  assert(cnt !== 0);
  
  O.wfs(pth, buf);
};

const indexOf = (buf, s, start=0) => {
  s = Buffer.from(s);
    
  for(let i = start; i !== buf.length; i++)
    if(buf.slice(i, i + s.length).equals(s))
      return i;
  
  return -1;
};

main();