'use strict';

const fs = require('fs');
const path = require('path');
const http = require('http');
const querystring = require('querystring');
const O = require('../js-util');
const readline = require('../readline');

const port = 80
const max_chunk_size = 16 << 20

const headers = {
  'Access-Control-Allow-Origin': '*',
  'Access-Control-Allow-Headers': 'x-requested-with',
  'Cache-Control': 'no-cache',
}

const cwd = __dirname;
const rootDir = path.join(cwd, '../..');

let server = null;
let rl = null;

const main = () => {
  server = http.createServer(onReq);

  server.listen(port, () => {
    // log(`Server is listening on port ${port}`);
  });

  rl = readline.rl();
  rl.on('line', onInput);
};

const onInput = str => {
  str = str.trim();

  if(str === '') return;

  if(/^q$/i.test(str)){
    server.close();
    rl.close();
    server = null;
    rl = null;
    return;
  }

  log('Unknown command');
};

const onReq = (req, res) => {
  const url = new URL(`http://localhost${req.url}`);
  const url_pth = url.pathname;
  
  for(const key of O.keys(headers))
    res.setHeader(key, headers[key]);

  const e404 = () => {
    res.writeHead(404);
    res.end('Page not found');
  };
  
  const mk_read_stream = (...args) => {
    const st = fs.createReadStream(...args);
    
    st.pipe(res);
    res.once('close', () => st.close());
  };

  const send = pth => {
    if(!fs.existsSync(pth)) return 0;

    const stat = fs.statSync(pth);

    if(stat.isFile()){
      const {size} = stat;
      const range = req.headers.range;
      
      if(!range){
        const ext = path.parse(pth).ext;
        
        if(ext === '.wasm')
          res.setHeader('Content-Type', 'application/wasm');
        
        mk_read_stream(pth);
        return 1;
      }
      
      let positions = range.replace(/bytes=/, '').split('-');
      let start = Number(positions[0]);
      let total = size;
      let end = positions[1] ? Number(positions[1]) : total - 1;
      let chunk_size = Math.min(max_chunk_size, (end - start) + 1);
      
      end = start + chunk_size - 1
    
      try{
        mk_read_stream(pth, {start, end});
        
        res.writeHead(206, {
          'Content-Range': 'bytes ' + start + '-' + end + '/' + total,
          'Accept-Ranges': 'bytes',
          'Content-Length': chunk_size,
        });
      }catch(e){
        res.writeHead(416);
        res.end('Range not satisfiable');
      }
      
      return 1;
    }

    if(stat.isDirectory()){
      if(!url_pth.endsWith('/')){
        url.pathname += '/';
        res.writeHead(301, {Location: url});
        res.end();
        return 1;
      }
      
      return send(path.join(pth, 'index.htm'));
    }
    
    return 0;
  }
  
  let pth = null
  
  try{
    pth = decodeURIComponent(url_pth)
  }catch(e){
    log(url_pth)
    log(e)
  }
  
  if(pth === null) return e404()

  pth = path.join(rootDir, pth)
  if(!send(pth)) e404()
}

main()