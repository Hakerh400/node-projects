'use strict';

const fs = require('fs');
const path = require('path');
const O = require('../js-util');

module.exports = require(
  O.isElectron ? './electron-canvas' : './node-canvas'
);