'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('assert');
const O = require('../omikron');
const fsRec = require('../fs-rec');
const format = require('../format');

const pth = 'C:/Projects/liza'
const exts = O.arr2obj(['hs'])

const main = () => {
  // if(args.length !== 1){
  //   log('Expected exactly one argument');
  //   return;
  // }
  
  // const pth = args[0];
  const dir = new fsRec.Directory(pth);
  
  let sloc = 0;
  
  const f = function*(entry){
    if(entry.isFile){
      if(!O.has(exts, entry.ext))
        return;
      
      const str = entry.content.toString();
      sloc += O.sanl(str).filter(a => a.trim().length !== 0).length;
      
      return;
    }
    
    if(entry.isDir){
      if(entry.name.startsWith('.'))
        return;
      
      for(const e of entry.content)
        yield [f, e];
    
      return;
    }
  };
  
  O.rec(f, dir);
  
  log(format.num(sloc));
};

main();