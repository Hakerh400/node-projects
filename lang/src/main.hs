module Nat where

data Nat = Zero | Succ Nat

main :: Nat -> Nat -> Nat
main = add

add :: Nat -> Nat -> Nat
add a Zero = a
add a (Succ b) = Succ (add a b)