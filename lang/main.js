'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('assert');
const O = require('../omikron');
const parser = require('./parser');
const cs = require('./ctors');

const cwd = __dirname;
const srcDir = path.join(cwd, 'src');
const mainFile = path.join(srcDir, 'main.hs');

const main = () => {
  const src = O.lf(O.rfs(mainFile, 1)).
    replace(/^ *\n/gm, '').
    replace(/\n /gm, ' ');

  const lines = O.sanl(src.trim()).
    map(a => a.trim().replace(/\s+/g, ' '));
  
  const parsed = parser.parseModule(lines);
};

main();