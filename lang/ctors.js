'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('assert');
const O = require('../omikron');

class Base{}

class Module extends Base{
  constructor(name){
    super();
    this.name = name;
  }
};

module.exports = Object.assign({
  Base,
  Module,
});