'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('assert');
const O = require('../omikron');
const cs = require('./ctors');

const parseModule = lines => {
  const linesNum = lines.length;
  const mdName = parseModuleName(lines);
  const md = new cs.Module(mdName);
  
  log(md);
};

const parseModuleName = lines => {
  asrt(lines.length !== 0);
  
  const mdHeader = lines[0];
  const mdHeaderReg = /^module +([A-Z][a-zA-Z0-9]*) +where$/;
  const mdName = match(mdHeader, mdHeaderReg)[1];
  
  return mdName;
};

const match = (str, reg, force=1) => {
  const m = str.match(reg);
  
  if(m === null){
    asrt(!force);
    return null;
  }
  
  return m;
};

const asrt = a => {
  return assert(a);
};

module.exports = {
  parseModule,
  parseModuleName,
};