'use strict';

const fs = require('fs');
const path = require('path');
const O = require('../omikron');
const assert = require('../assert');

const main = () => {
  const sides_num = 20;
  const len = sides_num * 2 + 3;
  const a = O.ca(len, () => 0);
  
  for(let i = 0; i !== 1e8; i++){
    let sum = 0;
    
    while(sum < len){
      a[sum]++;
      sum += O.rand(1, sides_num);
    }
  }
  
  for(let i = 0; i !== len; i++){
    log(`${String(i).padEnd(4)} - ${a[i]}`);
  }
};

main();