'use strict';

global.isElectron = 1;

const fs = require('fs');
const path = require('path');
const O = require('../js-util');
const assert = require('../assert');
const media = require('../media');
const log_status = require('../log-status');
const format = require('../format');

const {
  min, max, sin, cos, tanh,
  floor, ceil, round,
  PI,
} = Math;

const HD = 1;

const w = HD ? 1920 : 640;
const h = HD ? 1080 : 480;
const fps = 60;
const fast = !HD;

const [wh, hh] = [w, h].map(a => a >> 1);
const [w1, h1] = [w, h].map(a => a - 1);

const duration = 60 * 30;
const framesNum = fps * duration;

const speed = 1e4;

const col = new Uint8ClampedArray(3);

const main = async () => {
  const out_file = get_out_file();
  
  const grid = new O.Grid(w, h, (x, y) => {
    return 0;
  });
  
  const get = grid.get.bind(grid);
  
  const set = (x, y, v, use_col=0) => {
    if(get(x, y) !== 1 - v)
      assert.fail();
    
    grid.set(x, y, v);
    
    // {
    //   const check = (x, y) => {
    //     const v = get(x, y);
    //     if(v !== 0) return;
    // 
    //     const xs = [
    //       get(x, y - 1),
    //       get(x + 1, y),
    //       get(x, y + 1),
    //       get(x - 1, y),
    //     ];
    // 
    //     if(xs.every(x => x === 1))
    //       assert.fail();
    //   };
    // 
    //   check(x, y - 1);
    //   check(x + 1, y);
    //   check(x, y + 1);
    //   check(x - 1, y);
    // }
    
    if(v === 0){
      col.fill(0);
      data.set(x, y, col);
      return;
    }
    
    if(use_col){
      data.set(x, y, col);
      return;
    }
    
    loop: for(let j = -1; j <= 1; j++){
      for(let i = -1; i <= 1; i++){
        if(i === 0 && j === 0) continue;
        
        const x1 = x + i;
        const y1 = y + j;
        if(!get(x1, y1)) continue;
        
        data.get(x1, y1, col);
        data.set(x, y, col);
        
        return;
      }
    }
    
    O.hsv(O.randf(1), col);
    data.set(x, y, col);
  };
  
  let data;

  media.renderVideo(out_file, w, h, fps, fast, (w, h, g, f) => {
    log_status(f, framesNum);
    
    if(f === 1){
      data = new O.ImageData(g);
    
      const s = 250;
      const sh = s >> 1;
      const s1 = s - 1;
      const x0 = wh - sh;
      const y0 = hh - sh;
      
      col.fill(255);
      
      const f = (x, y) => {
        if(get(x, y) === 1) return;
        set(x, y, 1, 1);
      };
      
      for(let i = 0; i !== s; i++){
        f(x0 + i, y0);
        f(x0 + i, y0 + s1);
        f(x0, y0 + i);
        f(x0 + s1, y0 + i);
      }
    }
    
    if(f !== 1){
      // let sp_prev = null;
      
      sp_loop: for(let sp = 0; sp !== speed; sp++){
        // if(f >= 65){
        //   // log(sp);
        //   const sp1 = sp + 1;
        // 
        //   if(sp_prev !== sp1){
        //     sp_prev = sp1;
        //     log(sp1);
        // 
        //     // if(sp1 === 7303){
        //     //   data.put();
        //     //   media.renderImage('-dw/render.png', w, h, (w, h, g1) => {
        //     //     g1.drawImage(g.canvas, 0, 0);
        //     //   }, () => {
        //     //     exit();
        //     //   });
        //     //   return 0;
        //     // }
        //   }
        // }
        
        // const angle = O.randf(0, PI * 2);
        // const dx = cos(angle);
        // const dy = sin(angle);
        // let xf = wh;
        // let yf = hh;
        
        ray_loop: while(1){
          // xf += dx;
          // yf += dy;
          // const x = floor(xf);
          // const y = floor(yf);
          const x = O.rand(w);
          const y = O.rand(h);
          
          const v = get(x, y);
          
          if(v === null){
            sp--;
            continue sp_loop;
          }
          
          if(O.rand(10) !== 0) continue;
          
          const v1 = 1 - v;
          
          const adjs = [
            get(x - 1, y - 1), get(x, y - 1),
            get(x + 1, y - 1), get(x + 1, y),
            get(x + 1, y + 1), get(x, y + 1),
            get(x - 1, y + 1), get(x - 1, y),
          ];
          
          const index = adjs.indexOf(1);
          
          if(index === -1){
            // if(v1 === 1)
            //   continue ray_loop;
            
            set(x, y, v1);
            continue sp_loop;
          }
          
          check_adj: {
            for(let i = 0; i !== 4; i++)
              if(adjs[i * 2 + 1] === v1)
                break check_adj;
            
            continue ray_loop;
          }
          
          let num = 0;
          
          for(let i = 0; i !== 8; i++)
            if(adjs[i] === 1) num++;
          
          if(v1 === 0 && num >= 8)
            continue ray_loop;
          
          num--;
          
          for(const di of [-1, 1]){
            for(let i = index + di; num !== 0; i += di){
              if(adjs[i & 7] !== 1) break;
              num--;
            }
          }
          
          if(num !== 0)
            continue ray_loop;
          
          set(x, y, v1);
          continue sp_loop;
        }
      }
    }
    
    data.put();
    
    // if(f === 10){
    //   data.put();
    //   media.renderImage('-dw/render.png', w, h, (w, h, g1) => {
    //     g1.drawImage(g.canvas, 0, 0);
    //   }, () => {
    //     exit();
    //   });
    //   return 0;
    // }

    return f !== framesNum;
  }, () => {
    exit();
  });
};

const get_out_file = (vid=0) => {
  return format.path('-render/1.mp4');
  // if(vid || !HD) return format.path('-vid/1.mp4');
  // const project = path.parse(__dirname).name;
  // return format.path(`-render/${project}.mp4`);
};

const exit = (...args) => {
  if(args.length !== 0) log(...args);
  window.close();
};

process.on('uncaughtException', exit);
main().catch(exit);