'use strict';

const fs = require('fs');
const path = require('path');
const cp = require('child_process');
const O = require('../js-util');
const readline = require('../readline');
const format = require('../format');

const args = process.argv.slice(2);
const file = format.path('C:/Projects/Other/alarm/alarm.mp3');
const dflt_time = [5, 0];

const get_target_time = () => {
  const len = args.length;
  
  if(len === 0)
    return dflt_time;
  
  if(len === 1){
    const arg = args[0];
    
    if(arg === 't'){
      const d = new Date();
      return [d.getHours(), d.getMinutes()];
    }
    
    assert.fail();
  }
  
  assert.fail();
};

const [h, m] = get_target_time();

let proc = null;
let ti = null;
let exit = 0;

const main = () => {
  const rl = readline.rl();
  
  rl.ask('', () => {
    rl.close();
    
    if(proc !== null){
      proc.kill();
      proc = null;
    }
    
    if(ti !== null){
      clearTimeout(ti);
      ti = null;
    }
    
    exit = 1;
  });
  
  const s = [h, m].map(a => a.toString().padStart(2, '0')).join(':');
  log(`Alarm scheduled for ${s}`);
  
  const f = () => {
    if(exit) return;
    
    ti = null;
    
    const d = new Date();
    const h1 = d.getHours();
    const m1 = d.getMinutes();

    if(h1 === h && m1 === m){
      play_alarm().catch(log);
      return;
    }

    ti = setTimeout(f, 30e3);
  };

  f();
};

const play_alarm = async () => {
  try{
    while(1){
      const res = await new Promise((res, rej) => {
        const prog = `C:/Program Files/FFmpeg/bin/latest/ffplay.exe`;
        const args = ``;
        
        proc = cp.spawn(prog, [
          '-loglevel', 'quiet',
          '-nodisp',
          '-autoexit',
          '-fast',
          '-i', file,
        ]);
        
        proc.on('exit', () => {
          if(proc === null) res(0);
          
          proc = null;
          res(1);
        });
      });
      
      if(!res) break;
    }
  }catch{}
};

main();