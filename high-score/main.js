"use strict"

const fs = require("fs")
const path = require("path")
const assert = require("assert")
const crypto = require("crypto")
const cp = require("child_process")
const O = require("../js-util")

const sections_info = [
  [
    [2, 1],
    [2, 0],
  ],
  [
    [0, 1, 4],
    [2, 1],
    [2, 1],
  ],
  [
    [0, 0, 24],
    [2, 1],
  ],
  [
    [1, 0],
    [2, 1],
  ],
  [
    [1, 1],
    [2, 0],
  ],
  [
    [0, 1, 2],
    [2, 1],
  ],
  [
    [0, 0, 8],
    [2, 0],
    [2, 1],
    [2, 0],
  ],
  [
    [0, 0, 1],
    [2, 0],
  ],
  [
    [0, 0, 2],
    [2, 0],
  ],
]

const sections_num = sections_info.length

const main = () => {
  const state0 = [[], [4, 0], O.ca(sections_num, i => 0)]
  const stack = [state0]
  
  let best_score = null
  let best_pth = null
  
  const show_score = () => {
    log(best_score)
    log(best_pth.join(" "))
  }
  
  const push = (pth, keys, state_info) => {
    if(keys.every(n => n === 0)) return
    
    const score = keys[0]
    
    if(best_score === null || score > best_score){
      best_score = score
      best_pth = pth
      show_score()
    }
    
    stack.push([pth, keys, state_info])
  }
  
  while(stack.length !== 0){
    const [pth, keys, state_info] = stack.pop()
    
    for(let i = 0; i !== sections_num; i++){
      const info = sections_info[i]
      const stage = state_info[i]
      
      if(stage === info.length) continue
      
      const [type, ki, details=null] = info[stage]
      const pth1 = [...pth, i]
      const keys1 = keys.slice()
      const state_info1 = state_info.slice()
      
      state_info1[i]++
      
      const f = () => {
        push(pth1, keys1, state_info1)
      }
      
      if(type === 0){
        if(keys[ki] < details) continue
        
        keys1[ki] -= details
        f()
        
        continue
      }
      
      if(type === 1){
        if(keys[ki] === 0) continue
        
        keys1[ki] = 0
        f()
        
        continue
      }
      
      if(type === 2){
        if(keys[ki] === 0) continue
        
        keys1[ki ^ 1] += keys[ki]
        f()
        
        continue
      }
      
      assert.fail()
    }
  }
  
  O.logb()
  show_score()
}

main()