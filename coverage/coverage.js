"use strict"

const fs = require("fs")
const path = require("path")
const test = require("node:test")
const assert = require("assert")
const O = require("../js-util")
const Ebuf = require("../../js-util/src/ebuf")

const {describe} = test

const main = async () => {
  await describe("js-util", async () => {
    await describe("process", async () => {
      await describe("Process", async () => {
        await test("onSigint", async () => {
          await new Promise((res, rej) => {
            O.proc.once("sigint", () => {
              res()
            })
            
            process.emit("SIGINT")
          })
        })
      })
      
      await describe("Stdin", async () => {
        await test("ref, unref", async () => {
          let {stdin} = O.proc
          
          stdin.ref()
          assert.strictEqual(stdin.refs, 1)
          
          stdin.unref()
          assert.strictEqual(stdin.refs, 0)
        })
        
        await test("onData (without sigint)", async () => {
          let {stdin} = O.proc
          let buf = Buffer.from("Test")
          
          await new Promise((res, rej) => {
            stdin.once("data", buf1 => {
              (async () => {
                assert.deepStrictEqual(buf1, buf)
              })().then(res, rej)
            })
            
            process.stdin.emit("data", buf)
          })
        })
        
        await test("onData (with sigint)", async () => {
          let {stdin} = O.proc
          let buf1 = Buffer.from("Test1")
          let buf2 = Buffer.from("Test2")
          let buf = Buffer.concat([
            buf1, Buffer.from([0x03]), buf2,
          ])
          
          await new Promise((res, rej) => {
            let cnt = 0
            
            let fn = () => {
              cnt++
              if(cnt === 2) res()
            }
            
            stdin.once("data", actual => {
              (async () => {
                let expected = Buffer.concat([
                  buf1, buf2,
                ])
                
                assert.deepStrictEqual(actual, expected)
              })().then(fn, rej)
            })
            
            O.proc.once("sigint", () => {
              fn()
            })
            
            process.stdin.emit("data", buf)
          })
        })
        
        await test("end", async () => {
          await new Promise((res, rej) => {
            O.proc.stdin.once("end", () => {
              res()
            })
            
            process.stdin.emit("end")
          })
        })
      })
    })
    
    // await describe("the require function", async () => {
    //   await test("no arguments", async () => {
    //     assert.throws(() => {
    //       O.node_require_fn()
    //     })
    //   })
    //   
    //   await test("multiple arguments", async () => {
    //     assert.throws(() => {
    //       O.node_require_fn(".", ".")
    //     })
    //   })
    //   
    //   await test("non-string argument", async () => {
    //     assert.throws(() => {
    //       O.node_require_fn(0)
    //     })
    //   })
    //   
    //   await test("non-native module", async () => {
    //     assert.throws(() => {
    //       O.node_require_fn(".")
    //     })
    //   })
    // })
    
    await describe("ExpandableBuffer", async () => {
      test("new (string), getBuf", () => {
        let str = "Test"
        let ebuf = new Ebuf(str)
        
        assert.strictEqual(ebuf.len, str.length)
        assert.deepStrictEqual(ebuf.getBuf(), Buffer.from(str))
      })
      
      test("new (buffer), push", () => {
        let buf1 = Buffer.from("Test1")
        let buf2 = Buffer.from("Test2")
        let ebuf = new Ebuf(buf1)
        
        for(let b of buf2)
          ebuf.push(b)
        
        assert.strictEqual(ebuf.len, buf1.length + buf2.length)
        assert.deepStrictEqual(ebuf.getBuf(), Buffer.concat([buf1, buf2]))
      })
      
      test("new (empty)", () => {
        let ebuf = new Ebuf()
        assert.strictEqual(ebuf.len, 0)
        assert.deepStrictEqual(ebuf.getBuf(), Buffer.alloc(0))
      })
      
      test("reset", () => {
        let buf = Buffer.from("Test1")
        let ebuf = new Ebuf()
        
        for(let b of buf)
          ebuf.push(b)
        
        assert.strictEqual(ebuf.len, buf.length)
        assert.deepStrictEqual(ebuf.getBuf(), buf)
        
        ebuf.reset()
        assert.strictEqual(ebuf.len, 0)
        assert.deepStrictEqual(ebuf.getBuf(), Buffer.alloc(0))
      })
    })
    
    await describe("Set2D", async () => {
      const {Set2D} = O
      
      test("static obj", () => {
        let obj = Set2D.obj()
        let keys = O.keys(obj)
        
        assert.strictEqual(keys.length, 1)
        assert.strictEqual(typeof keys[0], "symbol")
      })
      
      test("new", () => {
        new Set2D([])
        new Set2D([[1, 2]])
        assert.throws(() => {new Set2D([1])})
      })
      
      test("add", () => {
        let set = new Set2D()
        
        set.add(1, 2)
        set.add(1, 3)
        set.add(1, 2)
      })
      
      test("has", () => {
        let set = new Set2D()
        
        set.add(1, 2)
        
        assert(set.has(1, 2))
        assert(!set.has(1, 3))
        assert(!set.has(3, 2))
      })
      
      test("delete", () => {
        let set = new Set2D()
        
        set.add(1, 2)
        set.add(1, 3)
        set.add(3, 2)
        assert(set.has(1, 2))
        assert(set.has(1, 3))
        assert(set.has(3, 2))
        
        set.delete(1, 3)
        assert(set.has(1, 2))
        assert(!set.has(1, 3))
        assert(set.has(3, 2))
        
        set.delete(1, 4)
        assert(set.has(1, 2))
        assert(!set.has(1, 3))
        assert(set.has(3, 2))
        
        set.delete(1, 2)
        assert(!set.has(1, 2))
        assert(!set.has(1, 3))
        assert(set.has(3, 2))
      })
      
      test("iterator", () => {
        let set = new Set2D()
        
        set.add(1, 2)
        set.add(1, 3)
        set.add(3, 2)
        
        let xs = [...set]
        assert.strictEqual(xs.length, 3)
        
        let ys = []
        
        for(let item of xs){
          assert.strictEqual(item.length, 2)
          
          for(let a of item){
            assert.strictEqual(typeof a, "number")
            assert.strictEqual(a, a | 0)
          }
          
          ys.push(item.join(","))
        }
        
        let s = ys.sort().join("|")
        assert.strictEqual(s, "1,2|1,3|3,2")
      })
    })
  })
}

main().catch(log)