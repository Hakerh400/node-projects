"use strict"

const fs = require("fs")
const path = require("path")
const test = require("node:test")
const test_reporters = require("node:test/reporters")
const assert = require("assert")
const O = require("../js-util")

const {run, describe} = test

const cwd = __dirname
const test_file = path.join(cwd, "coverage.js")

const main = () => {
  run({
    files: [test_file],
    coverage: true,
  })
  .on("test:fail", () => {
    process.exitCode = 1
  })
  .compose(test_reporters.spec)
  .pipe(process.stdout)
}

main()