'use strict';

const fs = require('fs');
const path = require('path');
const util = require('util');
const O = require('../js-util');
const assert = require('../assert');

const semver_len = 3;

class Version {
  static semver_len = semver_len;
  
  constructor(semver){
    this.semver = semver;
    
    assert(this.is_standard);
  }
  
  copy(){
    return new Version(this.semver.slice());
  }
  
  get len(){
    return this.semver.length;
  }
  
  get is_standard(){
    return this.len === semver_len;
  }
  
  get major(){
    return this.semver[0];
  }
  
  set major(n){
    this.semver[0] = n;
  }
  
  get minor(){
    return this.semver[1];
  }
  
  set minor(n){
    this.semver[1] = n;
  }
  
  get patch(){
    return this.semver[2];
  }
  
  set patch(n){
    this.semver[2] = n;
  }
  
  cmp(ver){
    const ps1 = this.semver;
    const ps2 = ver.semver;
    
    for(let i = 0; i !== semver_len; i++){
      const p1 = ps1[i];
      const p2 = ps2[i];
      
      if(p1 < p2) return -1;
      if(p1 > p2) return 1;
    }
    
    return 0;
  }
  
  eq(ver){
    return this.cmp(ver) === 0;
  }
  
  str(add_v=0){
    return `${add_v ? 'v' : ''}${this.semver.join('.')}`;
  }
  
  toString(){
    return this.str(1);
  }
  
  [util.inspect.custom](){
    return this.toString();
  }
}

module.exports = Version;