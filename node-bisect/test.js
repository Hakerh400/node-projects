'use strict';

const http = require('http');
const cp = require('child_process');

const port = 1234;

let n = 0;

const server = http.createServer((req, res) => {
  if(n === 0){
    setTimeout(() => {
      server.close();
    });
  }
  
  n++;
  res.end(String(n));
});

server.listen(port);

server.on('close', () => {
  console.log(n === 1 ? 'new' : 'old');
})