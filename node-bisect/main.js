'use strict';

const fs = require('fs');
const path = require('path');
const crypto = require('crypto');
const cp = require('child_process');
const O = require('../js-util');
const assert = require('../assert');
const log_sync = require('../log-sync');
const download = require('../download');
const Version = require('./version');

const {semver_len} = Version;

const node_dist_url = 'https://nodejs.org/dist/';
const main_dir_wrap = 'D:/Users/User';
const hash_alg = 'sha256';
const hash_file = 'SHASUMS256.txt';
const arch = 'win-x64';
const major_start = 10;

const cwd = __dirname;
const proj = path.parse(cwd).base;
const main_dir = path.join(main_dir_wrap, proj);
const vers_dir = path.join(main_dir, 'versions');
const test_dir = path.join(main_dir, 'test');
const orig_test_file = path.join(cwd, 'test.js');
const test_file = path.join(test_dir, 'main.js');

const arch_exe_mp = {
  'win-x64': 'node.exe',
};

let arch_exe_name;
let arch_exe_pth;

const main = async () => {
  arch_exe_name = arch_exe_mp[arch];
  arch_exe_pth = pth_join(arch, arch_exe_name);

  O.del_dir(test_dir);
  O.mk_dirs([main_dir, vers_dir, test_dir]);
  
  fs.copyFileSync(orig_test_file, test_file);
  
  let vers = await get_vers_list();
  
  if(major_start !== null)
    vers = vers.filter(v => v.major >= major_start);
  
  const vers_num = vers.length;
  assert(vers_num !== 0);
  
  let i = 0;
  let j = vers_num - 1;
  
  while(j !== i + 1){
    const k = i + j >> 1;
    assert(k > i);
    assert(k < j);
    
    const ver = vers[k];
    const res = await test_ver(ver);
    
    if(res === 0) i = k;
    else j = k;
  }
  
  O.logb();
  
  log(`Old: ${vers[i].str(0)}`);
  log(`New: ${vers[j].str(0)}`);
};

const test_ver = async ver => {
  const pth = await get_ver_exe(ver);
  
  log_sync(`Testing ${ver.str(0)} `);
  
  const out = await run_exe(pth, [test_file]);
  const res = parse_out(out);
  
  log(`---> ${out}`);
  
  return res;
};

const parse_out = out => {
  if(out === 'old') return 0;
  if(out === 'new') return 1;
  
  assert.fail(out);
};

const get_ver_exe = async (ver, snd_attempt=0) => {
  const major_dir = path.join(vers_dir, String(ver.major));
  O.mk_dir(major_dir);
  
  const pth = path.join(major_dir, ver.str(1));
  
  if(!O.exi(pth))
    await dw_ver(ver, pth);
  
  const hash_expected = await dw_hash(ver);
  
  const hash_actual = crypto.createHash(hash_alg).
    update(O.rfs(pth)).digest().toString('hex');
  
  if(hash_actual !== hash_expected){
    if(snd_attempt) assert.fail();
    
    log(`Corrupted ${ver.str(0)}`);
    fs.unlinkSync(pth);
    
    return get_ver_exe(ver, 1);
  }
  
  return pth;
};

const dw_ver = async (ver, pth) => {
  const url = pth_join(
    node_dist_url,
    ver.str(1),
    arch_exe_pth,
  );
  
  log(`Downloading ${ver.str(0)}`);
  await download(url, pth);
  
  // const out = await run_exe(pth, ['--version']);
  // assert(out === ver.str(1));
};

const dw_hash = async ver => {
  const url = pth_join(
    node_dist_url,
    ver.str(1),
    hash_file,
  );
  
  const s = (await download(url)).toString().trim();
  
  for(const line of O.sanl(s)){
    let s = line.trim();
    
    const index = s.indexOf(' ');
    assert(index !== -1);
    
    const hash = s.slice(0, index).trim();
    const name = s.slice(index + 1).trim();
    
    if(name !== arch_exe_pth) continue;
    
    return hash;
  }
  
  assert.fail();
};

const run_exe = (pth, args) => new Promise((res, rej) => {
  const on_err = err => {
    if(rej === null) return;
    
    rej(err);
    rej = null;
  };
  
  const proc = cp.spawn(pth, args, {
    cwd: test_dir,
  });
  
  proc.on('error', on_err);
  
  for(const s of ['stdin', 'stdout', 'stderr'])
    proc[s].on('error', on_err);
  
  const out_bufs = [];
  const err_bufs = [];
  
  const ael_bufs = (s, bufs) => {
    proc[s].on('data', buf => {
      bufs.push(buf);
    });
  };
  
  ael_bufs('stdout', out_bufs);
  ael_bufs('stderr', err_bufs);
  
  proc.on('exit', exit_code => {
    if(rej === null) return;
    
    const std_err = concat_into_str(err_bufs);
    
    if(std_err.length !== 0){
      on_err(new Error(std_err.trim()));
      return;
    }
    
    if(exit_code !== 0){
      on_err(new Error(`Non-zero exit code: ${exit_code}`));
      return;
    }
    
    const std_out = concat_into_str(out_bufs);
    res(std_out.trim());
  });
});

const get_vers_list = async () => {
  const s = (await download(node_dist_url)).toString();
  const reg = /\<a href="([^"]+)"/g;
  const vers = [];
  
  main_loop: for(const match of O.exec(s, reg)){
    let s = match[1];
    if(!s.startsWith('v')) continue;
    
    s = s.slice(1);
    if(s.endsWith('/')) s = s.slice(0, -1);
    
    const parts = s.split('.');
    if(parts.length !== semver_len) continue;
    
    const ver = [];
    
    for(const s of parts){
      if(!/^\d+$/.test(s)) continue main_loop;
      ver.push(Number(s));
    }
    
    vers.push(new Version(ver));
  }
  
  return vers.sort((a, b) => a.cmp(b));
};

const concat_into_str = bufs => {
  return Buffer.concat(bufs).toString();
};

const pth_join = (...urls) => {
  return urls.reduce((a, b) => pth_join_1(a, b));
};

const pth_join_1 = (a, b) => {
  if(a.endsWith('/')) a = a.slice(0, -1);
  if(b.startsWith('/')) b = b.slice(1);
  
  return `${a}/${b}`;
};

main().catch(log);