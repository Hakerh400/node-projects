'use strict';

const fs = require('fs');
const path = require('path');
const O = require('../omikron');
const assert = require('../assert');
const format = require('../format');

const {min, max} = Math;

const pth_file = format.path('-dw/a/1.txt');

const ext = 'webm';
const quote_char = '\uff02';

const main = () => {
  const dir = O.rfs(pth_file, 1);
  let files = fs.readdirSync(dir);
  
  const episodes_num = []
  
  files = files.map(base => {
    const {name} = path.parse(base);
    const match = name.match(/\bS(\d{1,2})E(\d{1,2})\b/);
    if(match === null) return null;
    
    const season = (match[1] | 0) - 1;
    const episode = (match[2] | 0) - 1;
    
    while(episodes_num.length <= season)
      episodes_num.push(0);
    
    episodes_num[season] = max(episode + 1, episodes_num[season]);
    
    return {
      season,
      episode,
      name,
      pth: path.join(dir, base),
      title: get_title(name),
    };
  }).filter(a => a !== null).sort((a, b) => {
    return a.season - b.season || a.episode - b.episode;
  });
  
  const get_season_dir = season => {
    return path.join(dir, `Season ${season + 1}`);
  };
  
  const seasons_num = episodes_num.length;
  
  for(let season = 0; season !== seasons_num; season++){
    const season_dir = path.join(dir, `Season ${season + 1}`);
    
    if(!fs.existsSync(season_dir))
      fs.mkdirSync(season_dir);
  }
    
  for(const file of files){
    const {season, episode, name, pth, title} = file;
    const season_dir = get_season_dir(season);
    const name1 = `${mk_se_string(season, episode)} - ${title}`;
    const base1 = `${name1}.${ext}`;
    const pth1 = path.join(season_dir, base1);
    
    fs.renameSync(pth, pth1);
  }
};

const get_title = name => {
  if(name.includes(quote_char)){
    let parts = name.split(quote_char);
    assert(parts.length === 3);
    
    return parts[1].trim();
  }
  
  let parts = name.split(' - ');
  let str = O.last(parts);
  parts = str.split('[');
  
  return parts[0].trim();
};

const mk_se_string = (s, e) => {
  const f = n => String(n + 1).padStart(2, 0);
  return `S${f(s)}E${f(e)}`;
};

main();