"use strict"

const fs = require("fs")
const path = require("path")
const O = require("../js-util")
const assert = require("../assert")

class FsItem {
  #stat = null
  #items = null
  #parsed = null
  
  constructor(pth){
    this.pth = path.normalize(pth)
  }
  
  update(){
    this.#stat = null
    this.#items = null
    this.#parsed = null
  }
  
  get stat(){
    if(this.#stat === null)
      this.#stat = O.stat(this.pth)
    
    return this.#stat
  }
  
  get is_file(){
    return this.stat.isFile()
  }
  
  get is_dir(){
    return this.stat.isDirectory()
  }
  
  get items(){
    assert(this.is_dir)
    
    if(this.#items === null){
      let {pth} = this
      
      this.#items = fs.readdirSync(pth).map(name => {
        let pth1 = path.join(pth, name)
        return new FsItem(pth1)
      })
    }
    
    return this.#items
  }
  
  get items_rec(){
    assert(this.is_dir)
    
    let stack = [[this]]
    
    return (function*(){
      while(stack.length !== 0){
        let items = stack.pop()
        
        yield items
        
        let last = O.last(items)
        if(!last.is_dir) continue
        
        items = items.slice(0, -1)
        
        for(let item of last.items.slice().reverse())
          stack.push([...items, item])
      }
    })()
  }
  
  get parsed(){
    if(this.#parsed === null)
      this.#parsed = path.parse(this.pth)
    
    return this.#parsed
  }
  
  get base(){
    return this.parsed.base
  }
  
  get name(){
    return this.parsed.name
  }
  
  get ext(){
    let {ext} = this.parsed
    
    if(ext === "" || ext === ".") return ""
    
    assert(ext[0] === ".")
    return ext.slice(1)
  }
  
  get buf(){
    assert(this.is_file)
    return O.rfs(this.pth)
  }
  
  set buf(buf){
    assert(this.is_file)
    O.wfs(this.pth, buf)
  }
  
  get src(){
    assert(this.is_file)
    return O.rfs(this.pth, 1)
  }
  
  set src(src){
    assert(this.is_file)
    O.wfs(this.pth, src)
  }
  
  lf(){
    this.src = O.lf(this.src)
  }
  
  trim_end(){
    this.src = this.src.trimEnd()
  }
  
  trim_right(){
    return this.trim_end()
  }
  
  rel(pth){
    if(pth instanceof FsItem)
      pth = pth.pth
    
    return path.relative(this.pth, pth)
  }
  
  nav(pth){
    return new FsItem(path.join(this.pth, pth))
  }
  
  remove_new_line(){}
}

const mk_item = pth => {
  return new FsItem(pth)
}

module.exports = {
  FsItem,
  mk: mk_item,
}