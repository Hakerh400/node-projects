"use strict"

const fs = require("fs")
const path = require("path")
const O = require("../js-util")
const assert = require("../assert")
const readline = require("../readline")

let rl = null

const main = async () => {
  rl = readline.rl()
  
  let vid = await get_video_id()
  
  await cleanup()
}

const get_video_id = async () => {
  let vid = await ask("Video:")
  
  vid = vid.trim()
  if(is_video_id_valid(vid)) return vid
  
  return extract_video_id_from_url(vid)
}

const extract_video_id_from_url = url_str => {
  let url = new URL(url_str)
  log(url)
}

const mk_url = url_str => {
  try{
    return new URL(url_str)
  }
}

const is_video_id_valid = vid => {
  return /^[a-zA-Z0-9\-\x5F]{11}$/.test(vid)
}

const ask = prompt => new Promise((res, rej) => {
  if(!prompt.endsWith(" ")) prompt = `${prompt} `
  
  rl.ask(prompt, response => {
    res(response)
  })
})

const on_err = async err => {
  log(err)
  await cleanup()
}

const cleanup = async () => {
  if(rl !== null){
    rl.close()
    rl = null
  }
}

main().catch(on_err).catch(log)