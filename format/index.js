'use strict';

const fs = require('fs');
const path = require('path');
const O = require('../js-util');
const fNum = require('./number');
const fTime = require('./time');
const fPath = require('./path');
const f_bytes = require('./bytes');

module.exports = {
  num: fNum,
  time: fTime,
  path: fPath,
  pth: fPath,
  bytes: f_bytes,
};