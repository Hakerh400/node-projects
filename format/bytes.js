'use strict';

const fs = require('fs');
const path = require('path');
const O = require('../js-util');
const { assert } = require('console');

const units = 'KMGTPE';

const units_num = units.length;

const format = val => {
  const n = 2 ** 10;
  
  if(val < n) return `${val} B`;

  for(let i = 0; i !== units_num; i++){
    let c = units[i];

    val /= n;
    if(val < n) return `${round(val, 1)} ${c}iB`;
  }

  assert.fail();
};

const round = (n, k) => {
  k = 10 ** k;
  return Math.round(n * k) / k;
};

module.exports = format;