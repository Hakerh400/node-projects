'use strict';

const fs = require('fs');
const path = require('path');
const O = require('../omikron');
const assert = require('../assert');
const download = require('../download');
const logStatus = require('../log-status');
const format = require('../format');

const dir = format.path('-dw/a');
const inpFile = path.join(dir, 'inp.txt');
const outFile = path.join(dir, 'out.txt');

const urlBase = 'https://noembed.com/embed?url=https://www.youtube.com/watch?v=';

const main = async () => {
  const ids = O.sanl(O.rfs(inpFile, 1));
  const idsNum = ids.length;
  const titles = [];
  
  const outFd = fs.openSync(outFile, 'a');
  
  const write = data => {
    fs.writeSync(outFd, data);
  };
  
  const getTitle = async id => {
    const url = `${urlBase}${id}`;
    const str = (await download(url)).toString();
    const info = JSON.parse(str);
    
    if(O.has(info, 'title')) return info.title;
    return null;
  };
  
  const getTitleOrError = async id => {
    const title = await getTitle(id);
    if(title !== null) return title
    
    return '/';
  }
  
  for(let i = 30; i <= idsNum; i++){
    logStatus(i, idsNum, 'URL');
    
    const id = ids[i - 1];
    const title = await getTitleOrError(id);
    
    if(i !== 0) write('\n');
    write(title);
  }
  
  fs.closeSync(outFd);
};

main().catch(log);