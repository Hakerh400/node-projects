'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('assert');
const O = require('../omikron');

const size = 6

// O.enhanceRNG(1)
// O.randSeed(3)

const main = () => {
  const N = Symbol();
  
  const mk_var = i => {
    return O.sfcc(0x61 + i - 2);
  };
  
  const gen_type = () => {
    const t = [];
    const queue = [t];
    
    while(queue.length !== 0 && O.rand(2) !== 0){
      const t = O.randElem(queue, 1);
      const a = [];
      const b = [];
      
      t.push(a, b);
      queue.push(a, b);
    }
    
    const finish_type = function*(t){
      if(t.length === 0) return N;
      
      const [a, b] = t;
      const a1 = yield [finish_type, a];
      const b1 = yield [finish_type, b];
      
      return [a1, b1];
    };
    
    return O.rec(finish_type, t);
  };
  
  const args = [
    ['L', [N, [N, N]]],
    ['M', [[N, N], N]],
  ];
  
  const queue = [];
  
  const add_term_to_queue = function*(args, t){
    const args_num = args.length;
    const e = [];
    
    if(t === N){
      queue.push([args, e]);
    }else{
      const [a, b] = t;
      const name = mk_var(args_num);
      const args1 = [...args, [name, a]];
      const e1 = yield [add_term_to_queue, args1, b];
      
      e.push('lam', name, a, e1);
    }
    
    return e;
  };
  
  const f = O.rec(add_term_to_queue, args, [N, N]);
  
  const func_type_to_arr = function*(t){
    if(t === N) return [];
    
    const [a, b] = t;
    const ts = yield [func_type_to_arr, b];
    
    return [a, ...ts];
  };
  
  while(queue.length !== 0 && O.rand(size) !== 0){
    const [args, e] = O.randElem(queue, 1);
    const args_num = args.length;
    const arg_index = O.rand(args_num + 1);
    
    if(arg_index === args_num){
      const t = gen_type();
      const e1 = O.rec(add_term_to_queue, args, [t, N]);
      const e2 = O.rec(add_term_to_queue, args, t);
      
      e.push('app', e1, e2);
      
      continue;
    }
    
    const [name, type] = args[arg_index];
    const ts = O.rec(func_type_to_arr, type);
    
    let e1 = ['lit', name];
    
    for(const t of ts){
      const e2 = O.rec(add_term_to_queue, args, t);
      e1 = ['app', e1, e2];
    }
    
    for(const x of e1)
      e.push(x)
  }
  
  for(const [args, e] of queue)
    e.push('lit', '0');
  
  const type_to_str = function*(t){
    if(t === N) return 'ℕ';
    
    const [a, b] = t;
    const a_str_raw = yield [type_to_str, a];
    const a_str = a !== N ? `(${a_str_raw})` : a_str_raw;
    const b_str = yield [type_to_str, b];
    
    return `${a_str} → ${b_str}`;
  };
  
  const term_to_str = function*(e){
    const t = e[0];
    
    e = e.slice(1);
    
    if(t === 'lit')
      return e[0];
    
    if(t === 'lam'){
      const [name, type, body] = e;
      const type_str = yield [type_to_str, type];
      const body_str = yield [term_to_str, body];
      return `λ (${name} : ${type_str}), ${body_str}`
    }
    
    if(t === 'app'){
      const [a, b] = e;
      const ta = a[0];
      const tb = b[0];
      const a_str_raw = yield [term_to_str, a];
      const b_str_raw = yield [term_to_str, b];
      const a_str = ta === 'lam' ? `(${a_str_raw})` : a_str_raw;
      const b_str = tb === 'lam' || tb === 'app' ? `(${b_str_raw})` : b_str_raw;
      return `${a_str} ${b_str}`;
    }
    
    assert.fail(t);
  };
  
  const str = O.rec(term_to_str, f);
  
  log(str);
};

main();