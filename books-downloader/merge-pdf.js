'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('assert');
const O = require('../omikron');
const mext = require('D:/Git/easy-pdf-merge/source');

const opts = {
  maxBuffer: 1 << 20,
  maxHeap: '10m',
};

const merge = (...args) => new Promise((res, rej) => {
  mext(...args, opts, err => {
    if(err) return rej(err);
    res();
  });
});

module.exports = merge;