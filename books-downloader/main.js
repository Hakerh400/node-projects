'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('assert');
const urlm = require('url');
const O = require('../omikron');
const download = require('../download');
const format = require('../format');
const logStatus = require('../log-status');
const mergePdf = require('./merge-pdf');

const pdfsDir = format.path('-dw/pdfs');
const pdfFile = format.path('-dw/output.pdf');

const main = async () => {
  assert(!fs.existsSync(pdfsDir));
  assert(!fs.existsSync(pdfFile));
  
  fs.mkdirSync(pdfsDir);
  
  const url = 'https://booksvooks.com/...';
  const pagesNum = await getPagesNum(url);
  const pad = String(pagesNum).length;
  const files = [];
  
  for(let i = 1; i <= pagesNum; i++){
    logStatus(i, pagesNum, 'page');
    
    const pageUrl = getPageUrl(url, i);
    const data = await downloadPage(pageUrl);
    const file = path.join(pdfsDir, `${String(i).padStart(pad, '0')}.pdf`);
    
    files.push(file);
    O.wfs(file, data);
  }
  
  let mergeIndex = 0;
  
  const mergePdfs = async files => {
    const len = files.length;
    assert(len !== 0);
    
    if(len === 1)
      return files[0];
    
    const i = len >> 1;
    const files1 = files.slice(0, i);
    const files2 = files.slice(i);
    const file = path.join(pdfsDir, `a${++mergeIndex}.pdf`);
    const file1 = await mergePdfs(files1);
    const file2 = await mergePdfs(files2);
    
    await mergePdf([file1, file2], file);
    
    return file;
  };

  const file = await mergePdfs(files);
  fs.copyFileSync(file, pdfFile);
  
  for(const file of files)
    fs.unlinkSync(file);
  
  fs.rmdirSync(pdfsDir);
};

const getPagesNum = async url => {
  const data = String(await download(url));
  const match = data.match(/\<h1\>[^\<\|]+\| Page \d+ of (\d+)\</);
  assert(match !== null);
  
  const pagesNum = Number(match[1]);
  assert(pagesNum > 0);
  
  return pagesNum;
};

const getPageUrl = (url, i) => {
  const u = new urlm.URL(url);
  u.searchParams.set('page', i);
  return u.href;
};

const downloadPage = async url => {
  const data = String(await download(url));
  const match = data.match(/pdfData = atob\(\"([^"]+)/);
  assert(match !== null);
  
  return Buffer.from(match[1], 'base64');
};

main().catch(log);