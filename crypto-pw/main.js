'use strict';

const fs = require('fs');
const path = require('path');
const crypto = require('crypto');
const O = require('../js-util');
const assert = require('../assert');
const format = require('../format');

const inp = 'test';

const main = () => {
  let buf = Buffer.from(inp);

  let num = 0;
  let sum = 0;
  let min = null;

  while(1){
    const hash = calc_hash(buf);
    
    buf = Buffer.from([...hash].filter(byte => {
      return byte >= 33 && byte <= 126;
    }));

    const len = buf.length;

    num++;
    sum += len;
    if(min === null || len < min) min = len;
    if(num % 1e4 === 0)
      log(`${min} ${(sum / num).toFixed(3)} ${format.num(num)}`);

    if(len === 20) break;
  }

  O.logb();
  log(buf.toString());
};

const calc_hash = buf => {
  const bufs = [];

  for(let i = 0; i !== 2; i++){
    buf = crypto.createHash('sha512').
      update(buf).digest();
    
    bufs.push(buf);
  }

  return Buffer.concat(bufs);
};

main();