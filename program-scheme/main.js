'use strict';

const fs = require('fs');
const path = require('path');
const urlm = require('url');
const O = require('../omikron');
const assert = require('../assert');
const download = require('../download');
const format = require('../format');
const logStatus = require('../log-status');

const outDir = format.path('-dw/a');
const urlBase = 'https://www.rts.rs/page/tv/sr/broadcast';

const channels = [
  ['rts-1', 20],
  ['rts-2', 21],
  ['rts-3', 255],
];

const startDate = [1, 7, 2008];
const endDate = [1, 1, 2011];

const main = async () => {
  const channelsNum = channels.length;
  const daysNum = daysDif(startDate, endDate) + 1;
  // const totalDaysNum = daysNum * channelsNum;
  
  for(let chIndex = 0; chIndex !== channelsNum; chIndex++){
    const channel = channels[chIndex];
    const [chName, chId] = channel;
    
    const outFile = path.join(outDir, `${chName}.txt`);
    const url = new urlm.URL(urlBase);
    
    url.pathname = path.join(url.pathname, String(chId), chName);
    url.searchParams.set('type', '8');
    
    const info = [];
    
    if(fs.existsSync(outFile)){
      const infoPrev = O.rfs(outFile, 1);
      if(infoPrev !== 0) info.push(infoPrev);
    }
    
    let date = startDate;
    let i = 0;
    
    logStatus.reset();
    
    while(dateLe(date, endDate)){
      const msg = `channel ${O.sf(chName)} ${chIndex+1}/${channelsNum} day`;
      logStatus(++i, daysNum, msg);
      
      setUrlParams(url, {
        day: date[0],
        month: date[1],
        year: date[2],
      });
    
      let str = (await download(url.href)).toString();
    
      let index = str.indexOf('<div id="programska-sema">');
      assert(index !== -1);
      str = str.slice(index);
    
      index = str.indexOf('<div id="right">');
      assert(index !== -1);
      str = str.slice(0, index);
    
      const movies = O.match(str, /\<div class="name">.*?\<\/div\>/gs).
        map(a => removeAllTags(a)).
        map(a => removeSuffix(a, ', film'));
    
      info.push(`-- ${dateToStr(date)}${movies.map(a => `\n${a}`).join('')}`);
      date = nextDate(date);
    }
    
    O.wfs(outFile, info.join('\n\n'));
  }
};

const reversed = (f, str) => {
  return O.rev(f(O.rev(str)));
};

const hasTag = str => {
  if(str[0] !== '<') return 0;
  if(O.last(str) !== '>') return 0;
  
  return 1;
};

const removeTag = str => {
  assert(hasTag(str));
  
  let index = str.indexOf('>');
  assert(index !== -1);
  str = str.slice(index + 1).trimLeft();
  
  index = str.lastIndexOf('<');
  assert(index !== -1);
  str = str.slice(0, index).trimRight();
  
  return str;
};

const removeAllTags = str => {
  str = str.trim();
  
  while(hasTag(str)){
    const str1 = removeTag(str);
    assert(str1 !== str);
    
    str = str1;
  }
  
  return str;
};

const removeSuffix = (str, s) => {
  if(!str.endsWith(s)) return str;
  return str.slice(0, -s.length);
};

const dateInfoToDateObj = date => {
  const obj = new Date(0);
  
  obj.setDate(date[0]);
  obj.setMonth(date[1] - 1);
  obj.setFullYear(date[2]);
  
  return obj;
};

const dateObjToDateInfo = obj => {
  const day = obj.getDate();
  const month = obj.getMonth() + 1;
  const year = obj.getFullYear();
  
  return [day, month, year];
};

const dateInfoToMs = date => {
  return Date.UTC(date[2], date[1] - 1, date[0]);
};

const dateEq = (date1, date2) => {
  for(let i = 0; i !== 3; i++)
    if(date1[i] !== date2[i]) return 0;
  
  return 1;
};

const dateLeOrLt = (date1, date2, le) => {
  for(let i = 2; i !== -1; i--){
    const a = date1[i];
    const b = date2[i];
    
    if(a < b) return 1;
    if(a > b) return 0;
  }
  
  return le;
};

const dateLt = (date1, date2) => {
  return dateLeOrLt(date1, date2, 0);
};

const dateLe = (date1, date2) => {
  return dateLeOrLt(date1, date2, 1);
};

const dateOffset = (date, offset) => {
  const obj = dateInfoToDateObj(date);
  obj.setDate(date[0] + offset);
  return dateObjToDateInfo(obj);
};

const nextDate = date => {
  return dateOffset(date, 1);
};

const prevDate = date => {
  return dateOffset(date, -1);
};

const dateToStr = date => {
  return date.map(a => `${String(a).padStart(2, '0')}.`).join('');
};

const setUrlParams = (url, obj) => {
  for(const key of O.keys(obj))
    url.searchParams.set(key, obj[key]);
  
  return url;
};

const putIn = (str1, str2, str) => {
  return str1 + str + str2;
};

const putIn2 = (str1, str) => {
  return putIn(str1, str2, str);
};

const daysDif = (date1, date2) => {
  const ms = 24 * 60 * 60 * 1e3;
  const d1 = dateInfoToMs(date1);
  const d2 = dateInfoToMs(date2);

  const days = (d2 - d1) / ms;
  assert(days === (days | 0));

  return days;
};

main().catch(log);