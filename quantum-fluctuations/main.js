"use strict"

const fs = require("fs")
const path = require("path")
const cp = require("child_process")
const O = require("../js-util")
const assert = require("../assert")
const media = require("../media-node")
const log_status = require("../log-status")
const format = require("../format")

const {min, max, abs, sin, cos} = Math
const {pi, pi2} = O

const HD = 1

const w = HD ? 1920 : 640
const h = HD ? 1080 : 480
const fps = 60
const fast = !HD

const [wh, hh] = [w, h].map(a => a >> 1)
const [w1, h1] = [w, h].map(a => a - 1)

const duration = 60 * 10
const framesNum = fps * duration

const rad = 50
const diam = rad * 2

const main = () => {
  const out_file = get_out_file()
  const col = Buffer.alloc(3)
  
  const pn = w * h
  const grid = new O.Grid(w, h, () => [0, 0])
  
  for(let cnt = 0; cnt !== pn;){
    if(cnt > pn) assert.fail()
    
    const x0 = O.rand(w)
    const y0 = O.rand(h)
    
    const x1 = max(x0 - rad, 0)
    const y1 = max(y0 - rad, 0)
    const x2 = min(x0 + rad, w1)
    const y2 = min(y0 + rad, h1)
    
    for(let y = y1; y <= y2; y++){
      for(let x = x1; x <= x2; x++){
        const info = grid.get(x, y)
        const [visited, val] = info
        
        const dist = O.dist(x, y, x0, y0)
        const v = max((1 - dist / rad) * 0.5, 0)
        
        if(v === 0) continue;
        
        if(!visited){
          cnt++
          info[0] = 1
        }
        
        info[1] = (val + v) % 1
      }
    }
  }
  
  grid.iter((x, y, info) => {
    const val = (info[1] + 1) % 1
    grid.set(x, y, val)
  })
  
  media.renderVideo(out_file, w, h, fps, fast, (w, h, imgd, f) => {
    log_status(f, framesNum)
    
    imgd.iter((x, y) => {
      const v = grid.get(x, y)
      // const k = abs((v + f * 0.005) % 1 - 0.5) * 2
      const k = v + f * 0.01
      
      return O.hsv(k, col)
    })
    
    return f !== framesNum
  })
}

const get_out_file = (vid=0) => {
  const project = path.parse(__dirname).name
  const name = vid ? project : "1"
  return format.path(`-render/${name}.mp4`)
}

main()