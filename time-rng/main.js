'use strict';

const fs = require('fs');
const path = require('path');
const O = require('../js-util');
const assert = require('../assert');

const bits_num = 1e3;

const main = () => {
  test_rng(O.rand);
  log();
  test_rng(rand_bit);
};

const test_rng = f => {
  const bits = O.ca(bits_num, () => f());
  log(bits.join(''));
};

const rand_bit = () => {
  const t = cur_time();
  let b = 0;
  
  while(cur_time() === t)
    b ^= 1;
  
  return b;
};

const cur_bit = () => {
  return cur_time() & 1;
};

const cur_time = () => {
  return Date.now();
};

main();