'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('assert');
const O = require('../omikron');

const pp = {
  nat: 1,
};

// let id = 0;

class Expr{
  fullyReduced = 0;
  
  constructor(vari){
    this.vari = vari;
    // this.id = id++;
  }
  
  get ctor(){ return this.constructor; }
  
  get isNat(){ return 0; }
  get isNil(){ return 0; }
  get isPair(){ return 0; }
  get isFunc(){ return 0; }
  get isCall(){ return 0; }
  get isRef(){ return 0; }
  get isArg(){ return 0; }
  
  *toStr(ps){ O.virtual('toStr'); }
  
  toString(){
    return O.rec([this, 'toStr'], 1);
  }
}

class Nat extends Expr{
  constructor(val, cacheNil=1){
    super(0);
    
    assert(typeof val === 'bigint');
    assert(val >= 0n);
    
    if(cacheNil && val === 0n)
      return nil;
    
    this.val = val;
  }
  
  get isNat(){ return 1; }
  get isNil(){ return this.val === 0n; }
  
  *toStr(ps){
    return String(this.val);
  }
}

const nil = new Nat(0n, 0);

class Pair extends Expr{
  constructor(fst, snd){
    if(fst.isNil && snd.isNat)
      return new Nat(snd.val + 1n);
    
    super(fst.vari || snd.vari);
    
    this.fst = fst;
    this.snd = snd;
  }
  
  get isPair(){ return 1; }
  
  *toStr(ps){
    const fst = yield [[this.fst, 'toStr'], 1];
    const snd = yield [[this.snd, 'toStr'], 0];
    return addParens(ps, 1, `${fst}, ${snd}`);
  }
}

class Func extends Expr{
  constructor(arity, body, optimized, args=[]){
    super(0);
    
    assert(arity !== 0);
    assert(args.length < arity);
    
    this.arity = arity;
    this.body = body;
    this.optimized = optimized;
    this.args = args;
  }
  
  get isFunc(){ return 1; }
  
  *toStr(ps){
    return '<function>';
  }
}

class Call extends Expr{
  constructor(target, arg, reduced=null){
    super(target.vari || arg.vari);
    
    this.target = target;
    this.arg = arg;
    this.reduced = reduced;
  }
  
  setReduced(reduced){
    this.reduced = reduced;
    this.vari = 0;
  }
  
  get isCall(){ return 1; }
  
  *toStr(ps){
    const target = yield [[this.target, 'toStr'], 1];
    const arg = yield [[this.arg, 'toStr'], 2];
    return addParens(ps, 2, `${target} ${arg}`);
  }
}

class Ref extends Expr{
  constructor(name){
    super(0);
    this.name = name;
  }
  
  get isRef(){ return 1; }
  
  *toStr(ps){
    return this.name;
  }
}

class Arg extends Expr{
  constructor(index){
    super(1);
    this.index = index;
  }
  
  get isArg(){ return 1; }
  
  *toStr(ps){
    // return `{${this.index}}`;
    assert.fail();
  }
}

const addParens = (ps, m, s) => {
  if(ps >= m) return `(${s})`;
  return s;
};

module.exports = {
  nil,
  
  Expr,
  Nat,
  Pair,
  Func,
  Call,
  Ref,
  Arg,
};