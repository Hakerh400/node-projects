'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('assert');
const O = require('../omikron');
const cs = require('./ctors');

const {nil} = cs;

const cwd = __dirname;
const srcDir = path.join(cwd, 'src');
const srcFile = path.join(srcDir, 'src.txt');
const inpFile = path.join(srcDir, 'inp.txt');

const main = () => {
  O.bion(1);
  
  const src = O.rfs(srcFile, 1);
  const inp = O.rfs(inpFile, 1);
  const prog = parse(src);
  
  const t = O.now;
  const out = run(prog, inp);
  
  log(`Time: ${((O.now - t) / 1e3).toFixed(3)}\n`);
  log(out);
};

const parse = str => {
  const unescapeCache = O.obj();
  let auxIdentsNum = 0;
  
  const getAuxIdent = () => {
    return `\x5F${auxIdentsNum++}`;
  };
  
  const unescape = c => {
    if(O.has(unescapeCache, c))
      return unescapeCache[c];
    
    const c1 = new Function(`return "\\${c}"`)();
    unescapeCache[c] = c1;
    
    return c1;
  };
  
  const parseEscapedStr = (str, trim=0) => {
    if(trim) str = str.slice(1, -1);
    return str.replace(/\\(.)/gs, (a, b) => unescape(b));
  };
  
  const parseStr = str => {
    const elems = [...parseEscapedStr(str, 1)].map(a => a.codePointAt(0));
    return `[${elems.join(',')}]`;
  };
  
  const parseChar = str => {
    return String(parseEscapedStr(str, 1).codePointAt(0));
  };
  
  str = O.lf(str).
    replace(/"(?:\\[\s\S]|[^\\"])*"/g, parseStr).
    replace(/(?<=^|[\s\(\)\[\]])'(?:\\[\s\S]|[^\\'])'/g, parseChar).
    replace(/\-{2}[^\n]*/g, '').
    replace(/;\s+/g, '\n').
    replace(/\(\s*\)/g, '(#)').
    replace(/\[/g, '(').
    replace(/\]/g, ',#)').
    replace(/\(\s*,/g, '(').
    replace(/,\s*([,\)])/g, (a, b) => b).
    replace(/([^a-zA-Z0-9\x5F\'])return([^a-zA-Z0-9\x5F\'])/g, (a, b, c) => `${b}pure|${c}`);
  
  str = O.sanl(str).
    map(line => line.trimRight()).
    filter(line => line.length !== 0).join('\n');
  
  const unfoldDoBlock = (fname, argsStr, name, expr, f) => {
    const aux = getAuxIdent();
    const argsArr = O.match(argsStr, /\S+/g);
    const argsArr1 = argsArr.filter(a => a !== name);
    const argsArr2 = [...argsArr1, name];
    const args = argsArr.join(' ');
    const args1 = argsArr1.join(' ');
    const args2 = argsArr2.join(' ');
    
    return f(aux, args, args1, args2);
  };
  
  while(1){
    const strPrev = str;
    
    str = str.
      replace(/([a-zA-Z0-9\x5F\']+)((?: +[a-zA-Z0-9\x5F\']+)*) *= *do\n +([a-zA-Z0-9\x5F\']+) *= *([^\n]+)(?=\n )/g, (a, fname, argsStr, name, expr) => {
        return unfoldDoBlock(fname, argsStr, name, expr, (aux, args, args1, args2) => `${fname} ${args} = ${aux} ${args1} | ${expr}\n${aux} ${args2} = do`);
      }).
      replace(/([a-zA-Z0-9\x5F\']+)((?: +[a-zA-Z0-9\x5F\']+)*) *= *do\n +([a-zA-Z0-9\x5F\']+) *\<\- *([^\n]+)(?=\n )/g, (a, fname, argsStr, name, expr) => {
        return unfoldDoBlock(fname, argsStr, name, expr, (aux, args, args1, args2) => `${fname} ${args} = bind (${expr}) | ${aux} ${args1}\n${aux} ${args2} = do`);
      }).
      replace(/([a-zA-Z0-9\x5F\']+)((?: +[a-zA-Z0-9\x5F\']+)*) *= *do\n +([^\n]+)(?=\n )/g, (a, fname, args, expr) => {
        if(/\=|\<\-/.test(expr)) return a;
        args = args.trim();
        const aux = getAuxIdent();
        return `${fname} ${args} = seq (${expr}) | ${aux} ${args}\n${aux} ${args} = do`;
      }).
      replace(/([a-zA-Z0-9\x5F\']+)((?: +[a-zA-Z0-9\x5F\']+)*) *= *do\n +([^\n]+)(?=\n(?! )|$)/g, (a, fname, args, expr,m) => {
        args = args.trim();
        const aux = getAuxIdent();
        return `${fname} ${args} = ${expr}`;
      });
    
    if(str === strPrev) break;
  }
  
  // log('main'+O.last(str.split(/main/)).replace(/ +/g, ' '));
  // O.exit();
  
  str = O.sanl(str);
  
  let lines = [];
  
  for(const line of str){
    if(/^\s/.test(line)){
      assert(lines.length !== 0);
      const last = O.last(lines);
      O.setLast(lines, `${last} ${line.trimLeft()}`);
      continue;
    }
    
    lines.push(line);
  }
  
  const nneg = a => {
    if(a < 0n) return 0n;
    return a;
  };
  
  const defs = {
    dbg,
    eq,
    
    le: (a, b) => a <= b ? 1n : 0n,
    ge: (a, b) => a >= b ? 1n : 0n,
    lt: (a, b) => a < b ? 1n : 0n,
    gt: (a, b) => a > b ? 1n : 0n,
    add: (a, b) => a + b,
    sub: (a, b) => nneg(a - b),
    mul: (a, b) => a * b,
    div: (a, b) => b ? a / b : 0n,
    mod: (a, b) => a % b,
    pow: (a, b) => a ** b,
    root: (a, b) => O.rootn(a, b),
    log: (a, b) => O.logn(a, b),
  };
  
  for(const line of lines){
    const ps = line.split('=').map(a => O.match(a, /(?:[\#\(\)\,\|]|[^\#\(\)\,\|\s]+)/g));
    assert(ps.length === 2, line);
    
    const [lhs, rhs] = ps;
    assert(lhs.length !== 0);
    
    const name = lhs.shift();
    assert(/^(?:[a-zA-Z]|\x5F.)/s.test(name), name);
    
    if(O.has(defs, name)){
      const def = defs[name];
      
      if(typeof def !== 'function')
        assert.fail(name);
    }
    
    defs[name] = ps;
  }
  
  for(const name of O.keys(defs)){
    const def = defs[name];
    
    if(typeof def === 'function'){
      defs[name] = new cs.Func(def.length, def, 1)
      continue;
    }
    
    const [argsArr, toks] = def;
    const arity = argsArr.length;
    const toksNum = toks.length;
    
    const args = O.obj();
    
    for(let i = 0; i !== arity; i++){
      const arg = argsArr[i];
      assert(!O.has(args, arg));
      args[arg] = i;
    }
    
    let index = 0;
    
    const eof = () => {
      return index === toksNum;
    };
    
    const next = (advance=1) => {
      assert(!eof());
      const tok = toks[index];
      if(advance) index++;
      return tok;
    };
    
    const parseIdent = function*(name){
      if(name === '#') return nil;
      
      if(/^\d+$/.test(name))
        return new cs.Nat(BigInt(name));
      
      if(O.has(args, name))
        return new cs.Arg(args[name]);
      
      if(O.has(defs, name))
        return new cs.Ref(name);
      
      assert.fail(name);
    };
    
    const parseTerm = function*(){
      const tok = next();
      
      if(tok === '('){
        const expr = yield [parseExpr];
        assert(next() === ')');
        return expr;
      }
      
      return O.tco(parseIdent, tok);
    };
    
    const parseExpr = function*(){
      const tok = next(0);
      
      if(tok === ')' || tok === ',' || tok === '|')
        assert.fail();
      
      let expr = yield [parseTerm];
      
      while(!eof()){
        const tok = next(0);
        if(tok === ')') break;
        
        if(tok === ','){
          next();
          const snd = yield [parseExpr];
          return new cs.Pair(expr, snd);
        }
        
        if(tok === '|'){
          next();
          const snd = yield [parseExpr];
          return new cs.Call(expr, snd);
        }
        
        const arg = yield [parseTerm];
        expr = new cs.Call(expr, arg);
      }
      
      return expr;
    };
    
    const body = O.rec(parseExpr);
    assert(eof());
    
    if(arity === 0){
      defs[name] = body;
      continue;
    }
    
    defs[name] = new cs.Func(arity, body, 0);
  }
  
  return defs;
};

const run = (defs, inp) => {
  assert(O.has(defs, 'main'));
  
  const warns = O.obj();
  
  const warn = msg => {
    if(O.has(warns, msg)) return;
    warns[msg] = 1;
    
    log(`WARNING: ${msg}`);
  };
  
  const mkProj = (n, k) => {
    return new cs.Func(n, new cs.Arg(k), 0);
  };
  
  const projs = [
    new cs.Func(2, new cs.Arg(1), 0),
    new cs.Func(2, new cs.Arg(0), 0),
    new cs.Func(2, nil, 0),
  ];
  
  const subst = function*(expr, args){
    if(!expr.vari) return expr;
    
    if(expr.isArg){
      const {index} = expr;
      assert(index < args.length);
      return args[index];
    }
    
    if(expr.isPair){
      const fst = yield [subst, expr.fst, args];
      const snd = yield [subst, expr.snd, args];
      return new cs.Pair(fst, snd);
    }
    
    if(expr.isCall){
      const target = yield [subst, expr.target, args];
      const arg = yield [subst, expr.arg, args];
      return new cs.Call(target, arg);
    }
    
    assert.fail(expr.ctor.name);
  };
  
  const reduceCall = function*(target, arg){
    assert(!target.isCall);
    
    if(target.isNil){
      arg = yield [reduce, arg];
      
      if(arg.isNil) return projs[0];
      if(arg.isPair) return projs[1];
      if(arg.isNat) return projs[arg.val === 0n ? 0 : 1];
      if(arg.isFunc) return projs[2];
      
      assert.fail(arg.ctor.name);
    }
    
    if(target.isPair){
      const {fst, snd} = target;
      const expr = new cs.Call(new cs.Call(arg, fst), snd);
      return O.tco(reduce, expr);
    }
    
    if(target.isFunc){
      const {arity, body, args: argsOld, optimized: opz} = target;
      const args = [...argsOld, arg];
      const argsNum = args.length;
      
      if(argsNum !== arity)
        return new cs.Func(arity, body, opz, args);
      
      if(opz){
        const vals = [];
        
        if(body === eq){
          const expr1 = yield [fullReduce, args[0]];
          const expr2 = yield [fullReduce, args[1]];
          const b = yield [eq, expr1, expr2];
          return new cs.Nat(b ? 1n : 0n);
        }
        
        if(body === dbg){
          // log('DEBUG');
          // O.logb();
          
          const val = yield [fullReduce, args[0]];
          return dbg(val);
        }
        
        for(let i = 0; i !== arity; i++){
          const expr = yield [fullReduce, args[i]];
          if(!expr.isNat) return nil;
          
          vals.push(expr.val);
        }
        
        return new cs.Nat(body(...vals));
      }
      
      const expr = yield [subst, body, args];
      return O.tco(reduce, expr);
    }
    
    if(target.isNat){
      const {val} = target;
      if(val === 0n) return O.tco(reduceCall, nil, arg);
      
      const expr = new cs.Call(new cs.Call(arg, nil), new cs.Nat(val - 1n));
      return O.tco(reduce, expr);
    }
    
    assert.fail(target.ctor.name);
  };
  
  const reduce = function*(expr){
    if(expr.isNat) return expr;
    if(expr.isPair) return expr;
    if(expr.isFunc) return expr;
    
    if(expr.isCall){
      const {target, arg, reduced} = expr;
      if(reduced !== null) return reduced;
      
      const t = yield [reduce, target];
      const r = yield [reduceCall, t, arg];
      
      expr.setReduced(r);
      return r;
    }
    
    if(expr.isRef)
      return O.tco(reduce, defs[expr.name]);
    
    assert.fail(expr.ctor.name);
  };
  
  const fullReduce = function*(expr){
    expr = yield [reduce, expr];
    if(expr.fullyReduced) return expr;
    
    if(expr.isNat || expr.isFunc){
      expr.fullyReduced = 1;
      return expr;
    }
    
    if(expr.isPair){
      const fst = yield [fullReduce, expr.fst];
      const snd = yield [fullReduce, expr.snd];
      const pair = new cs.Pair(fst, snd);
      
      pair.fullyReduced = 1;
      return pair;
    }
    
    errOut();
  };
  
  const inpExpr = O.rec(inpToExpr, [...inp]);
  const mainExpr = new cs.Call(new cs.Ref('main'), inpExpr);
  const outExpr = O.rec(fullReduce, mainExpr);
  // log(outExpr+'');O.exit();
  const out = O.rec(exprToOut, outExpr).join('');
  
  return out;
};

const inpToExpr = function*(inp){
  if(inp.length === 0) return nil;
  
  const [x, ...xs] = inp;
  const n = new cs.Nat(BigInt(x.codePointAt(0)));
  const inpExpr = yield [inpToExpr, xs];
  
  return new cs.Pair(n, inpExpr);
};

const exprToOut = function*(expr){
  if(expr.isNil) return [];
  if(expr.isNat) return O.ca(Number(expr.val), () => '\x00');
  if(!expr.isPair) errOut();
  
  const {fst, snd} = expr;
  const c = String.fromCodePoint(Number(yield [exprToNat, fst]));
  const out = yield [exprToOut, snd];
  
  return [c, ...out];
};

const exprToNat = function*(expr){
  assert(expr.isNat);
  return Number(expr.val);
};

const eq = function*(a, b){
  if(a === b) return 1;
  if(a.ctor !== b.ctor) return 0;
  
  if(a.isNil) return 1;
  if(a.isNat) return a.val === b.val;
  
  if(a.isFunc) assert.fail();
  
  if(a.isPair){
    const eq1 = yield [eq, a.fst, b.fst];
    if(!eq1) return 0;
    return O.tco(eq, a.snd, b.snd);
  }
    
  assert.fail(a.ctor.name);
};

const dbg = expr => {
  O.exit(expr.toString());
};

const errOut = () => {
  // assert.fail();
  err(`ERROR: Invalid output`);
};

const err = msg => {
  O.exit(msg);
};

main();