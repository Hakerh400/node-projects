'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('assert');
const O = require('../omikron');

const cwd = __dirname;
const srcDir = path.join(cwd, 'src');
const rawFile = path.join(srcDir, 'raw.js');
const srcFile = path.join(srcDir, 'src.txt');
const inpFile = path.join(srcDir, 'inp.txt');

const hash = O.sha256('');

const main = () => {
  const raw = O.rfs(rawFile, 1);
  const src = encode(raw);
  
  O.wfs(srcFile, src);
  
  const [func, rest] = decode(src);
  const inp = O.rfs(inpFile, 1);
  
  const result = func(inp, rest, require);
  
  if(typeof result !== 'undefined')
    log(result);
};

const encode = raw => {
  const [code, rest] = split(raw, str => {
    try{
      exec(str);
      return [str];
    }catch(err){
      assert(err instanceof SyntaxError);
      return null;
    }
  });
  
  const buf = O.IO.xor(code, hash);
  
  const codeNew = Buffer.from(
    O.NatSerializer.convertBase(buf, 256, 94, 0).
    map(a => a + 33)).toString();
  
  return O.match(codeNew, /.{100}|.+/g).join('\n') + rest;
};

const decode = src => {
  const [prog, rest] = split(src, str => {
    if(/[^\s!-~]/.test(str)) return null;
    
    const buf = Buffer.from(str.replace(/\s+/g, '')).map(a => a - 33);
    const bufNew = Buffer.from(O.NatSerializer.convertBase(buf, 94, 256, 0));
    const code = O.IO.xor(bufNew, hash).toString();
    
    try{
      return [exec(code)];
    }catch(err){
      assert(err instanceof SyntaxError);
      return null;
    }
  });
  
  return [prog, rest];
};

const split = (list, func) => {
  const len = list.length;
  
  for(let i = len; i !== -1; i--){
    const part = list.slice(0, i);
    const result = func(part);
    
    if(result !== null)
      return [result[0], list.slice(i)];
  }

  assert.fail();
};

const exec = code => {
  return new Function('input', 'code', 'require', code);
};

main();