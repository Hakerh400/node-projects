const https = require('https');

const req = https.get('https://esolangs.org/wiki/Main_Page', req => {
  const bufs = [];

  req.on('data', buf => bufs.push(buf));
  req.on('end', () => {
    log(Buffer.concat(bufs).toString());
  });
});