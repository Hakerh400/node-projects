'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('assert');
const http = require('http');
const O = require('../js-util');
const readline = require('../readline');
const format = require('../format');
const rmi = require('.');
const methods = require('./methods');

const logFile = format.path('-dw/rmi.txt');

const sem = new O.Semaphore();

const rl = readline.rl();

const main = async () => {
  rmi.init();

  rl.on('line', str => {
    cmd([`> ${str}`], async () => {
      str = str.trim();

      if(str === '')
        return;

      if(/^\:?(?:q(?:uit)?|exit)$/.test(str)){
        exit();
        return;
      }

      if(str.includes('.'))
        return await str.split('.').reduce((a, b) => a[b], methods)();

      log(`Unknown command`);
    }).catch(O.nop);
  });
  
  const s = ' Request: [[';
  
  for(const line of O.sanl(O.rfs(logFile, 1))){
    const index = line.indexOf(s);
    if(index === -1) continue;
    
    const str = line.slice(index + s.length - 2);
    await processReq(str);
  }
  
  exit();
};

const cmd = async (cmdInfo, func) => {
  await sem.wait();

  if(Array.isArray(cmdInfo)){
    rmi.log(cmdInfo);
  }else{
    log(cmdInfo);
  }

  log.inc();

  try{
    return await func();
  }catch(err){
    const isErr = err instanceof Error;
    
    log(`Error: ${isErr ? err.stack : err}`);

    if(isErr)
      throw err.message;

    throw err;
  }finally{
    log.dec();
    sem.signal();
  }
};

const processReq = str => {
  return cmd(`Request: ${str}`, async () => {
    const req = JSON.parse(str);
    const [methodPath, args] = req;

    const errPth = () => {
      throw `Unknown method ${O.sf(methodPath.join('.'))}`;
    };

    const method = methodPath.reduce((obj, key) => {
      const val = obj[key];
      if(val === undefined) errPth();
      return val;
    }, methods);

    if(typeof method !== 'function')
      errPth();

    const result = await method(...args);

    if(result !== undefined)
      log(`Result: ${sf(result)}`);

    return result;
  });
};

const setHeaders = res => {
  for(const key of O.keys(headers))
    res.setHeader(key, headers[key]);
};

const exit = () => {
  (async () => {
    await sem.wait();
    rl.close();
    rmi.close();
  })().catch(O.exit);
};

const sf = (val=null) => {
  return JSON.stringify(val);
};

main();