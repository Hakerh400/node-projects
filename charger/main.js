"use strict"

const fs = require("fs")
const path = require("path")
const cp = require("child_process")
const O = require("../js-util")
const assert = require("../assert")

const intval = 1e3

const cwd = __dirname
const audio_file = path.join(cwd, "001.mp3")

let proc = null

const main = () => {
  func()
}

const func = () => {
  let out = cp.execSync("acpi -a").toString()
  let m = out.trim().match(/^Adapter 0: (on|off)-line$/)
  assert(m !== null)
  
  let status = m[1]
  
  if(status === "on" && proc !== null){
    cp.execSync(`pkill -P ${proc.pid}`)
    proc = null
  }
    
  if(status === "off" && proc === null){
    proc = cp.exec(`ffplay` +
      ` -loglevel quiet` +
      ` -loop 0` +
      ` -nodisp -autoexit -fast` +
      ` -i "${audio_file}"`)
  }
  
  setTimeout(func, intval)
}

main()