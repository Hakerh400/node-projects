'use strict';

const fs = require('fs');
const path = require('path');
const O = require('../js-util');
const assert = require('../assert');
const readline = require('../readline');
const translate = require('.');

const rl = readline.rl();

const main = () => {
  get_inp(0);
};

const get_inp = (new_line=1) => {
  if(new_line) log('');
  rl.question('> ', inp => on_inp(inp).catch(on_err));
};

const on_inp = async inp => {
  if(inp === '' || inp === 'q'){
    rl.close();
    return;
  }
  
  const out = await translate(inp);
  log(out);
  
  get_inp();
};

const on_err = err => {
  log(err);
  get_inp();
};

main();