'use strict';

const fs = require('fs');
const path = require('path');
const https = require('https');
const O = require('../js-util');
const assert = require('../assert');

const cwd = __dirname;
const url_file = path.join(cwd, '../../Other/Translate/url.txt');

let url = null;

const translate = inp => new Promise((resolve, reject) => {
  if(url === null)
    url = O.rfs(url_file, 1);
  
  const opts = {
    method: 'POST',
  };
  
  const req = https.request(url, opts, res => {
    const bufs = [];
    
    res.on('data', buf => {
      bufs.push(buf);
    });
    
    res.on('end', () => {
      let out = null;
      
      try{
        const obj_str = Buffer.concat(bufs).toString();
        const obj = JSON.parse(obj_str);
        const key = 'translated';
        
        if(!O.has(obj, key))
          return reject(obj);
        
        out = obj[key];
      }catch(err){
        return reject(err);
      }
      
      resolve(out);
    });
    
    res.on('error', reject)
  });
  
  req.on('error', reject);
  req.end(inp);
});

module.exports = translate;