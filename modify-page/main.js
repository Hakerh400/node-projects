"use strict"

const fs = require("fs")
const path = require("path")
const net = require("net")
const http = require("http")
const https = require("https")
const electron = require("electron")
const O = require("../js-util")

const {
  app,
  protocol,
} = electron

const main = () => {
  process.traceProcessWarnings = 1
  
  protocol.handle("http", async req => {
    let res = await fetch(req.url, {
      method: req.method,
      headers: req.headers,
      body: req.body,
      duplex: "half",
    })
    
    let bytes = await res.clone().bytes()
    
    if(bytes.some(a => a === 0))
      return res
    
    let s = await res.clone().text()
    
    let res1 = new Response(`${s} /* WORKS */`, {
      status: res.status,
      statusText: res.statusText,
      headers: res.headers,
    })
    
    return new Proxy(res1, {
      get(t, prop){
        if(prop === "headers")
          return res.headers
        
        return t[prop]
      },
    })
  })

  const win = new electron.BrowserWindow({
    width: 640,
    height: 480,
    show: false,
    webPreferences: {
      nodeIntegration: false,
      contextIsolation: true,
    },
  })

  win.webContents.openDevTools()

  win.on("ready-to-show", () => {
    win.maximize()
    win.show()
  })

  win.loadURL(`http://localhost/web/`)
}

app.once("ready", main)