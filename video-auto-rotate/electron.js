'use strict';

const fs = require('fs');
const path = require('path');
const O = require('../js-util');

O.isElectron = 1;

const assert = require('../assert');
const media = require('../media');
const format = require('../format');
const log_status = require('../log-status');

const {min, max, sqrt} = Math;

const y_window = 5;

const dir = format.path('-dw/a');
const inp_file = path.join(dir, 'inp.png');
const out_file = path.join(dir, 'out.png');

const [col, col1] = O.ca(2, () => O.ca(3, () => 0));

let w = null;
let h = null;
let mat = null;

const main = async () => {
  const g = await media.loadImage(inp_file);
  
  const {canvas} = g;
  
  w = canvas.width;
  h = canvas.height;
  mat = new Uint8Array(w * h);
  
  const imgd = g.getImageData(0, 0, w, h);
  
  const t = Date.now();
  const angle = get_angle(imgd, 1);
  const dt = Date.now() - t;
  log(format.time(dt / 1e3));
  
  g.putImageData(imgd, 0, 0);
  
  O.body.appendChild(canvas);
  
  // await media.saveImage(out_file, g);
};

const get_angle = (imgd, rot=0) => {
  const {data} = imgd;
  let {width: w, height: h} = imgd;
  
  if(rot){ let t = w; w = h; h = t; }
  
  const calc_index = (x, y) => {
    if(rot) return x * h + y << 2;
    return y * w + x << 2;
  };
  
  init_mat(rot, (y, x) => {
    if(y === 0) return 0;
    
    const y_start = max(0, y - y_window);
    const y_end = y - 1;
    const y_num = y_end - y_start + 1;
    
    if(y_num === 0) assert.fail();
    
    for(let c = 0; c !== 3; c++)
      col[c] = 0;
    
    for(let i = y_start; i <= y_end; i++){
      const j = calc_index(x, i);
      
      for(let c = 0; c !== 3; c++)
        col[c] += data[j + c] | 0;
    }
    
    for(let c = 0; c !== 3; c++)
      col[c] = col[c] / y_num | 0;
    
    const j = calc_index(x, y);
    
    for(let c = 0; c !== 3; c++)
      col1[c] = data[j + c];
    
    let dif = 0;
    
    for(let c = 0; c !== 3; c++){
      const d = col[c] - col1[c] | 0;
      dif += d * d | 0;
    }
    
    dif = sqrt(dif);
    dif = min(255, dif + .5 | 0);
    
    return dif > 20 ? 255 : 0;
  });
  
  mat.iter((x, y, dif) => {
    const j = calc_index(x, y);
    
    for(let c = 0; c !== 3; c++)
      data[j + c] = dif;
  });
};

const init_mat = (rot, f) => {};

main().catch(log);