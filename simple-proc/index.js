"use strict"

const fs = require("fs")
const path = require("path")
const cp = require("child_process")
const O = require("../js-util")
const assert = require("../assert")

const dflt_opts = {
  allow_nzec: 0,
  allow_stderr: 0,
  raw: 0,
  cwd: null,
}

const sts = ["stdin", "stdout", "stderr"]

const run = (...fn_args) => {
  if(fn_args.length === 3 && typeof fn_args[2] !== "string")
    fn_args.splice(2, 0, "")
  
  let [exe, args = [], inp = "", opts = {}] = fn_args
  
  opts = {...dflt_opts, ...opts}
  
  return new Promise((res, rej) => {
    let spawn_opts = {
      ...opts.cwd !== null ? {cwd: opts.cwd} : {},
    }
    
    let proc = cp.spawn(exe, args, spawn_opts)
    let out_bufs = []
    let err_bufs = []
    
    let done = 0
    
    const resolve = v => {
      done = 1
      res(v)
    }
    
    const reject = e => {
      done = 1
      rej(e)
    }
    
    const ael = (obj, type, fn) => {
      obj.on(type, (...args) => {
        if(!done) fn(...args)
      })
    }
    
    const on_err = err => {
      reject(err)
    }
    
    const ael_err = obj => {
      return ael(obj, "error", on_err)
    }
    
    ael_err(proc)
    
    for(const st of sts)
      ael_err(proc[st])
    
    ael(proc.stdout, "data", a => out_bufs.push(a))
    ael(proc.stderr, "data", a => err_bufs.push(a))
    
    proc.on("exit", code => {
      let out = Buffer.concat(out_bufs)
      let err = Buffer.concat(err_bufs)
      
      if(!opts.raw) out = out.toString()
      if(!opts.raw) err = err.toString()
        
      const fail = () => reject([code, out, err])
      
      if(!opts.allow_nzec && code)
        return fail()
      
      if(!opts.allow_stderr && err.length !== 0)
        return fail()
      
      if(opts.allow_stderr)
        return resolve([out, err])
      
      resolve(out)
    })
    
    proc.stdin.end(inp)
  })
}

module.exports = {
  sts,
  opts: dflt_opts,
  run,
}