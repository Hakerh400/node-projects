"use strict"

global.isElectron = 1

const fs = require("fs")
const path = require("path")
const O = require("../js-util")
const assert = require("../assert")
const media = require("../media")
const log_status = require("../log-status")
const format = require("../format")

const HD = 0

const w = HD ? 1920 : 640
const h = HD ? 1080 : 480
const fps = 60
const fast = !HD

const [wh, hh] = [w, h].map(a => a >> 1)
const [w1, h1] = [w, h].map(a => a - 1)

const duration = 10
const framesNum = fps * duration

const main = async () => {
  const out_file = get_out_file()

  media.renderVideo(out_file, w, h, fps, fast, (w, h, g, f) => {
    log_status(f, framesNum)
    
    g.fillStyle = "white"
    g.fillRect(0, 0, w, h)
    
    g.fillStyle = "red"
    g.fillRect(0, 0, 100, 100)

    return f !== framesNum
  }, () => {
    window.close()
  })
}

const get_out_file = (vid=0) => {
  if(vid || !HD) return format.path("-render/1.mp4")
  const project = path.parse(__dirname).name
  return format.path(`-render/${project}.mp4`)
}

main().catch(log)