// "use strict"

const fs = require("fs")
const path = require("path")
// const crypto = require("crypto")
const O = require("../js-util")
const assert = require("../assert")
const crypto = require(".")

const main = () => {
  let str = "Test"
  let key = crypto.mk_sym_key()
  let enc = crypto.enc(key, str)
  let dec = crypto.dec(key, enc)
  
  O.wfs("/home/user/Downloads/1.txt", crypto.show(key))
  
  // let key_pair = crypto.generateKeyPairSync("rsa", {
  //   modulusLength: 8192,
  // })
  // 
  // let pub = key_pair.publicKey
  // let priv = key_pair.privateKey
  // 
  // // O.show_keys(crypto)
  // let str = "Test"
  // let buf = Buffer.from(str)
  // let enc = crypto.publicEncrypt({key: pub}, buf)
  // let dec = crypto.privateDecrypt({key: priv}, enc)
  // 
  // log(enc.toString("hex"))
  // O.logb()
  // log(dec.toString())
  // 
  // // log(priv.export({
  // //   format: "jwk",
  // // }))
}

main()