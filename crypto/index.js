"use strict"

const fs = require("fs")
const path = require("path")
const crypto = require("crypto")
const O = require("../js-util")
const assert = require("../assert")

const hash_size = 64
const hash_alg = "sha512"
const line_len = 100

const ctor_sym = Symbol()

const rand_buf = len => {
  return crypto.randomBytes(len)
}

const mk_salt = () => {
  return rand_buf(hash_size)
}

const concat = (...xs) => {
  return Buffer.concat(xs.map(x => {
    if(Buffer.isBuffer(x)) return x
    if(Array.isArray(x)) return Buffer.from(x)
    if(typeof x === "string") return Buffer.from(x)
    // if(typeof x === "number") return Buffer.from([x])
    
    assert.fail(x)
  }))
}

const hash = buf => {
  buf = to_buf(buf)
  
  let hash_obj = crypto.createHash(hash_alg)
  hash_obj.update(buf)
  
  return hash_obj.digest()
}

const squash = (...xs) => {
  return hash(concat(...xs))
}

const enc_sym_raw = (key, buf) => {
  buf = to_buf(buf)
  key = to_buf(key)
  
  assert(key.length === hash_size)
  
  let len = buf.length
  let buf1 = Buffer.alloc(len)
  let h = null
  let k = 0
  
  for(let i = 0; i !== len; i++){
    let hi = i % hash_size
    
    if(hi === 0){
      h = hash(concat(key, String(k)))
      k++
    }
    
    buf1[i] = buf[i] ^ h[hi]
  }
  
  return buf1
}

const dec_sym_raw = (key, buf) => {
  return enc_sym_raw(key, buf)
}

const pack = buf => {
  return sym_key_0.enc(buf)
}

const unpack = buf => {
  return sym_key_0.dec(buf)
}

const mk_sym_key = () => {
  let key = rand_buf(hash_size)
  return new SymKey(ctor_sym, key)
}

const enc = (key, buf) => {
  assert(key instanceof Key)
  return key.enc(buf)
}

const dec = (key, buf) => {
  assert(key instanceof Key)
  return key.dec(buf)
}

const show = obj => {
  if(Buffer.isBuffer(obj))
    return show_buf(obj)
  
  assert(obj instanceof Key)
  return obj.show()
}

const parse_buf = str => {
  assert(typeof str === "string")
  return Buffer.from(str.replace(/\s+/g, ""), "hex")
}

const parse = str => {
  let buf = unpack(parse_buf(str))
  
  let type = buf[0]
  buf = buf.slice(1)
  
  if(type === 0)
    return new SymKey(ctor_sym, buf)
  
  assert.fail(type)
}

class Key {
  enc(buf){ O.virtual("enc") }
  dec(buf){ O.virtual("dec") }
  show(){ O.virtual("show") }
}

class SymKey extends Key {
  constructor(sym, key){
    assert(sym === ctor_sym)
    assert(Buffer.isBuffer(key))
    assert(key.length === hash_size)
    
    super()
    
    this.key = key
  }
  
  enc(buf){
    buf = to_buf(buf)
    
    let salt = mk_salt()
    let key = squash(this.key, salt)
    let h = hash(buf)
    
    buf = enc_sym_raw(key, concat(h, buf))
    
    return concat(salt, buf)
  }
  
  dec(buf){
    assert(Buffer.isBuffer(buf))
    
    let salt = buf.slice(0, hash_size)
    let key = squash(this.key, salt)
    
    buf = dec_sym_raw(key, buf.slice(hash_size))
    
    let h = buf.slice(0, hash_size)
    buf = buf.slice(hash_size)
    
    assert(hash(buf).equals(h))
    
    return buf
  }
  
  show(){
    return show_buf(pack(concat([0], this.key)))
  }
}

class AsymKey extends Key {
  constructor(sym){
    assert(sym === ctor_sym)
    super()
  }
}

const to_buf = buf => {
  if(Buffer.isBuffer(buf)) return buf
  return Buffer.from(buf)
}

const show_buf = buf => {
  let str = buf.toString("hex")
  let n = str.length
  let xs = []
  
  for(let i = 0; i < n; i += line_len)
    xs.push(str.slice(i, i + line_len))
  
  return xs.join("\n")
}

const sym_key_0 = new SymKey(ctor_sym, hash(""))

module.exports = {
  hash_alg,
  hash_size,
  sym_key_0,
  rand_buf,
  mk_salt,
  hash,
  squash,
  enc_sym_raw,
  dec_sym_raw,
  pack,
  unpack,
  mk_sym_key,
  enc,
  dec,
  show,
  show_buf,
  parse,
  parse_buf,
  Key,
  SymKey,
  AsymKey,
}