'use strict';

const fs = require('fs');
const path = require('path');
const O = require('../omikron');
const assert = require('../assert');

const main = () => {
  log(O.shuffle([...'LBWXV']).join(''));
};

main();