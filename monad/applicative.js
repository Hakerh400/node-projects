"use strict"

const fs = require("fs")
const path = require("path")
const O = require("../js-util")
const assert = require("../assert")
const Functor = require("./functor")

class Applicative extends Functor {
  static is_Applicative(){ return 1 }
  
  static pure(x){ O.virtual("pure", 1) }
  
  static liftA2(f, m1, m2){
    return this.bind(m1, x => this.fmap(y => f(x, y), m2))
  }
}

module.exports = Applicative