"use strict"

const fs = require("fs")
const path = require("path")
const O = require("../js-util")
const assert = require("../assert")
const Foldable = require("./foldable")
const Functor = require("./functor")
const Applicative = require("./applicative")
const Monad = require("./monad")

const main = () => {
  let S = AsyncT(List)
  
  let res = S.do([
    "a", V => S.pure(5),
    V => S.pure(V.a),
  ])
  
  res.then(log).catch(a => {throw new Error(a)})
}

const Identity = Foldable.extend(class extends Monad {
  static name = "Identity"
  
  static pure(x){
    return x
  }
  
  static fmap(f, m){
    return f(m)
  }
  
  static bind(m, f){
    return f(m)
  }
  
  static is_Foldable(){ return 1 }
  
  static foldr(f, z, m){
    return f(m, z)
  }
})

const List = Foldable.extend(class extends Monad {
  static name = "List"
  
  static pure(x){
    return [x]
  }
  
  static fmap(f, m){
    return m.map(f)
  }
  
  static bind(m, f){
    return m.flatMap(f)
  }
  
  static is_Foldable(){ return 1 }
  
  static foldr(f, z, m){
    return m.reduceRight((xs, x) => f(x, xs), z)
  }
})

const StateT = M => class extends Monad {
  static name = `(StateT ${M.name})`
  
  static pure(x){
    return s => M.pure([x, s])
  }
  
  static fmap(f, m){
    return s => M.fmap(([x, s1]) => [f(x), s1], m(s))
  }
  
  static bind(m, f){
    return s => M.bind(m(s), ([x, s1]) => f(x)(s1))
  }
  
  static get(){
    return s => M.pure([s, s])
  }
  
  static put(s){
    return _ => M.pure([null, s])
  }
  
  static lift(m){
    return s => M.fmap(a => [a, s], m)
  }
  
  static runStateT(m, s){
    return m(M.pure(s))
  }
  
  static evalStateT(m, s){
    return M.fmap(([x, _]) => x, this.runStateT(m, s))
  }
  
  static execStateT(m, s){
    return M.fmap(([_, s]) => s, this.runStateT(m, s))
  }
}

const AsyncT = M => {
  if(!M.is_Foldable())
    throw new Error(`No instance for (Foldable ${M.name})`)
  
  return class extends Monad {
    static name = `(AsyncT ${M.name})`
    
    static pure(x){
      return Promise.resolve(M.pure(x))
    }
    
    static fmap(f, m){
      return m.then(m1 => Promise.resolve(M.fmap(f, m1)))
    }
    
    // static bind(m, f){
    //   const A = p => {
    //     if(p instanceof Promise) p.then(log)
    //     else log(p)
    //     return p
    //   }
    //   
    //   return m.then(m1 => {
    //     let ps = M.bind(m1, x => f(x))
    //     return new Promise((a, b) => {})
    //   })
    // }
  }
}

main()