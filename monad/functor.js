"use strict"

const fs = require("fs")
const path = require("path")
const O = require("../js-util")
const assert = require("../assert")
const TypeClass = require("./type-class")

class Functor extends TypeClass {
  static is_Functor(){ return 1 }
  static fmap(f, m){ O.virtual("fmap", 1) }
}

module.exports = Functor