"use strict"

const fs = require("fs")
const path = require("path")
const O = require("../js-util")
const assert = require("../assert")

class TypeClass {
  static is_Foldable(){ return 0 }
  static is_Functor(){ return 0 }
  static is_Applicative(){ return 0 }
  static is_Monad(){ return 0 }
  
  static extend(M){
    let this_keys = new Set(O.keys(this))
    let M_keys = new Set(O.keys(M))
    
    for(let key of this_keys){
      if(M_keys.has(key)) continue
      M[key] = this[key]
    }
    
    return M
  }
}

module.exports = TypeClass