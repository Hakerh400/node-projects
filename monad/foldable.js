"use strict"

const fs = require("fs")
const path = require("path")
const O = require("../js-util")
const assert = require("../assert")
const TypeClass = require("./type-class")

class Foldable extends TypeClass {
  static is_Foldable(){ return 1 }
  
  static foldr(f, z, m){ O.virtual("foldr", 1) }
  
  static toList(m){
    return this.foldr((x, xs) => [x, ...xs], [], m)
  }
}

module.exports = Foldable