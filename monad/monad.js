"use strict"

const fs = require("fs")
const path = require("path")
const O = require("../js-util")
const assert = require("../assert")
const Functor = require("./functor")
const Applicative = require("./applicative")

class Monad extends Applicative {
  static is_Monad(){ return 1 }
  
  static bind(m, f){ O.virtual("bind", 1) }
  
  static do(ms){
    let ms1 = []
    
    for(let i = 0; i < ms.length; i++){
      let f = ms[i]
      let s = null
      
      if(typeof f === "string"){
        s = f
        f = ms[++i]
      }
      
      ms1.push([s, f])
    }
    
    if(ms1.length === 0)
      throw new Error("Empty 'do' block")
    
    let last = O.last(ms1)
    
    if(last[0] !== null)
      throw new Error("The last statement in a 'do' block must be an expression")
    
    let s = Symbol()
    last[0] = s
    
    let res = ms1.reduce((m, [s, f]) => {
      let m1 = this.bind(m, V => {
        let res = f(V)
        return this.fmap(x => (s !== null ? {...V, [s]: x} : V), res)
      })
      
      return m1
    }, this.pure({}))
    
    return this.fmap(x => x[s], res)
  }
  
  static sequence(ms){
    if(ms.length === 0) return this.pure([])
    
    return this.do([
      "x", V => ms[0],
      "xs", V => this.sequence(ms.slice(1)),
      V => this.pure([V.x, ...V.xs]),
    ])
  }
  
  static replicateM(n, m){
    return this.sequence(replicate(n, m))
  }
}

const replicate = (n, x) => [...Array(n)].map(_ => x)

module.exports = Monad