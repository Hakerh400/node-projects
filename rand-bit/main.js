"use strict"

const fs = require("fs")
const path = require("path")
const O = require("../js-util")
const assert = require("../assert")
const crypto = require("crypto")
const readline = require("../readline")

const fd_out = process.stdout.fd

let closed = 0

let rl = null

const main = () => {
  rl = readline.rl()
  interact()
}

const ask_for_input = () => {
  if(closed) return
  rl.ask("", on_input)
}

const on_input = inp => {
  if(closed) return
  
  if(inp === ""){
    clear()
    interact()
    return
  }
  
  if(inp === "q"){
    close()
    return
  }
}

const gen_bit = () => {
  return crypto.randomBytes(1)[0] & 1
}

const display_bit = bit => {
  log(`\n ${bit}`)
}

const gen_and_display_bit = () => {
  const bit = gen_bit()
  display_bit(bit)
}

const interact = () => {
  gen_and_display_bit()
  ask_for_input()
}

const close = () => {
  closed = 1
  rl.close()
  rl = null
}

const clear = () => {
  fs.writeSync(fd_out, "\x1B[2J\x1B[0;0H\x1Bc")
}

main()