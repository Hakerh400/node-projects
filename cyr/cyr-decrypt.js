'use strict';

const fs = require('fs');
const path = require('path');
const O = require('../js-util');
const assert = require('../assert');
const cyr = require('.');

let flags = O.obj()

const set_flags = flags1 => {
  flags = flags1
}

const dec = inp => {
  if(flags.LEGACY){
    return O.sanl(inp)
      .map(a => dec_legacy(a))
      .join('\n')
  }

  inp = inp.trim()
  if(inp.length === 0) return ""
  
  let lines = O.sanl(inp.trim())
  let h = lines.length
  
  assert(h !== 0)
  if(h === 1) return ""
  
  let w = lines[0].length
  
  for(const line of lines)
    assert(line.length === w)
  
  const grid = new O.Grid(w, h, (x, y) => {
    let c = lines[y][x]
    assert(/^\d$/.test(c))
    return c | 0
  })
  
  let s = ""
  let a0 = null
  let d0 = null
  let n_cnt = 0
  
  loop: for(let y = 1; y !== h; y++){
    for(let x = 0; x !== w; x++){
      let a = grid.get(x, y - 1)
      let d = (10 - grid.get(x, y)) % 10
      
      if(a0 === null){
        a0 = a
        d0 = d
        continue
      }
      
      let na = a0 * 10 + a
      let nd = d0 * 10 + d
      
      a0 = null
      
      let n = (nd - na + 100) % 100
      
      if(n_cnt !== 0){
        s += String(n).padStart(2, "0")
        n_cnt--
        continue
      }
      
      if(n === 0){
        s += " "
        continue
      }
      
      if(n <= 30){
        let c = nat_to_char(n)
        assert(c !== null)
        s += c
        continue
      }
      
      if(n === 31) break loop
      
      if(n >= 40 && n < 50){
        n_cnt = n - 40 + 1;
        continue
      }
    }
  }
  
  if(a0 !== null) s += "?"
  
  return s
}

const dec_legacy = inp => {
  inp = inp.replace(/\s+/, '');
  
  let out = '';
  
  const push = s => {
    out += s;
  };
  
  const err = () => {
    push('?');
  };
  
  const next_char = () => {
    const c = inp.slice(0, 1);
    inp = inp.slice(1);
    return c;
  };
  
  const next_digit = () => {
    const c = next_char();
    if(!(c >= '0' && c <= '9')) return null;
    return Number(c);
  };
  
  while(inp !== ''){
    const d1 = next_digit();
    
    if(d1 === null || d1 >= 4){
      err();
      continue;
    }
    
    const d2 = next_digit();
    
    if(d2 === null){
      err();
      continue;
    }
    
    const n = d1 * 10 + d2;
    const c = nat_to_char(n);
    
    if(c === null){
      err();
      continue;
    }
    
    push(c);
  }
  
  return out;
};

const nat_to_char = n => {
  if(n === 0) return ' ';
  if(n === 31) return '.';
  
  return cyr.nat_to_letter(n);
};

module.exports = {
  set_flags,
  dec,
  nat_to_char,
};