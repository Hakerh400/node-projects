'use strict';

const fs = require('fs');
const path = require('path');
const O = require('../js-util');
const assert = require('../assert');
const format = require('../format');
const cyr = require('.');
const cyr_decrypt = require('./cyr-decrypt');

const flags = {
  TEST: 0,
  LEGACY: 0,
}

const {cyr_dir} = cyr;
const inp_file = path.join(cyr_dir, 'inp.txt');
const out_file = path.join(cyr_dir, 'out.txt');

const main = () => {
  cyr_decrypt.set_flags(flags)
  
  const args = flags.TEST
    ? [format.path("-dw/inp.txt")]
    : process.argv.slice(2)
  const args_num = args.length
  
  if(args_num === 0){
    const inp = O.rfs(inp_file, 1);
    const out = O.sanl(inp).map(a => cyr_decrypt.dec(a)).join('\n');
    
    O.wfs(out_file, out);
    
    return
  }
  
  if(args_num === 1){
    const inp = O.rfs(args[0], 1)
    const out = cyr_decrypt.dec(inp)
    
    log(out)
    
    return
  }
  
  assert.fail()
};

main();