'use strict';

const fs = require('fs');
const path = require('path');
const O = require('../js-util');
const assert = require('../assert');

const cwd = __dirname;
const cyr_dir = path.join(cwd, '../../Other/Cyrillic');
const cyr_file = path.join(cyr_dir, '1.txt');

const lower = O.rfs(cyr_file, 1);
const upper = lower.toUpperCase();
const len = lower.length;
const mp = O.obj();

for(let i = 0; i !== len; i++)
  mp[lower[i]] = i + 1;

const nat_to_letter = (n, up=0) => {
  let s = up ? upper : lower
  
  n--
  if(n >= 0 && n < s.length) return s[n]
  
  return null
};

const letter_to_nat = c => {
  c = c.toLowerCase()
  assert(O.has(mp, c))
  return mp[c]
};

module.exports = {
  cyr_dir,
  cyr_file,
  len,
  lower,
  upper,
  mp,
  nat_to_letter,
  letter_to_nat,
};