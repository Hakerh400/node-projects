"use strict"

const fs = require("fs")
const path = require("path")
const O = require("../js-util")
const assert = require("../assert")
const format = require("../format")
const readline = require("../readline")

const cwd = __dirname
const info_file = path.join(cwd, "info.json")
const fs_file = path.join(cwd, "../../react/fs.txt")

const included_exts = [
  "webm",
  "mp3",
  "m4a",
  "ogg",
  "mkv",
  "mp4",
  "wav",
]

const excluded_exts = [
  "bat",
  "txt",
  "aup3",
  "zip",
  "ytdl",
  "part",
  "jpeg",
]

const excluded_rules = [
  //
]

const excluded_ytids = [
  //
]

let rl = null
let info_obj = null
let index0 = null
let index = null

const main = async () => {
  if(!O.exi(info_file)){
    O.wfs(info_file, JSON.stringify({
      index: 1,
      mp: {},
    }))
  }

  info_obj = JSON.parse(O.rfs(info_file, 1))
  index0 = index = info_obj.index

  rl = readline.rl()

  await main1()

  rl.close()
  rl = null

  info_obj.index = index
  O.wfs(info_file, JSON.stringify(info_obj))
}

const main1 = async () => {
  let lines = O.sanl(O.rfs(fs_file, 1))

  lines = lines.map(line => {
    let m = line.match(/^(?:\|\s+)+(?!\|?\s+|[\+\\]\-{3})(.*)$/)
    if(m === null) return null

    let s = m[1].trim()
    if(s === "") return null

    let {name, ext} = path.parse(s)
    m = ext.match(/^\.(.+)$/)

    if(m === null)
      assert.fail(line)

    ext = m[1]

    if(excluded_exts.includes(ext))
      return null

    if(included_exts.includes(ext))
      return name

    assert.fail(ext)
  }).filter(line => line !== null)

  const lines_num = lines.length

  main_loop: for(let i = 1; i <= lines_num; i++){
    const cond = i >= index0

    const log1 = (...args) => {
      if(!cond) return
      log(...args)
    }

    if(i > index0) await clear()
    log1(`index: ${i}`)

    const line = lines[i - 1]

    if(excluded_rules.some(rule => rule.test(line)))
      continue

    let m = line.match(/^\[((?:[^\[\]]|\[[^\]]*\])+)\] (.*) \[([a-zA-Z0-9\-\x5F]{11})\]\~?$/)

    if(m !== null){
      let [ch, s, ytid] = m.slice(1)
      
      if(excluded_ytids.includes(ytid))
        continue
      
      const info = {
        channel: ch,
        ytid,
        authors: null,
        title: null,
        feat: null,
        vs: null,
        remixes: null,
        attribs: null,
        // publishers: null,
        genres: null,
      }

      s = s.trim()
      let s0 = s

      const show = (...args) => {
        if(!cond) log(`index: ${i}`)
        log(`ytid: ${ytid}`)
        log(`channel: ${ch}`)
        log(`orig: ${s0}`)

        if(s.trim() !== "")
          log(`\n${s}`)
        
        log()
        log(info)

        if(args.length !== 0){
          log()
          log(...args)
        }
      }

      s = s.replace(/ l /g, " | ")

      check_title: {
        check_title_1: {
          if(info.title !== null) break check_title

          let titles = []

          let s1 = s.replace(/(?:^|\s)\'([^\']+)\'(?:\s|$)/ig, (a, s) => {
            titles.push(s.trim())
            return ""
          }).replace(/\s+/g, " ").trim()

          if(titles.length === 0)
            break check_title_1

          if(titles.length >= 2)
            return show(titles)

          let title = titles[0]

          check_1: {
            if(/^(?:[a-zA-Z]+ )+$/.test(`${title} `)){
              title = title.split(" ").map(a => O.cap(a, 1)).join(" ")
              break check_1
            }

            return show("check_title_1 check_1", title)
          }

          s = s1
          info.title = title

          break check_title
        }

        // return show("check_title")
      }

      check_authors: {
        check_authors_1: {
          let authors = []
          
          let s1 = s.replace(/ \| ([\w ]+) production\b/ig, (a, s) => {
            authors.push(s.trim())
            return ""
          }).replace(/\s+/g, " ").trim()
          
          if(authors.length === 0)
            break check_authors_1
          
          if(authors.length >= 2)
            return show(authors)
          
          let author = authors[0]
          
          check_1: {
            if(/^(?:[A-Z]+ )+$/.test(`${author} `)){
              author = author.split(" ").map(a => O.cap(a, 1)).join(" ")
              break check_1
            }
          
            return show("check_authors_1 check_1", authors)
          }
          
          s = s1

          if(info.authors === null) info.authors = []
          info.authors.push(author)
        }

        check_authors_2: {
          let authors = []
          
          let s1 = s.replace(/\bprod by ([a-z\u2020]+)/ig, (a, s) => {
            authors.push(s.trim())
            return ""
          }).replace(/\s+/g, " ").trim()
          
          if(authors.length === 0)
            break check_authors_2
          
          if(authors.length >= 2)
            return show(authors)
          
          let author = authors[0]
          
          check_1: {
            let m
            
            if(/^(?:[A-Z]+ )+$/.test(`${author} `)){
              author = author.split(" ").map(a => O.cap(a, 1)).join(" ")
              break check_1
            }

            if(m = author.match(/^[A-Z]{3,}|[a-z\u2020]{3,}$/g)){
              author = m.map(a => O.cap(a.replace(/\u2020/g, "t"), 1)).join(" ")
              break check_1
            }
          
            return show("check_authors_2 check_1", authors)
          }
          
          s = s1

          if(info.authors === null) info.authors = []
          info.authors.push(author)
        }

        // return show("check_authors")
      }

      check_genres: {
        check_genres_1: {
          let genres = []
          
          let s1 = s.replace(/\btrap\b/ig, (a, s) => {
            genres.push("Trap")
            return ""
          }).replace(/\s+/g, " ").trim()
          
          if(genres.length === 0)
            break check_genres_1
          
          if(genres.length >= 2)
            return show(genres)
          
          let genre = genres[0]
          
          s = s1
          
          if(info.genres === null) info.genres = []
          info.genres.push(genre)
        }

        check_genres_2: {
          let genres = []
          
          let s1 = s.replace(/\bbeat\b/ig, (a, s) => {
            genres.push("Beat")
            return ""
          }).replace(/\s+/g, " ").trim()
          
          if(genres.length === 0)
            break check_genres_2
          
          if(genres.length >= 2)
            return show(genres)
          
          let genre = genres[0]
          
          s = s1
          
          if(info.genres === null) info.genres = []
          info.genres.push(genre)
        }

        // return show("check_genres")
      }

      check_attribs: {
        check_attribs_1: {
          if(info.attribs !== null) break check_attribs

          let attribs = []
          
          let s1 = s.replace(/\bins?trumental\b/ig, (a, s) => {
            attribs.push("Instrumental")
            return ""
          }).replace(/\s+/g, " ").trim()
          
          if(attribs.length === 0)
            break check_attribs_1
          
          if(attribs.length >= 2)
            return show(attribs)
          
          let attrib = attribs[0]
          
          if(info.attribs === null) info.attribs = []
          info.attribs.push(attrib)

          s = s1
        }

        // return show("check_attribs")
      }

      check_extra_info: {
        let s1 = s

        while(1){
          s1 = s
          
          s1 = s1.replace(/\bsave (?:nature|future)\b/ig, "")
          s1 = s1.replace(/\bearth 20\d{2}/ig, "")

          s1 = [...s1].filter(c => {
            return c.codePointAt(0) <= 0xFFFF
          }).join("")

          s1 = s1.trim()

          if(s1 !== s){
            s = s1
            continue
          }

          break
        }
      }

      if(s !== "")
        return show("Extra info")

      if(!cond){
        if(!O.has(info_obj.mp, line))
          return show("Does not contain line")

        const info_str_old = JSON.stringify(info_obj.mp[line])
        const info_str_new = JSON.stringify(info)

        if(info_str_old !== info_str_new)
          return show("Regression")

        continue
      }

      show()

      get_inp: while(1){
        const cmd = await ask("\n> ")
  
        if(cmd === ""){
          index++
          info_obj.mp[line] = info
          continue main_loop
        }

        if(cmd === "q") return
        
        log1("Unknown command")
        continue get_inp
      }
    }

    assert.fail()
  }
}

const ask = (s="") => new Promise(res => {
  rl.ask(s, inp => {
    res(inp)
  })
})

const clear = async () => {
  await write('\x1B[2J\x1B[0;0H\x1Bc')
}

const write = data => new Promise(res => {
  process.stdout.write(data, () => {
    res()
  })
})

main().catch(log)