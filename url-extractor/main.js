'use strict';

const fs = require('fs');
const path = require('path');
const O = require('../omikron');
const assert = require('../assert');
const format = require('../format');

const file = format.path('-dw/1.txt');

const main = () => {
  let str = O.rfs(file, 1);
  let urls = O.match(str, /"url"\:"[^"]*/g).map(a => a.slice(7));
  
  urls = urls.
    filter(url => url !== '').
    filter(url => !url.startsWith('chrome://'));
  
  const urls_new = [];
  let prev = null;
  
  for(const url of urls){
    if(url === prev) continue;
    
    urls_new.push(url);
    prev = url;
  }
  
  O.wfs(file, urls_new.join('\n'));
};

main();