"use strict"

const fs = require("fs")
const path = require("path")
const crypto = require("crypto")
const O = require("../js-util")
const assert = require("../assert")
const log_status = require("../log-status")
const format = require("../format")

const alg = "sha256"

const main = () => {
  O.cap1 = O.cap
  
  const mk_str = O.last([
    E => {
      return O.cap1([
        "dear",
        E[0] ? E[1] ? "sha256" : "sha-256" : E[1] ? "SHA256" : "SHA-256",
        ",",
        E[2] ? E[3] ? "we want you to" : "" : E[3] ?
          E[4] ? "can you" : "please" :
          E[4] ? "can you please" : "please",
        E[5] ? E[6] ? "write" : "create" : "show us",
        E[7] ? "a" : "some",
        "hash that",
        E[8] ? "starts" : "begins",
        "with two",
        E[9] ? "leading" : "",
        "hexadecimal",
        E[10] ? "zeros" : "0s",
        ".",
      ].join(" ").replace(/\s+/g, " ")
        .replace(/ ([\.\,\?\!])/g, (a, b) => b)
        .replace(/[\.\?\!] \w/g, a => a.toUpperCase())
        .trim())
    },
    E => {
      return O.cap1([
        "Hey listen,",
        E[12] ? "can you" : "",
        E[13] ? "please" : "",
        E[14] ? "now" : "",
        E[0] ? E[1] ? "write" : "create" : E[1] ? "show me" : "show us",
        E[2] ? "a" : "some",
        E[3] ? E[4] ? "sha256" : "sha-256" : E[4] ? "SHA256" : "SHA-256",
        "hash",
        E[5] ? "that" : ", such that it",
        E[6] ? "starts" : "begins",
        "with",
        E[7] ? "four" : "4",
        E[8] ? "zeros" : "digits 0",
        "when",
        E[9] ? "written" : "represented",
        E[10] ? "in" : "as a",
        E[11] ? "hex" : "hexadecimal",
        E[10] ? "" : "string",
        E[15] ? E[16] ? "." : "?" : E[16] ? "!" : "",
      ].join(" ").replace(/\s+/g, " ")
        .replace(/ ([\.\,\?\!])/g, (a, b) => b).trim())
    },
    E => {
      return O.cap1([
        E[18] ? "that's" : "that is",
        E[19] ? E[20] ? "actually" : "indeed" : E[20] ? "very" : "",
        E[21] ? "good" : "nice",
        E[22] ? "." : "!",
        E[12] ? "can you" : "",
        E[13] ? "please" : "",
        E[14] ? E[23] ? "now" : "this time" : E[23] ? "here" : "",
        E[0] ? E[1] ? "write" : "create" : E[1] ? "show me" : "show us",
        E[2] ? "a" : "some",
        E[3] ? E[4] ? "sha256" : "sha-256" : E[4] ? "SHA256" : "SHA-256",
        "hash",
        E[5] ? "that" : ", such that it",
        E[6] ? "starts" : "begins",
        "with",
        E[7] ? "six" : "6",
        "digits",
        E[8] ? E[17] ? "7" : "'7'" : E[17] ? "\"7\"" : "`7`",
        "when",
        E[9] ? "written" : "represented",
        E[10] ? "in" : "as a",
        E[11] ? "hex" : "hexadecimal",
        E[10] ? "" : "string",
        E[15] ? E[16] ? "." : "?" : E[16] ? "!" : "",
      ].join(" ").replace(/\s+/g, " ")
        .replace(/ ([\.\,\?\!])/g, (a, b) => b)
        .replace(/[\.\?\!] \w/g, a => a.toUpperCase())
        .trim())
    },
    E => {
      return O.cap1([
        "ok, how about",
        E[25] ? "at least" : "",
        "seven",
        E[14] ? "starting" : "",
        E[26] ? "digits" : "",
        E[18] ? E[24] ? "7" : "'7'" : E[24] ? "\"7\"" : "`7`",
        E[23] ? E[19] ? "?" : "?!" : E[19] ? "!" : ":",
        E[20] ? E[13] ? "your task is to" : "" : E[12] ?
          E[13] ? "can you" : "please" :
          E[13] ? "can you please" : "please can you",
        E[0] ? E[1] ? "write" : "create" : E[1] ? "show me" : "show us",
        E[2] ? "a" : "some",
        E[3] ? E[4] ? "sha256" : "sha-256" : E[4] ? "SHA256" : "SHA-256",
        E[5] ? "that" : ", such that it",
        E[6] ? "starts" : "begins",
        "with",
        E[21] ? E[22] ? "literally" : "basically" : E[22] ? "at least" : "",
        E[7] ? "seven" : "7",
        E[27] ? "digits" : "",
        (E[8] ? E[17] ? "7" : "'7'" : E[17] ? "\"7\"" : "`7`") +
          (E[7] ? "" : "s"),
        "when",
        E[9] ? "written" : "represented",
        E[10] ? "in" : "as a",
        E[11] ? "hex" : "hexadecimal",
        E[10] ? "" : "string",
        E[15] ? E[16] ? "." : "?" : E[16] ? "!" : "",
      ].join(" ").replace(/\s+/g, " ")
        .replace(/ ([\.\,\?\!])/g, (a, b) => b)
        .replace(/[\.\?\!] \w/g, a => a.toUpperCase())
        .trim())
    },
    E => {
      return O.cap1([
        E[0] ? "the" : "",
        E[1] ? "Rudolf's" : "",
        "number",
        E[2] ? "pi" : "PI",
        "is a",
        E[3] ? "famous" : "",
        E[23] ? "positive" : "",
        E[24] ? "real" : "",
        "constant",
        E[4] ? "in math" : "",
        E[5] ? "that" : ", which",
        E[6] ? "is" : "can",
        E[7] ? "always" : "",
        E[6] ? "" : "be",
        "used",
        ...E[8] ? [
          "to",
          E[9] ? E[10] ? "calculate" : "evaluate" :
            E[10] ? "measure" : "find out",
        ] : [
          "for",
          E[9] ? E[10] ? "calculating" : "evaluating" :
            E[10] ? "measuring" : "finding out",
        ],
        E[11] ? "the" : "",
        E[12] ? "value of" : "",
        E[13] ? "the" : "",
        E[14] ? E[15] ? "area" : "circumference" :
          E[15] ? "volume" : "perimeter",
        E[16] ? "of" : "for",
        E[17] ? "almost" : "",
        E[18] ? "any" : "all",
        E[19] ? "round" : "spherical",
        E[18] ? "object" : "objects",
        "in",
        E[20] ? "the context of" : "",
        E[21] ? "algebraic" : "",
        "geometry",
        E[22] ? "." : "!",
      ].join(" ").replace(/\s+/g, " ")
        .replace(/ ([\.\,\?\!])/g, (a, b) => b)
        .replace(/[\.\?\!] \w/g, a => a.toUpperCase())
        .trim())
    },
    E => {
      s = parseInt(E.join(""), 2).toString(16).padStart(n, "0")
      return `The hash of this string starts with "${s}"`
    },
  ])
  
  let s = null
  
  const bits_of_entropy = 36
  let n = bits_of_entropy >> 2
  
  const check_hash = (E, hash) => {
    // let n = bits_of_entropy >> 2
    // let s = parseInt(E.join(""), 2).toString(16).padStart(n, "0")
    return hash.startsWith(s)
  }
  
  let res = find_hash(bits_of_entropy, mk_str, check_hash)
  
  O.logb()
  
  if(res === null){
    log("/")
    return
  }
  
  let {bits, str, hash} = res
  
  log(bits.join(""))
  log(`\n${O.ca(5, () => "-").join("")}\n`)
  
  log(str)
  log(hash)
}

const find_hash = (bits_of_entropy, fn_mk_str, fn_check_hash) => {
  let iters_num = 2 ** bits_of_entropy
  
  let i = 20000e6
  let bits = O.ca(bits_of_entropy, j => i / 2 ** j & 1)
  
  outer_loop: while(1){
    let str = fn_mk_str(bits)
    
    if(++i % 1e6 === 0){
      log_status(i, iters_num, "hash")
      // log(str)
    }
    
    let hash = calc_hash(str).toString("hex")
    let found = fn_check_hash(bits, hash)
    
    if(found) return {bits, str, hash}
    
    for(let i = 0; i !== bits_of_entropy; i++)
      if(bits[i] ^= 1) continue outer_loop
    
    return null
  }
}

const calc_hash = data => {
  let hash_obj = crypto.createHash(alg)
  hash_obj.update(data)
  return hash_obj.digest()
}

main()