'use strict';

const fs = require('fs');
const path = require('path');
const O = require('../js-util');
const assert = require('../assert');

const cwd = __dirname;
const drafts_dir = path.join(cwd, '../../Drafts');
const m_file = path.join(drafts_dir, 'm.txt');

const main = () => {
  const games = O.sanl(O.rfs(m_file, 1).trim());
  log(O.randElem(games));
};

main();