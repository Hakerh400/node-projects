"use strict"

const fs = require("fs")
const path = require("path")
const O = require("../js-util")
const assert = require("../assert")

const {min, max} = Math

const cwd = __dirname
const io_dir = path.join("../../Other/io")
const inp_file = path.join(io_dir, "inp.txt")
const out_file = path.join(io_dir, "out.txt")
const excluded_words_file = path.join(cwd, "excluded-words.txt")
const exts_file = path.join(cwd, "exts.txt")

let excluded_words = null
let exts = null

const main = () => {
  init_excluded_words()
  // init_exts()
  
  let inp = O.rfs(inp_file, 1)
  let out = fn(inp)
  
  O.wfs(out_file, out)
}

const load_word_list = file => {
  let s = O.rfs(file, 1)
  let xs = O.sortAsc(O.nub(O.sanl(s)))
  
  let s1 = xs.join("\n")
  if(s1 !== s) O.wfs(file, s1)
  
  return new Set(xs)
}

const init_excluded_words = () => {
  excluded_words = load_word_list(excluded_words_file)
}

const init_exts = () => {
  exts = load_word_list(exts_file)
}

const fn = s => {
  // let reg = /[a-z][a-z\d']{2,}/gu
  let reg = /\p{L}[\p{L}\d']{2,}/gu
  
  let toks = s.toLowerCase()
    .replace(/\[[a-z\d\-\x5F]{11}\]/g, "")
    .match(reg)
    .filter(s => {
      if(excluded_words.has(s)) return 0
      if(exts !== null && exts.has(s)) return 0
      if(!/\p{L}{3,}/u.test(s)) return 0
      
      return 1
    })
  
  if(toks === null) toks = []
  
  let mp = new Map()
  
  for(let s of toks){
    if(!mp.has(s)){
      mp.set(s, 1)
      continue
    }
    
    let n = mp.get(s)
    mp.set(s, n + 1)
  }
  
  for(let s of mp.keys()){
    const check = s1 => {
      if(!mp.has(s1)) return
      
      let n = mp.get(s)
      let n1 = mp.get(s1)
      
      mp.delete(s1)
      mp.set(s, n + n1)
    }
    
    if(s.endsWith("y")){
      check(s.slice(0, -1) + "ies")
      continue
    }
    
    if(s.endsWith("ex") || s.endsWith("ix")){
      check(s.slice(0, -2) + "ices")
      continue
    }
    
    if(s === "tooth"){
      check("teeth")
      continue
    }
    
    if(s === "foot"){
      check("feet")
      continue
    }
    
    if(!s.includes("'"))
      check(s + "s")
  }
  
  let xs = [...mp].sort(([a, n], [b, m]) => {
    return (m > n) - (n > m) || (a > b) - (b > a)
  })
  
  let n1 = 0
  let n2 = 0
  
  xs = xs.map(([s, n]) => {
    let s1 = s
    let s2 = String(n)
    
    n1 = max(n1, s1.length)
    n2 = max(n2, s2.length)
    
    return [s1, s2]
  })
  
  xs = xs.map(([s1, s2]) => {
    return `${s1.padEnd(n1)} ${s2.padStart(n2)}`
  })

  return xs.join("\n")
}

main()