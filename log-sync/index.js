'use strict';

const fs = require('fs');
const path = require('path');
const O = require('../js-util');

const fdOut = process.stdout.fd;

module.exports = (
  O.isElectron ? console.logRaw.bind(console) : logSync
);

function logSync(data){
  return fs.writeSync(fdOut, data)
}